import com.denghq.projectbuilder.component.App;
import com.denghq.projectbuilder.component.redis.StringRedisUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {App.class})// 指定启动类
public class TestRedis {

    @Autowired
    private StringRedisUtils stringRedisUtils;


    @Test
    public void testBatchReadRedisForList() throws InterruptedException {
        List<String> keys = Lists.newArrayList();
        IntStream.rangeClosed(1, 100000).forEach(e -> keys.add("test_list:k_" + e));
        Map<String, List<String>> stringListMap = stringRedisUtils.batchReadRedisForList(keys);

        stringListMap.forEach((k, v) -> {
            System.out.println(k + ":" + v);
        });

    }

    @Test
    public void testBatchReadRedisForMap() throws InterruptedException {
        List<String> keys = Lists.newArrayList();
        IntStream.rangeClosed(1, 100000).forEach(e -> keys.add("test_map:k_" + e));
        Map<String, String> stringListMap = stringRedisUtils.batchReadRedisForMap("test_map", keys);

        stringListMap.forEach((k, v) -> {
            System.out.println(k + ":" + v);
        });

    }

    @Test
    public void testBatchReadRedisForString() throws InterruptedException {
        List<String> keys = Lists.newArrayList();
        IntStream.rangeClosed(1, 100).forEach(e -> keys.add("test:k_" + e));
        Map<String, String> stringListMap = stringRedisUtils.batchReadRedisForString(keys);

        stringListMap.forEach((k, v) -> {
            System.out.println(k + ":" + v);
        });

    }

    @Test
    public void testBatchWriteRedisByString() throws InterruptedException {
        List<String> keys = Lists.newArrayList();
        Map<String, String> keyItemMap = Maps.newHashMap();
        Map<String, Long> timeMap = Maps.newHashMap();

        IntStream.rangeClosed(1, 100).forEach(e -> {
            keyItemMap.put("test:k_" + e, "v" + e);
            timeMap.put("test:k_" + e, 50L);
        });
        long start = System.currentTimeMillis();
        stringRedisUtils.batchWriteRedisByString(keyItemMap, timeMap);

        System.out.println(String.format("总计耗时%sms:",System.currentTimeMillis() - start));
    }

    @Test
    public void testBatchWriteRedisByList() throws InterruptedException {

        List<String> keys = Lists.newArrayList();
        Map<String, List<String>> keyItemMap = Maps.newHashMap();
        Map<String, Long> timeMap = Maps.newHashMap();

        IntStream.rangeClosed(1, 100000).forEach(e -> {
            keyItemMap.put("test_list:k_" + e, Arrays.asList(""+e));
            timeMap.put("test_list:k_" + e, 120L);
        });
        long start = System.currentTimeMillis();
        stringRedisUtils.batchWriteRedisByList(keyItemMap, timeMap);

        System.out.println(String.format("总计耗时%sms:",System.currentTimeMillis() - start));
    }

    @Test
    public void testBatchWriteRedisByMap() throws InterruptedException {
        List<String> keys = Lists.newArrayList();
        Map<String, String> keyItemMap = Maps.newHashMap();


        IntStream.rangeClosed(1, 100000).forEach(e -> {
            keyItemMap.put("test_map:k_"+e , ""+e);
        });

        stringRedisUtils.batchWriteRedisByHash("test_map",keyItemMap, 120L);


    }
    @Test
    public void testBatchDelRedisForMap() throws InterruptedException {
        List<String> keys = Lists.newArrayList();


        IntStream.rangeClosed(1, 100000).forEach(e -> {
            keys.add("test_map:k_"+e );
        });

        stringRedisUtils.batchDelRedisForMap("test_map",keys);


    }

    @Test
    public void testRedisTTL() throws InterruptedException {
        /*stringRedisUtils.set("test____","v");
        stringRedisUtils.expire("test____",1000_000);

        stringRedisUtils.set("test____","v1");*/

        stringRedisUtils.lSet("test1____","v");
        stringRedisUtils.expire("test1____",1000_000);
        stringRedisUtils.lSet("test1____","v1");
    }
}
