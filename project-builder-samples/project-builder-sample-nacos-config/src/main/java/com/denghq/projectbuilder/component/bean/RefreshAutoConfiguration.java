package com.denghq.projectbuilder.component.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.context.refresh.ContextRefresher;
import org.springframework.cloud.context.scope.refresh.RefreshScope;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(RefreshScope.class)
@ConditionalOnProperty(name = "spring.cloud.refresh.enabled", matchIfMissing = true)
public class RefreshAutoConfiguration {


    @Autowired
    Ser ser;

    @Bean
    @ConditionalOnMissingBean(RefreshScope.class)
    public static RefreshScope refreshScope() {
        return new RefreshScope();
    }

    @Bean
    @ConditionalOnMissingBean
    public ContextRefresher contextRefresher(ConfigurableApplicationContext context,
                                             RefreshScope scope) {
        return new ContextRefresher(context, scope);
    }

    @Bean
    @ConditionalOnMissingBean
    public ConfigChangeListener configChangeListener(ContextRefresher contextRefresher) {
        return new ConfigChangeListener(ser, contextRefresher);
    }

    @Bean
    @ConditionalOnMissingBean
    public MyLoggingRebinder myLoggingRebinder() {
        return new MyLoggingRebinder(ser);
    }
}
