package com.denghq.projectbuilder.component.bean;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class SqlLog {
    private String logName;

}
