package com.denghq.projectbuilder.component.bean;


import com.denghq.projectbuilder.component.config.metadata.annotation.Category;
import com.denghq.projectbuilder.component.config.metadata.annotation.ConfigAttribute;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "ftp")
@Category(projectName= "基础配置", appNo= "smw", categoryNo= "ftpConfig", categoryName= "FTP服务配置")
public class FtpListConfig {
	
	@ConfigAttribute(name = "FTP服务列表")
	public List<FtpDetail> details;
}