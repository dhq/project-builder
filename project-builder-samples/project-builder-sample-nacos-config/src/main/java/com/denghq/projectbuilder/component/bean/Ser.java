package com.denghq.projectbuilder.component.bean;

import com.denghq.projectbuilder.component.config.metadata.annotation.Category;
import com.denghq.projectbuilder.component.config.metadata.annotation.ConfigAttribute;
import com.denghq.projectbuilder.component.config.metadata.enums.ConfigValueTypeEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Arrays.asList;

@Data
@Component
@ConfigurationProperties(prefix = "test")
@Category(projectName = "test-项目名-Ser", categoryNo = "test-testCategoryNo5", categoryName = "分类名称-categoryName1")
public class Ser {

    @ConfigAttribute(name = "属性名-myName")
    private String myName;

    @ConfigAttribute(name = "属性名-list")
    private List<String> list = asList("1", "22", "ddd");

    @ConfigAttribute(name = "属性名-beans",valueType = ConfigValueTypeEnum.DATA_LIST)
    private List<Bean> beans = asList(new Bean(), new Bean());

    @ConfigAttribute(name = "属性名-array")
    private Integer[] array = {1, 2, 3};


    public String getMyName() {
        return myName;
    }

    public void setMyName(String myName) {
        this.myName = myName;
    }

}
