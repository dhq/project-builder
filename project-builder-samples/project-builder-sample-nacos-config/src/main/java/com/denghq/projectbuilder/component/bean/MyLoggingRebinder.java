package com.denghq.projectbuilder.component.bean;

import org.springframework.boot.logging.LoggingSystem;
import org.springframework.cloud.context.environment.EnvironmentChangeEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;

/**
 * Listener that looks for {@link EnvironmentChangeEvent} and rebinds logger levels if any
 * changed.
 *
 * @author Dave Syer
 */
public class MyLoggingRebinder
        implements ApplicationListener<EnvironmentChangeEvent>, EnvironmentAware {

    private Ser ser;


    public MyLoggingRebinder(Ser ser) {
        this.ser = ser;

    }

    @Override
    public void onApplicationEvent(EnvironmentChangeEvent event) {

        LoggingSystem system = LoggingSystem.get(LoggingSystem.class.getClassLoader());
        System.out.println("------------------------------------------------------------------");
        System.out.println(ser);
        System.out.println("------------------------------------------------------------------");
    }


    @Override
    public void setEnvironment(Environment environment) {
        System.out.println("------------------------------------------------------------------");
        System.out.println(ser);
        System.out.println("------------------------------------------------------------------");
    }
}
