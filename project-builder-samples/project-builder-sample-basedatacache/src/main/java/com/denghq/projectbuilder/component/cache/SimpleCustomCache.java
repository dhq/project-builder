package com.denghq.projectbuilder.component.cache;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICustomCacheDomain;
import com.denghq.projectbuilder.component.basedatacache.cache.AbstractCacheDomain;
import org.springframework.stereotype.Component;

@Component
public class SimpleCustomCache extends AbstractCacheDomain<String> implements ICustomCacheDomain<String> {
    private Boolean loadSuccess = false;

    private String cacheData;

    @Override
    public String getCacheName() {
        return "customCacheDomain1_name";
    }

    @Override
    public String getCacheKey() {
        return "customCacheDomain1_type";
    }

    @Override
    public String getCacheData() {
        return cacheData;
    }

    @Override
    public Boolean getLoadSuccess() {
        return this.loadSuccess;
    }

    @Override
    public void setLoadSuccess(Boolean loadSuccess) {
        this.loadSuccess = loadSuccess;
    }

    @Override
    public String getTopicName() {
        return null;
    }

    @Override
    public int initLoad() {
        this.cacheData = "customCacheDomain1_cacheData";
        return 1;
    }

    @Override
    public int update(String msg) {
        this.cacheData = "customCacheDomain1_cacheData_"+System.currentTimeMillis();
        return 1;
    }
}
