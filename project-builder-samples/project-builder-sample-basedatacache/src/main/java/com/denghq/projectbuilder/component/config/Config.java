package com.denghq.projectbuilder.component.config;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICustomCacheDomain;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;

//@Configuration
public class Config {

    @Bean
    public ICustomCacheDomain customCacheDomain1(){

        return new ICustomCacheDomain() {

            private Boolean loadSuccess = false;

            private String cacheData;

            @Override
            public String getCacheName() {
                return "customCacheDomain1_name";
            }

            @Override
            public String getCacheKey() {
                return "customCacheDomain1_type";
            }

            @Override
            public Object getCacheData() {
                return cacheData;
            }

            @Override
            public Boolean getLoadSuccess() {
                return this.loadSuccess;
            }

            @Override
            public void setLoadSuccess(Boolean loadSuccess) {
                this.loadSuccess = loadSuccess;
            }

            @Override
            public String getTopicName() {
                return null;
            }

            @Override
            public int initLoad() {
                this.cacheData = "customCacheDomain1_cacheData";
                return 1;
            }

            @Override
            public int update(String msg) {
                this.cacheData = "customCacheDomain1_cacheData_"+System.currentTimeMillis();
                return 1;
            }
        };
    }

    //@Bean
    public ICustomCacheDomain customCacheDomain2(){
        return new ICustomCacheDomain() {

            private Boolean loadSuccess = false;

            private List<String> cacheData;

            @Override
            public String getCacheName() {
                return "customCacheDomain2_name";
            }

            @Override
            public String getCacheKey() {
                return "customCacheDomain2_type";
            }

            @Override
            public Object getCacheData() {
                return cacheData;
            }

            @Override
            public Boolean getLoadSuccess() {
                return this.loadSuccess;
            }

            @Override
            public void setLoadSuccess(Boolean loadSuccess) {
                this.loadSuccess = loadSuccess;
            }

            @Override
            public String getTopicName() {
                return null;
            }

            @Override
            public int initLoad() {
                this.cacheData = Arrays.asList("customCacheDomain2_cacheData1","customCacheDomain2_cacheData2");
                return 2;
            }

            @Override
            public int update(String msg) {
                return initLoad();
            }
        };
    }
}
