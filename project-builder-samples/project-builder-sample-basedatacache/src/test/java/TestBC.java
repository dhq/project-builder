import com.denghq.projectbuilder.component.App;
import com.denghq.projectbuilder.component.remote.model.UserRightViewModel;
import com.denghq.projectbuilder.component.remote.service.SysUserService;
import com.denghq.projectbuilder.component.basedatacache.cache.VehicleRedlistCache;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {App.class})// 指定启动类
public class TestBC {


    @Autowired
    private VehicleRedlistCache vehicleRedlistCache;

    @Autowired
    private SysUserService sysUserService;

    @Test
    public void testBaseDataCacheHolder() {

        UserRightViewModel userInfo = sysUserService.getUserInfo();
        System.out.println(vehicleRedlistCache.getLoadedCacheData());
        //baseDataCacheHolder.getAllCache().forEach(System.out::println);
    }
}
