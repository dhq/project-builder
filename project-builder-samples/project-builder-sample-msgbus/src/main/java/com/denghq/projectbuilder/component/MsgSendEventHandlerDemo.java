package com.denghq.projectbuilder.component;

import com.denghq.projectbuilder.component.msgbus.sender.IMsgSendEvent;
import com.denghq.projectbuilder.component.msgbus.sender.IMsgSendEventHandler;
import org.springframework.stereotype.Component;

@Component
public class MsgSendEventHandlerDemo implements IMsgSendEventHandler {


    /**
     * 消息丢失
     * @param event 消息事件
     */
    @Override
    public void onSuccessEvent(IMsgSendEvent event) {
        System.out.println("发送成功(到交换机)，msgId" + event.getMsgId());
        //根据msgId进行相关逻辑处理
    }

    @Override
    public void onFailEvent(IMsgSendEvent event) {
        System.out.println("发送失败(未发送到交换机)，msgId" + event.getMsgId());
        //根据msgId进行相关逻辑处理，比如重发
    }

    @Override
    public void onMissEvent(IMsgSendEvent event) {
        System.out.println("消息丢失(交换机未能发送到队列)，msgId" + event.getMsgId());
        //根据msgId进行相关逻辑处理
    }

}
