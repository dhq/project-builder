package com.denghq.projectbuilder.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
@Slf4j
public class App {
    public static void main(String[] args) {
        log.info("web 后台系统启动！！！");
        SpringApplication.run(App.class, args);
    }
}
