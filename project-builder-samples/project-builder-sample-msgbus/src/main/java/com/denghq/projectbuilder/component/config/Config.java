package com.denghq.projectbuilder.component.config;

import com.denghq.projectbuilder.component.msgbus.sender.IMsgSendEvent;
import com.denghq.projectbuilder.component.msgbus.sender.IMsgSendEventHandler;
import com.denghq.projectbuilder.component.msgbus.sender.impl.SimpleMsgSendEventHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    public IMsgSendEventHandler msgSendEventHandler() {

        return new SimpleMsgSendEventHandler() {
            @Override
            public void onSuccessEvent(IMsgSendEvent event) {
                System.out.println("test-event-handler>>>" + event);
            }
        };
    }
}
