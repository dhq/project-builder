package com.denghq.projectbuilder.component.config;

import com.denghq.projectbuilder.component.msgbus.annotation.RabbitMsgBusProcessor;
import com.denghq.projectbuilder.component.msgbus.bus.IMsgProcessor;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@RabbitMsgBusProcessor(topic = "topic2")
@Component
public class SimpleRabbitMsgBusProcessor implements IMsgProcessor{

    @Override
    public void execute(String topic, String msg) {
        Arrays.asList("SimpleRabbitMsgBusProcessor process msg:" + topic,msg).stream().forEach(System.out::println);
    }
}
