import com.denghq.projectbuilder.component.App;
import com.denghq.projectbuilder.component.amqp.MqChangeTypeEnum;
import com.denghq.projectbuilder.component.amqp.MsgDecorator;
import com.denghq.projectbuilder.component.amqp.MsgDecoratorData;
import com.denghq.projectbuilder.component.msgbus.bus.impl.RabbitMqMsgBus;
import com.denghq.projectbuilder.component.msgbus.bus.impl.RabbitMqMsgBusUtil;
import com.denghq.projectbuilder.component.msgbus.sender.impl.SimpleMsg;
import com.google.common.collect.Lists;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {App.class})// 指定启动类
public class TestMsgBus {

    @Autowired
    private RabbitMqMsgBus rabbitMqMsgBus;

    @Autowired
    private RabbitMqMsgBusUtil rabbitMqMsgBusUtil;

    //@Test
    public void testUtilSendMsg() {

        //构建消息内容
        List<MsgDecoratorData> data = Lists.newArrayList();
        data.add(new MsgDecoratorData(MqChangeTypeEnum.Insert.name(), "11111_demo"));
        MsgDecorator.Builder<MsgDecoratorData> msgBuilder = MsgDecorator.build();
        MsgDecorator<MsgDecoratorData> msg = msgBuilder
                .setData(data)
                .setDataType("TestRabbitMqMsgBusUtil send msg")
                .setReportedTime(new Date())
                .build();

        //发送消息
        rabbitMqMsgBusUtil.sendMsg("topic2", msg);

        //发送消息,携带消息id(用于做消息发送确认)
        rabbitMqMsgBusUtil.sendMsg("topic2","msgId",msg);
        //rabbitMqMsgBus.publish("topic2xx", new SimpleMsg("----msg----", "111"));
    }

   // @Test
    public void testSendMsg() {

        //发送消息
        rabbitMqMsgBus.publish("topic2xx", "111");

        //发送消息,携带消息id(用于做消息发送确认)
        rabbitMqMsgBus.publish("topic2xx", new SimpleMsg("----msg----", "111"));
    }

   // @Test
    public void testRecvMsg() {

        rabbitMqMsgBus.subscribe("topic2", (String topic, String msg) ->
            System.out.println("topic:" + topic + ",msg:" + msg)
        );
    }
}
