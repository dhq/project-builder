package com.denghq.projectbuilder.sample.singleweb.pojo.api;

import com.denghq.projectbuilder.component.mybatisplus.common.MybatisPlusPageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@ApiModel("Worker Information查询参数")
@EqualsAndHashCode(callSuper = false)
@Data
public class SlaveWorkerInfoSearchReq extends MybatisPlusPageReq {

    /**
     * 列表
     */
    @ApiModelProperty(" 列表")
    private List<Integer> idList;
    /**
     *
     */
    @ApiModelProperty("")
    private Integer id;
    /**
     *
     */
    @ApiModelProperty("")
    private String relayLogName;
    /**
     *
     */
    @ApiModelProperty("")
    private Long relayLogPos;
    /**
     *
     */
    @ApiModelProperty("")
    private String masterLogName;
    /**
     *
     */
    @ApiModelProperty("")
    private Long masterLogPos;
    /**
     *
     */
    @ApiModelProperty("")
    private String checkpointRelayLogName;
    /**
     *
     */
    @ApiModelProperty("")
    private Long checkpointRelayLogPos;
    /**
     *
     */
    @ApiModelProperty("")
    private String checkpointMasterLogName;
    /**
     *
     */
    @ApiModelProperty("")
    private Long checkpointMasterLogPos;
    /**
     *
     */
    @ApiModelProperty("")
    private Integer checkpointSeqno;
    /**
     *
     */
    @ApiModelProperty("")
    private Integer checkpointGroupSize;
 

}
