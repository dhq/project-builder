package com.denghq.projectbuilder.sample.singleweb.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.denghq.projectbuilder.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Worker Information
 *
 * @author denghq
 * @email
 * @date 2018/10/29
 */
@TableName("slave_worker_info")
@ApiModel("Worker Information")
public class SlaveWorkerInfoEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "Id", type = IdType.UUID)
    @NotNull(message = "不能为空", groups = {UpdateGroup.class})
    @ApiModelProperty("")
    @TableField("Id")
    private Integer id;
    /**
     *
     */
    @ApiModelProperty("")
    @TableField("Relay_log_name")
    private String relayLogName;
    /**
     *
     */
    @ApiModelProperty("")
    @TableField("Relay_log_pos")
    private Long relayLogPos;
    /**
     *
     */
    @ApiModelProperty("")
    @TableField("Master_log_name")
    private String masterLogName;
    /**
     *
     */
    @ApiModelProperty("")
    @TableField("Master_log_pos")
    private Long masterLogPos;
    /**
     *
     */
    @ApiModelProperty("")
    @TableField("Checkpoint_relay_log_name")
    private String checkpointRelayLogName;
    /**
     *
     */
    @ApiModelProperty("")
    @TableField("Checkpoint_relay_log_pos")
    private Long checkpointRelayLogPos;
    /**
     *
     */
    @ApiModelProperty("")
    @TableField("Checkpoint_master_log_name")
    private String checkpointMasterLogName;
    /**
     *
     */
    @ApiModelProperty("")
    @TableField("Checkpoint_master_log_pos")
    private Long checkpointMasterLogPos;
    /**
     *
     */
    @ApiModelProperty("")
    @TableField("Checkpoint_seqno")
    private Integer checkpointSeqno;
    /**
     *
     */
    @ApiModelProperty("")
    @TableField("Checkpoint_group_size")
    private Integer checkpointGroupSize;


    /**
     * 设置：
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置：
     */
    public void setRelayLogName(String relayLogName) {
        this.relayLogName = relayLogName;
    }

    /**
     * 获取：
     */
    public String getRelayLogName() {
        return relayLogName;
    }

    /**
     * 设置：
     */
    public void setRelayLogPos(Long relayLogPos) {
        this.relayLogPos = relayLogPos;
    }

    /**
     * 获取：
     */
    public Long getRelayLogPos() {
        return relayLogPos;
    }

    /**
     * 设置：
     */
    public void setMasterLogName(String masterLogName) {
        this.masterLogName = masterLogName;
    }

    /**
     * 获取：
     */
    public String getMasterLogName() {
        return masterLogName;
    }

    /**
     * 设置：
     */
    public void setMasterLogPos(Long masterLogPos) {
        this.masterLogPos = masterLogPos;
    }

    /**
     * 获取：
     */
    public Long getMasterLogPos() {
        return masterLogPos;
    }

    /**
     * 设置：
     */
    public void setCheckpointRelayLogName(String checkpointRelayLogName) {
        this.checkpointRelayLogName = checkpointRelayLogName;
    }

    /**
     * 获取：
     */
    public String getCheckpointRelayLogName() {
        return checkpointRelayLogName;
    }

    /**
     * 设置：
     */
    public void setCheckpointRelayLogPos(Long checkpointRelayLogPos) {
        this.checkpointRelayLogPos = checkpointRelayLogPos;
    }

    /**
     * 获取：
     */
    public Long getCheckpointRelayLogPos() {
        return checkpointRelayLogPos;
    }

    /**
     * 设置：
     */
    public void setCheckpointMasterLogName(String checkpointMasterLogName) {
        this.checkpointMasterLogName = checkpointMasterLogName;
    }

    /**
     * 获取：
     */
    public String getCheckpointMasterLogName() {
        return checkpointMasterLogName;
    }

    /**
     * 设置：
     */
    public void setCheckpointMasterLogPos(Long checkpointMasterLogPos) {
        this.checkpointMasterLogPos = checkpointMasterLogPos;
    }

    /**
     * 获取：
     */
    public Long getCheckpointMasterLogPos() {
        return checkpointMasterLogPos;
    }

    /**
     * 设置：
     */
    public void setCheckpointSeqno(Integer checkpointSeqno) {
        this.checkpointSeqno = checkpointSeqno;
    }

    /**
     * 获取：
     */
    public Integer getCheckpointSeqno() {
        return checkpointSeqno;
    }

    /**
     * 设置：
     */
    public void setCheckpointGroupSize(Integer checkpointGroupSize) {
        this.checkpointGroupSize = checkpointGroupSize;
    }

    /**
     * 获取：
     */
    public Integer getCheckpointGroupSize() {
        return checkpointGroupSize;
    }


}
