package com.denghq.projectbuilder.sample.singleweb.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.denghq.projectbuilder.sample.singleweb.entity.SlaveWorkerInfoEntity;
import com.denghq.projectbuilder.sample.singleweb.pojo.api.SlaveWorkerInfoInfoResp;
import com.denghq.projectbuilder.sample.singleweb.pojo.api.SlaveWorkerInfoSearchReq;
import com.denghq.projectbuilder.sample.singleweb.pojo.api.SlaveWorkerInfoSearchResp;
import com.denghq.projectbuilder.sample.singleweb.pojo.api.SlaveWorkerInfoUpdateOrSaveReq;

import java.util.List;

/**
 * Worker Information
 *
 * @author denghq
 * @email 
 * @date 2018/10/29
 */
public interface SlaveWorkerInfoService extends IService<SlaveWorkerInfoEntity> {

    /**
    	 * 条件查询总记录数
     *
     * @param query
     * @return
     */
    Long searchCount(SlaveWorkerInfoSearchReq query);

    Page<SlaveWorkerInfoSearchResp> search(SlaveWorkerInfoSearchReq query);

	/**
	 * 保存Worker Information
	 * @param slaveWorkerInfo
	 */
	void save(SlaveWorkerInfoUpdateOrSaveReq slaveWorkerInfo);

	/**
	 * 修改Worker Information
	 * @param slaveWorkerInfo
	 */
	void update(SlaveWorkerInfoUpdateOrSaveReq slaveWorkerInfo);

	/**
	 * 批量删除Worker Information
	 * @param idList
	 */
	void deleteBatch(List<String> idList);

	/**
	 * 显示Worker Information详情
	 * @param id
	 * @return
	 */
	SlaveWorkerInfoInfoResp info(String id);
}

