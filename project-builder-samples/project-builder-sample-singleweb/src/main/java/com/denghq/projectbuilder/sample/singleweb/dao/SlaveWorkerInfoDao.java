package com.denghq.projectbuilder.sample.singleweb.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.denghq.projectbuilder.sample.singleweb.entity.SlaveWorkerInfoEntity;
import com.denghq.projectbuilder.sample.singleweb.pojo.api.SlaveWorkerInfoSearchReq;
import com.denghq.projectbuilder.sample.singleweb.pojo.api.SlaveWorkerInfoSearchResp;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Worker Information
 *
 * @author denghq
 * @date 2018/10/29
 */
@Mapper
public interface SlaveWorkerInfoDao extends BaseMapper<SlaveWorkerInfoEntity> {

    Long searchCount(SlaveWorkerInfoSearchReq query);

    List<SlaveWorkerInfoSearchResp> search(Page<SlaveWorkerInfoSearchResp> page, SlaveWorkerInfoSearchReq query);

    List<SlaveWorkerInfoSearchResp> search(SlaveWorkerInfoSearchReq query);
}