package com.denghq.projectbuilder.sample.singleweb;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@MapperScan(basePackages = "com.denghq.projectbuilder.sample.singleweb.dao")
@Slf4j
public class App {
    public static void main(String[] args) {
        log.info("web 后台系统启动！！！");
        SpringApplication.run(App.class, args);
    }
}
