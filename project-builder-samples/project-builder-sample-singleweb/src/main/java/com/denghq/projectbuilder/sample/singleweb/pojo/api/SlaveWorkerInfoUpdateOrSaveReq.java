package com.denghq.projectbuilder.sample.singleweb.pojo.api;


import com.denghq.projectbuilder.common.validator.group.AddGroup;
import com.denghq.projectbuilder.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel("Worker Information修改或保存表单对象")
@Data
@EqualsAndHashCode(callSuper = false)
public class SlaveWorkerInfoUpdateOrSaveReq implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @NotNull(message = "id不能为空", groups = {UpdateGroup.class, AddGroup.class})
    @ApiModelProperty("id")
    private Integer id;
    /**
     *
     */
    @NotBlank(message = "relayLogName不能为空", groups = {AddGroup.class})
    @ApiModelProperty("relayLogName")
    private String relayLogName;
    /**
     *
     */
    @ApiModelProperty("")
    private Long relayLogPos;
    /**
     *
     */
    @ApiModelProperty("")
    private String masterLogName;
    /**
     *
     */
    @ApiModelProperty("")
    private Long masterLogPos;
    /**
     *
     */
    @ApiModelProperty("")
    private String checkpointRelayLogName;
    /**
     *
     */
    @ApiModelProperty("")
    private Long checkpointRelayLogPos;
    /**
     *
     */
    @ApiModelProperty("")
    private String checkpointMasterLogName;
    /**
     *
     */
    @ApiModelProperty("")
    private Long checkpointMasterLogPos;
    /**
     *
     */
    @ApiModelProperty("")
    private Integer checkpointSeqno;
    /**
     *
     */
    @ApiModelProperty("")
    private Integer checkpointGroupSize;


}