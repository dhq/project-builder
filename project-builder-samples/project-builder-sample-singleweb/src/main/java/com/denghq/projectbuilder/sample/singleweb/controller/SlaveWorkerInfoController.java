package com.denghq.projectbuilder.sample.singleweb.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.denghq.projectbuilder.common.ResultInfo;
import com.denghq.projectbuilder.common.validator.ValidatorUtils;
import com.denghq.projectbuilder.common.validator.group.AddGroup;
import com.denghq.projectbuilder.common.validator.group.UpdateGroup;
import com.denghq.projectbuilder.sample.singleweb.pojo.api.SlaveWorkerInfoInfoResp;
import com.denghq.projectbuilder.sample.singleweb.pojo.api.SlaveWorkerInfoSearchReq;
import com.denghq.projectbuilder.sample.singleweb.pojo.api.SlaveWorkerInfoSearchResp;
import com.denghq.projectbuilder.sample.singleweb.pojo.api.SlaveWorkerInfoUpdateOrSaveReq;
import com.denghq.projectbuilder.sample.singleweb.service.SlaveWorkerInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

/**
 * Worker Information
 *
 * @author denghq
 * @date 2018/10/29
 */
@Api(tags = {"Worker Information"})
@RestController
@RequestMapping("/tst/slaveWorkerInfo")
public class SlaveWorkerInfoController {
    @Autowired
    private SlaveWorkerInfoService slaveWorkerInfoService;

    /**
     * 条件查询总数
     */
    @RequestMapping(value = "/searchCount", method = RequestMethod.POST)
    @ApiOperation(value = "条件查询，查询总数", notes = "根据查询条件查询Worker Information总条数")
    public ResultInfo<Long> searchCount(@RequestBody SlaveWorkerInfoSearchReq query) {
        Long count = slaveWorkerInfoService.searchCount(query);
        return ResultInfo.success(count, 1, 1);
    }

    /**
     * 条件查询
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "条件查询Worker Information，分页", notes = "根据查询条件查询Worker Information")
    public ResultInfo<List<SlaveWorkerInfoSearchResp>> search(@RequestBody SlaveWorkerInfoSearchReq query) {
        Page<SlaveWorkerInfoSearchResp> page = slaveWorkerInfoService.search(query);
        return ResultInfo.successForPage(page.getRecords(), page.getTotal(), page.getPages());
    }

    /**
     * 详情
     */
    @ApiOperation(value = "Worker Information详情", notes = "Worker Information详情")
    @ApiImplicitParams({@ApiImplicitParam(paramType = "query", dataType = "String", name = "id", value = "Worker Information主键", required = true)})
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public ResultInfo<SlaveWorkerInfoInfoResp> info(String id) {

        SlaveWorkerInfoInfoResp slaveWorkerInfo = slaveWorkerInfoService.info(id);

        return ResultInfo.success(slaveWorkerInfo);
    }

    /**
     * 保存
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation(value = "保存Worker Information", notes = "保存Worker Information")
    public ResultInfo<SlaveWorkerInfoUpdateOrSaveReq> save(@Validated(AddGroup.class) @RequestBody SlaveWorkerInfoUpdateOrSaveReq slaveWorkerInfo) {
        //ValidatorUtils.validateEntity(slaveWorkerInfo, AddGroup.class);
        slaveWorkerInfoService.save(slaveWorkerInfo);
        return ResultInfo.success(slaveWorkerInfo);
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ApiOperation(value = "修改Worker Information", notes = "修改Worker Information")
    public ResultInfo<SlaveWorkerInfoUpdateOrSaveReq> update(@Validated(UpdateGroup.class) @RequestBody SlaveWorkerInfoUpdateOrSaveReq slaveWorkerInfo) {
        ValidatorUtils.validateEntity(slaveWorkerInfo, UpdateGroup.class);
        slaveWorkerInfoService.update(slaveWorkerInfo);
        return ResultInfo.success(slaveWorkerInfo);
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ApiOperation(value = "删除Worker Information", notes = "删除Worker Information")
    public ResultInfo<String[]> delete(@RequestBody String[] ids) {

        slaveWorkerInfoService.deleteBatch(Arrays.asList(ids));
        return ResultInfo.success(ids);
    }

}
