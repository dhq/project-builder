package com.denghq.projectbuilder.sample.singleweb.pojo.api;


import com.denghq.projectbuilder.sample.singleweb.entity.SlaveWorkerInfoEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@ApiModel("Worker Information详细信息")
@Data
@EqualsAndHashCode(callSuper = false)
public class SlaveWorkerInfoInfoResp extends SlaveWorkerInfoEntity {

    private static final long serialVersionUID = 1L;

}

