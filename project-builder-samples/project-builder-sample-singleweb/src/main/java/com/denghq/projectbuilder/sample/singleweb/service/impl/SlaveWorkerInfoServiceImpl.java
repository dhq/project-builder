package com.denghq.projectbuilder.sample.singleweb.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.denghq.projectbuilder.common.CodeMsg;
import com.denghq.projectbuilder.common.exception.BussinessException;
import com.denghq.projectbuilder.sample.singleweb.dao.SlaveWorkerInfoDao;
import com.denghq.projectbuilder.sample.singleweb.entity.SlaveWorkerInfoEntity;
import com.denghq.projectbuilder.sample.singleweb.pojo.api.SlaveWorkerInfoInfoResp;
import com.denghq.projectbuilder.sample.singleweb.pojo.api.SlaveWorkerInfoSearchReq;
import com.denghq.projectbuilder.sample.singleweb.pojo.api.SlaveWorkerInfoSearchResp;
import com.denghq.projectbuilder.sample.singleweb.pojo.api.SlaveWorkerInfoUpdateOrSaveReq;
import com.denghq.projectbuilder.sample.singleweb.service.SlaveWorkerInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;


@Transactional
@Service("slaveWorkerInfoService")
public class SlaveWorkerInfoServiceImpl extends ServiceImpl<SlaveWorkerInfoDao, SlaveWorkerInfoEntity> implements SlaveWorkerInfoService {

    @Override
    public Long searchCount(SlaveWorkerInfoSearchReq query) {
        return this.baseMapper.searchCount(query);
    }

    @Override
    public Page<SlaveWorkerInfoSearchResp> search(SlaveWorkerInfoSearchReq query) {

        if (query.getIsPagination() != null && query.getIsPagination()) {
            Page<SlaveWorkerInfoSearchResp> page = query.getPage();
            List<SlaveWorkerInfoSearchResp> list = this.baseMapper.search(page, query);
            //searchFilter(list);
            return page.setRecords(list);
        } else {
            List<SlaveWorkerInfoSearchResp> list = this.baseMapper.search(query);
            //searchFilter(list);
            Page<SlaveWorkerInfoSearchResp> page = new Page<SlaveWorkerInfoSearchResp>();
            page.setTotal(list.size());
            return page.setRecords(list);
        }

    }

    public void searchFilter(List<SlaveWorkerInfoSearchResp> list) {
        if (!CollectionUtils.isEmpty(list)) {
            //字段转换，比如字典转换成中文
        }

    }

    @Override
    public void save(SlaveWorkerInfoUpdateOrSaveReq req) {
        //唯一性校验，比如说名称不能重复
        //this.checkForKey(req);
        //req.setCreateTime(new Date());
        //req.setCreateBy(UserContextHolder.getUserId());
        SlaveWorkerInfoEntity e = new SlaveWorkerInfoEntity();
        BeanUtils.copyProperties(req, e);
        this.insert(e);
        req.setId(e.getId());
    }

    @Override
    public void update(SlaveWorkerInfoUpdateOrSaveReq req) {

        //唯一性校验，比如说名称不能重复
        //this.checkForKey(req);
        //req.setUpdateTime(new Date());
        //req.setUpdateUser(UserContextHolder.getUserId());
        SlaveWorkerInfoEntity e = this.selectById(req.getId());
        if (e == null) {
            throw new BussinessException(CodeMsg.PARAMETER_ERROR, "记录不存在！");
        }
        BeanUtils.copyProperties(req, e);
        this.updateAllColumnById(e);
    }

    private void checkForKey(SlaveWorkerInfoUpdateOrSaveReq req) {
		/* demo
		Wrapper<SlaveWorkerInfoEntity> wrapper = new EntityWrapper<SlaveWorkerInfoEntity>()
                        .eq("is_delete", 0)
                        .andNew()
                        .eq("platform_name", req.getPlatformName())
                        .or()
                        .eq("platform_code", req.getPlatformCode());

                if (StringUtils.isNotBlank(req.getId())) {
                    wrapper.andNew()
                            .ne("Id", req.getId());
                }

                int selectCount = this.selectCount(wrapper);
                if (selectCount > 0) {
                    throw new BussinessException(CodeMsg.PARAMETER_ERROR, "平台名称或者平台编号重复！");
                }

                //select * from tbl_platform_access where (platform_name = #{platformName} or platform_code = #{platformCode}) and is_delete = 0
		*/

    }

    @Override
    public void deleteBatch(List<String> idList) {
        //TODO 数据校验
        //批量删除
        this.deleteBatchIds(idList);
    }

    @Override
    public SlaveWorkerInfoInfoResp info(String id) {
        SlaveWorkerInfoEntity entity = this.selectById(id);
        if (entity == null) {
            return null;
        }
        //字段内容转换，比如字典转中文
        //infoFilter(entity);
        SlaveWorkerInfoInfoResp resp = new SlaveWorkerInfoInfoResp();
        BeanUtils.copyProperties(entity, resp);
        return resp;
    }

    @SuppressWarnings("unused")
    private void infoFilter(SlaveWorkerInfoEntity entity) {
		/*if(entity != null) {
			//字典过滤
	    	List<DictModel> dicts = sysDictService.getCompanyType();
	    	if(dicts!=null) {
				dicts.forEach(d->{
	    			if(d.getDictionaryNo().equals(entity.getCompanytype())) {
	    				entity.setCompanytype(d.getDictionaryValue());
	    			}
	    		});
			}
		}
		*/
    }

}
