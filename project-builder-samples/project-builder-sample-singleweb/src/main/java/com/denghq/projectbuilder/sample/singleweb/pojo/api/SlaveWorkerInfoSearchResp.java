package com.denghq.projectbuilder.sample.singleweb.pojo.api;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("Worker Information查询结果")
@Data
public class SlaveWorkerInfoSearchResp{
	
	
	/**
	 * 
	 */
	@ApiModelProperty("")
	private Integer id;
	
	/**
	 * 
	 */
	@ApiModelProperty("")
	private String relayLogName;
	
	/**
	 * 
	 */
	@ApiModelProperty("")
	private Long relayLogPos;
	
	/**
	 * 
	 */
	@ApiModelProperty("")
	private String masterLogName;
	
	/**
	 * 
	 */
	@ApiModelProperty("")
	private Long masterLogPos;
	
	/**
	 * 
	 */
	@ApiModelProperty("")
	private String checkpointRelayLogName;
	
	/**
	 * 
	 */
	@ApiModelProperty("")
	private Long checkpointRelayLogPos;
	
	/**
	 * 
	 */
	@ApiModelProperty("")
	private String checkpointMasterLogName;
	
	/**
	 * 
	 */
	@ApiModelProperty("")
	private Long checkpointMasterLogPos;
	
	/**
	 * 
	 */
	@ApiModelProperty("")
	private Integer checkpointSeqno;
	
	/**
	 * 
	 */
	@ApiModelProperty("")
	private Integer checkpointGroupSize;

	
}
