import com.denghq.projectbuilder.component.App;
import com.denghq.projectbuilder.component.remote.model.UserRightViewModel;
import com.denghq.projectbuilder.component.remote.service.SysUserService;
import com.denghq.projectbuilder.component.kafka.KafkaMessageConsumer;
import com.denghq.projectbuilder.component.kafka.KafkaMessageSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {App.class})// 指定启动类
public class TestKafka {

    @Autowired
    private KafkaMessageSender kafkaMessageSender;

    @Autowired
    private KafkaMessageConsumer kafkaMessageConsumer;

    @Autowired
    private SysUserService sysUserService;

    @Test
    public void testKafka() throws InterruptedException {
        kafkaMessageSender.send("testt111");
        Thread.sleep(1000L);
    }

    @Test
    public void testUserService() throws InterruptedException {
        UserRightViewModel userInfo = sysUserService.getUserInfo();
        System.out.println(userInfo);
    }
}
