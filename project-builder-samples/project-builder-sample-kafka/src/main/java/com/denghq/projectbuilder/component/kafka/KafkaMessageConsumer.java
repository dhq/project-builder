package com.denghq.projectbuilder.component.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;


@Component
public class KafkaMessageConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaMessageConsumer.class);

    /**
     * 接受消息
     * @param message
     * @param headers
     */
    @KafkaListener(topics={"${spring.kafka.app.topic.test}"})
    public void receiveParsedData(@Payload String message, @Headers MessageHeaders headers){
        LOG.info("com.denghq.kafka.KafkaMessageConsumer 接收到消息："+message);
        headers.keySet().forEach(key->LOG.info("{}: {}",key,headers.get(key)));
    }
}