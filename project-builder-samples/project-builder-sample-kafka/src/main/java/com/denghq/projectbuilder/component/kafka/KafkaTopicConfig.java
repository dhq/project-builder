package com.denghq.projectbuilder.component.kafka;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "spring.kafka.app.topic")
@Component
@Data
public class KafkaTopicConfig {

    public String test;
}
