package com.denghq.projectbuilder.component.mq;

/**
 * 	是否已删除
 * 
 * 	DeletedState 状态
 * 	@author denghq
 *
 */
public enum MqDataTypeEnum {

	SpottingChanged("路口信息变更"),FrontDeviceChanged("路面设备信息变更"), TestChanged("测试信息变更");

	private String description;

	public String getDescription() {
		return description;
	}

	private MqDataTypeEnum(String description) {
		this.description = description;
	} 
	
}
