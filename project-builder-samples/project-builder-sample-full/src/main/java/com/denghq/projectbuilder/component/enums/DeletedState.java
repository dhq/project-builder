package com.denghq.projectbuilder.component.enums;

/**
 * 	是否已删除
 * 
 * 	DeletedState 状态
 * 	@author denghq
 *
 */
public enum DeletedState {

	y("yes"),n("no");

	private String description;

	public String getDescription() {
		return description;
	}

	private DeletedState(String description) {
		this.description = description;
	} 
	
	/*public static void main(String[] args) {
		System.out.println(OptState.DELETE.description);
	}*/ 
}
