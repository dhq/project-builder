package com.denghq.projectbuilder.component.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.denghq.projectbuilder.component.remote.config.properties.SysDictKindProp;
import com.denghq.projectbuilder.component.remote.model.DictModel;
import com.denghq.projectbuilder.component.remote.service.SysDictService;
import com.denghq.projectbuilder.component.mq.MessageQueueUtils;
import com.denghq.projectbuilder.component.mq.MqDataTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.denghq.projectbuilder.component.amqp.MqChangeTypeEnum;
import com.denghq.projectbuilder.component.amqp.MsgDecorator;
import com.denghq.projectbuilder.component.amqp.MsgDecorator.Builder;
import com.denghq.projectbuilder.component.amqp.MsgDecoratorData;
import com.google.common.collect.Lists;

@Controller
@RequestMapping("/test")
public class TestController {
	
	@Autowired
    MessageQueueUtils messageQueueUtils;

	@Autowired
    SysDictService dictService;

	@Autowired
	private SysDictKindProp sysDictKindProp;
	
	@RequestMapping("/fileUpload")
	public void fileUpload(@RequestParam("files") MultipartFile[] file,HttpServletResponse res,HttpServletRequest req) throws IOException {
		
		Enumeration<String> headerNames = req.getHeaderNames();
		for (;headerNames.hasMoreElements();) {
			String nextElement = headerNames.nextElement();
			System.out.println(nextElement+":"+req.getHeader(nextElement));
		}
		System.out.println(req.getParameterMap());

		if (null != file && file.length > 0) {
			// 遍历并保存文件
			for (MultipartFile files : file) {
				if (file != null) {
					// 取得当前上传文件的文件名称
					String fileName = files.getOriginalFilename();
					System.out.println(fileName);
					res.getWriter().write(fileName);
				}
			}
		} else {
			//System.out.println("error");
			res.getWriter().write("error");
		}
	}

	@RequestMapping("mq")
	public ResponseEntity<Void> mq() {
		/**
		 * demo1
		 */
		/*List<MsgDecoratorData> data = Lists.newArrayList();
		data.add(new MsgDecoratorData(MqChangeTypeEnum.Insert.name(), "11111_demo"));
		MsgDecorator<MsgDecoratorData> msg = new MsgDecorator<>(MqDataTypeEnum.TestChanged.name(),data,new Date());
		messageQueueUtils.sendTestMsg(msg);
		return ResponseEntity.ok().build();*/
		
		/**
		 * demo2
		 */
		List<MsgDecoratorData> data = Lists.newArrayList();
		data.add(new MsgDecoratorData(MqChangeTypeEnum.Insert.name(), "11111_demo"));
		
		Builder<MsgDecoratorData> msgBuilder = MsgDecorator.build();
		MsgDecorator<MsgDecoratorData> msg = msgBuilder
			.setData(data)
			.setDataType(MqDataTypeEnum.TestChanged.name())
			.setReportedTime(new Date()).build();
		
		messageQueueUtils.sendTestMsg(msg);
		return ResponseEntity.ok().build();
		
	}
	
	@RequestMapping("dict")
	public ResponseEntity<Map<String, List<DictModel>>> dict(){
		
		Map<String, List<DictModel>> someDicts = dictService.getSomeDicts(Arrays.asList(
				sysDictKindProp.getAreaCode()
				,sysDictKindProp.getCompanyType()));
		return ResponseEntity.ok().body(someDicts);
	}
}
