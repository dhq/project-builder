package com.denghq.projectbuilder.component.mq;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "spring.rabbitmq.routingkey")
public class RoutingKeyProperties {
	
    private String test;
    
}
