package com.denghq.projectbuilder.component.enums;

import java.math.BigDecimal;

public enum ApproveStatusEnum {
	
	NO("未审核"),PASS("审核通过"),REJECTED("审核驳回");
	
	private String name;
	
	ApproveStatusEnum(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}

	public static String getName(BigDecimal status){

		if(status!=null) {
			if(status.intValue() == 0) {
				return ApproveStatusEnum.NO.getName();
			}else if(status.intValue() == 1) {
				return ApproveStatusEnum.PASS.getName();
			}else if(status.intValue() == 2) {
				return ApproveStatusEnum.REJECTED.getName();
			}
		}
		//0:未审核, 1:审核通过, 2:审核未通过
		return ApproveStatusEnum.NO.getName();

	}
}
