package com.denghq.projectbuilder.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableDiscoveryClient
@Slf4j
public class App {
    public static void main(String[] args) {
        log.info("web 后台系统启动！！！");
        ConfigurableApplicationContext applicationContext = SpringApplication.run(App.class, args);
    }
}
