package com.denghq.projectbuilder.component.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.denghq.projectbuilder.common.validator.group.AddGroup;
import com.denghq.projectbuilder.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 道路信息
 * 
 * @author denghq
 * @email 
 * @date 2018/10/29
 */
@TableName("common_road")
@ApiModel("道路信息")
public class CommonRoadEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 道路ID-主键
	 */
	@TableId(type = IdType.UUID)
	@NotBlank(message="道路ID-主键不能为空",groups = {UpdateGroup.class})
	@ApiModelProperty("道路ID-主键")
	private String roadid;
	/**
	 * 道路编号-对应六合一道路代码
	 */
	@ApiModelProperty("道路编号-对应六合一道路代码")
	@NotBlank(message="道路编号不能为空",groups = {AddGroup.class})
	private String roadno;
	/**
	 * 道路名称
	 */
	@ApiModelProperty("道路名称")
	@NotBlank(message="道路名称不能为空",groups = {AddGroup.class})
	private String roadname;
	/**
	 * 道路类型
	 */
	@ApiModelProperty("道路类型")
	private String roadtypeid;
	/**
	 * 是否双向车道（1：是 0：否 默认 1）
	 */
	@ApiModelProperty("是否双向车道（1：是 0：否 默认 1）")
	private BigDecimal istwoway;
	/**
	 * 创建人
	 */
	@ApiModelProperty("创建人")
	private String creator;
	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	private Date createdtime;
	/**
	 * 修改人
	 */
	@ApiModelProperty("修改人")
	private String modifier;
	/**
	 * 修改时间
	 */
	@ApiModelProperty("修改时间")
	private Date modifiedtime;
	/**
	 * 备注
	 */
	@ApiModelProperty("备注")
	private String remark;

	/**
	 * 设置：道路ID-主键
	 */
	public void setRoadid(String roadid) {
		this.roadid = roadid;
	}
	/**
	 * 获取：道路ID-主键
	 */
	public String getRoadid() {
		return roadid;
	}
	/**
	 * 设置：道路编号-对应六合一道路代码
	 */
	public void setRoadno(String roadno) {
		this.roadno = roadno;
	}
	/**
	 * 获取：道路编号-对应六合一道路代码
	 */
	public String getRoadno() {
		return roadno;
	}
	/**
	 * 设置：道路名称
	 */
	public void setRoadname(String roadname) {
		this.roadname = roadname;
	}
	/**
	 * 获取：道路名称
	 */
	public String getRoadname() {
		return roadname;
	}
	/**
	 * 设置：道路类型
	 */
	public void setRoadtypeid(String roadtypeid) {
		this.roadtypeid = roadtypeid;
	}
	/**
	 * 获取：道路类型
	 */
	public String getRoadtypeid() {
		return roadtypeid;
	}
	/**
	 * 设置：是否双向车道（1：是 0：否 默认 1）
	 */
	public void setIstwoway(BigDecimal istwoway) {
		this.istwoway = istwoway;
	}
	/**
	 * 获取：是否双向车道（1：是 0：否 默认 1）
	 */
	public BigDecimal getIstwoway() {
		return istwoway;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreator() {
		return creator;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatedtime(Date createdtime) {
		this.createdtime = createdtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreatedtime() {
		return createdtime;
	}
	/**
	 * 设置：修改人
	 */
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	/**
	 * 获取：修改人
	 */
	public String getModifier() {
		return modifier;
	}
	/**
	 * 设置：修改时间
	 */
	public void setModifiedtime(Date modifiedtime) {
		this.modifiedtime = modifiedtime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getModifiedtime() {
		return modifiedtime;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
}
