package com.denghq.projectbuilder.component;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.denghq.projectbuilder.common.ResultInfo;
import com.denghq.projectbuilder.component.dao.CommonRoadDao;
import com.denghq.projectbuilder.component.entity.CommonRoadEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 道路信息
 *
 * @author denghq
 * @date 2018/10/29
 */
@Api(tags = {"道路"})
@RestController
@RequestMapping("/fmw/commonRoad")
@Slf4j
public class CommonRoadController {

    @Autowired
    private CommonRoadDao commonRoadDao;

    /**
     * 条件查询
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ApiOperation(value = "查询所有道路信息", notes = "查询所有道路信息")
    public ResultInfo<List<CommonRoadEntity>> search() {
        log.debug("aaaaaaaaaaaaaaa");
        List<CommonRoadEntity> commonRoadEntities = commonRoadDao.selectList(new EntityWrapper<>());
        return ResultInfo.success(commonRoadEntities);
    }

}
