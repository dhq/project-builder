package com.denghq.projectbuilder.component.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.denghq.projectbuilder.component.entity.CommonRoadEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 道路信息
 * 
 * @author denghq
 * @date 2018/10/29
 */
@Mapper
public interface CommonRoadDao extends BaseMapper<CommonRoadEntity> {

}
