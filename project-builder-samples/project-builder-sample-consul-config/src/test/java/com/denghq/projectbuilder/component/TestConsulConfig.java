package com.denghq.projectbuilder.component;

import com.denghq.projectbuilder.component.pojo.Bean;
import com.denghq.projectbuilder.component.pojo.FtpListConfig;
import com.denghq.projectbuilder.component.pojo.Ser;
import com.denghq.projectbuilder.component.pojo.TDMProperties;
import com.denghq.projectbuilder.component.consul.config.configuration.ConsulConfigManager;
import com.denghq.projectbuilder.component.config.domain.ConfigClassParser;
import com.denghq.projectbuilder.component.config.metadata.ConfigDescriptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {App.class})// 指定启动类
public class TestConsulConfig {

    @Autowired
    private ConfigClassParser parser;

    @Autowired
    private Ser ser;

    @Autowired
    private TDMProperties tdmProperties;

    private String v;

    @Autowired
    private FtpListConfig ftpListConfig;

    @Autowired
    private ConsulConfigManager configManager;

    @Test
    public void testParser() {

        System.out.println(1111);
        List<ConfigDescriptor> configDescriptorList = parser.getConfigDescriptorListList();
        parser.getConfigDescriptorListList().stream().forEach(System.out::print);
    }

    @Test
    public void testSpring() throws InterruptedException {
        System.out.println(tdmProperties);
        System.out.println(ser);
        System.out.println(ser.getBeans().get(0).getMyName());
        System.out.println(ser.getBeans().get(0).getList().get(0));
        Thread.sleep(1000L);
        System.out.println(tdmProperties);
    }


    @Test
    public void testClassScan() throws InterruptedException {
        System.out.println(ser);
        Thread.sleep(10000L);
        System.out.println(ser);
    }

    @Test
    public void testConfigManager() throws InterruptedException {
        System.out.println(ser);
        Bean b = new Bean();
        b.setMyName("邓浩钦");
        b.setList(Arrays.asList("d", "h", "q"));
        ser.setBeans(Arrays.asList(b));
        configManager.writeConfigDirect(ser);
        Thread.sleep(1000L);
        System.out.println(ser);
    }

    @Test
    public void testFtpListConfig(){
        System.out.println(ftpListConfig);
    }

}
