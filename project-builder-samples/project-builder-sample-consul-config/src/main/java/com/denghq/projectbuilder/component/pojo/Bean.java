package com.denghq.projectbuilder.component.pojo;

import com.denghq.projectbuilder.component.config.metadata.annotation.ConfigAttribute;
import com.denghq.projectbuilder.component.config.metadata.enums.ConfigValueTypeEnum;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
public class Bean {

    @ConfigAttribute(name = "Bean属性名-myName")
    private String myName;

    @ConfigAttribute(name = "Bean属性名-list",valueType = ConfigValueTypeEnum.DATA_LIST)
    private List<String> list = Arrays.asList("1", "22", "ddd");
}
