package com.denghq.projectbuilder.component.pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cloud.context.refresh.ContextRefresher;
import org.springframework.cloud.endpoint.event.RefreshEvent;
import org.springframework.cloud.endpoint.event.RefreshEventListener;
import org.springframework.context.event.EventListener;

import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class ConfigChangeListener {

    private static Log log = LogFactory.getLog(RefreshEventListener.class);
    private ContextRefresher refresh;
    private AtomicBoolean ready = new AtomicBoolean(false);
    private Ser ser;

    public ConfigChangeListener(Ser ser, ContextRefresher refresh) {
        this.ser = ser;
        this.refresh = refresh;
    }

    @EventListener
    public void handle(ApplicationReadyEvent event) {

        System.out.println("====================================================================");
        System.out.println("====================================================================");
        System.out.println("====================================================================");
        System.out.println(ser);
        this.ready.compareAndSet(false, true);
    }

    @EventListener
    public void handle(RefreshEvent event) {


        if (this.ready.get()) { // don't handle events before app is ready
            log.debug("Event received " + event.getEventDesc());
            Set<String> keys = this.refresh.refresh();
            log.info("Refresh keys changed: " + keys);
            System.out.println("=========1=====================1=====================================");
            System.out.println("=========1=====================1======================================");
            System.out.println("=========1=====================1======================================");
            System.out.println(ser);
        }
    }


}