package com.denghq.projectbuilder.component.pojo;

import com.denghq.projectbuilder.component.config.metadata.annotation.ConfigAttribute;
import com.denghq.projectbuilder.component.config.metadata.enums.ConfigValueTypeEnum;

import lombok.Data;

@Data
public class FtpDetail {
	
	@ConfigAttribute(name="key",valueType=ConfigValueTypeEnum.STRING)
	private String key;
	@ConfigAttribute(name="host",valueType=ConfigValueTypeEnum.STRING)
	private String host;
	@ConfigAttribute(name="port",valueType=ConfigValueTypeEnum.NUMBER)
	private int port;
	@ConfigAttribute(name="userName",valueType=ConfigValueTypeEnum.STRING)
	private String userName;
	@ConfigAttribute(name="password",valueType=ConfigValueTypeEnum.STRING)
	private String password;
	
	
}
