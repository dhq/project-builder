package com.denghq.projectbuilder.component.pojo;

import com.denghq.projectbuilder.component.config.metadata.annotation.Category;
import com.denghq.projectbuilder.component.config.metadata.annotation.ConfigAttribute;
import com.denghq.projectbuilder.component.config.metadata.enums.ConfigValueTypeEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
@ConfigurationProperties(prefix = "com.denghq.tdm")
@Category(projectName = "校时管理", categoryNo = "tdm/TDMProperties0", categoryName = "校时管理",autoRegiste = false)
@Data
public class TDMProperties {

    /**
     *  时间源服务器地址配置
     */
    @ConfigAttribute(name = "时间源服务器地址",valueType = ConfigValueTypeEnum.ARRAY)
    private List<String> ntpServerUrl;

    /**
     *  时间源服务器时间相差阈值(ms),超过阈值发送阻断消息
     */
    @ConfigAttribute(name = "时间源相差阈值(ms)")
    private Long remoteTimeDiffThreshold = 1000L;

    /**
     *  本地和校时服务器时间相差阈值(ms)，超过阈值则需重置本地时间为校时服务器的时间
     */
    @ConfigAttribute(name = "本地时间相差阈值(ms)")
    private Long localTimeDiffThreshold = 1500L;

    /**
     * 校准的时间间隔 (ms)
     */
    @ConfigAttribute(name = "校准的时间间隔(ms)")
    private Long syncInterval = 2000L;

    /**
     * 连接ntp服务的超时时间设置
     */
    @ConfigAttribute(name = "连接ntp服务超时时间(ms)")
    private Integer ntpClientDefaultTimeout = 800;

    /**
     * 连接ntp服务的重试次数
     */
    @ConfigAttribute(name = "连接ntp服务的重试次数")
    private Integer ntpClientRetry = 1;

    /**
     * 当前是否已阻断
     */
    @ConfigAttribute(name = "是否全局阻断")
    private Boolean blocked;
}
