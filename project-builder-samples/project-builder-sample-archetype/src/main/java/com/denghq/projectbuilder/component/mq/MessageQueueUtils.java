package com.denghq.projectbuilder.component.mq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.denghq.projectbuilder.component.amqp.MsgDecorator;
import com.denghq.projectbuilder.component.amqp.SimpleMessageQueueUtils;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@Component
public class MessageQueueUtils{

	@Autowired
	SimpleMessageQueueUtils simpleMessageQueueUtils;
	
	@Autowired
	private RoutingKeyProperties routingKeyProperties;

	
	public void sendTestMsg(MsgDecorator<?> msg) {
		simpleMessageQueueUtils.sendMsg( routingKeyProperties.getTest(),msg);
	}

}
