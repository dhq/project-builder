package com.denghq.projectbuilder.component.remote.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

//@ApiModel("设备表查询结果")
@Data
public class CommonFrontdeviceModel {

	/**
	 * 设备Id 主键
	 */
	@ApiModelProperty("设备Id 主键")
	private String deviceid;
	
	/**
	 * 设备名称
	 */
	@ApiModelProperty("设备名称")
	private String devicename;
	
	/**
	 * 设备编号，用于和前端设备进行匹配,系统自定义编号
	 */
	@ApiModelProperty("设备编号，用于和前端设备进行匹配,系统自定义编号")
	private String deviceno;
	
	/**
	 * 核心版对接编号
	 */
	@ApiModelProperty("核心版对接编号")
	private String coreno;
	
	/**
	 * 所属管辖单位Id，如果关联了路口则与路口管辖单位Id一致
	 */
	@ApiModelProperty("所属管辖单位-设备")
	private String departmentid;
	
	/**
	 * 设备的来源,系统添加，外部设备, 字典类型
	 */
	@ApiModelProperty("设备的来源,系统添加，外部设备, 字典类型")
	private String sourcekind;
	
	/**
	 * 设备型号
	 */
	@ApiModelProperty("设备型号")
	private String devicemodel;
	
	/**
	 * 功能类型
	 */
	@ApiModelProperty("功能类型")
	private String functiontype;
	
	/**
	 * 功能类型
	 */
	@ApiModelProperty("设备类型")
	private String devicetype;
	
	/**
	 * 关联点位名称
	 */
	@ApiModelProperty("关联点位（所在路口）")
	private String spotting;

	/**
	 * 关联点位ID
	 */
	@ApiModelProperty("关联点位id")
	private String spottingid;

	/**
	 * 方向Id
	 */
	@ApiModelProperty("关联方向id")
	private String directionid;

	/**
	 * 方向Id
	 */
	@ApiModelProperty("关联车道编号")
	private String landnolist;

	/**
	 * 方向Id
	 */
	@ApiModelProperty("关联方向名称")
	private String direction;
	
	/**
	 * 经度坐标值
	 */
	@ApiModelProperty("经度坐标值")
	private BigDecimal longitude;
	
	/**
	 * 纬度坐标值
	 */
	@ApiModelProperty("纬度坐标值")
	private BigDecimal latitude;
	
	/**
	 * 相机视频连接信息
	 */
	@ApiModelProperty("相机视频连接信息")
	private String videoinfo;
	
	/**
	 * 位置信息，卡口、电子警察、信号机、诱导屏、区间测速设备，关联信息各不相同，需要自定义
	 */
	@ApiModelProperty("位置信息，卡口、电子警察、信号机、诱导屏、区间测速设备，关联信息各不相同，需要自定义")
	private String spottinginfo;
	
	/**
	 * 扩展字段 用于存储每种设备拓展信息
	 */
	@ApiModelProperty("扩展字段 用于存储每种设备拓展信息")
	private String ext;
	
	/**
	 * 扩展字段1
	 */
	@ApiModelProperty("扩展字段1")
	private String ext1;
	
	/**
	 * 扩展字段2
	 */
	@ApiModelProperty("扩展字段2")
	private String ext2;
	
	/**
	 * 扩展字段3
	 */
	@ApiModelProperty("扩展字段3")
	private String ext3;
	
	/**
	 * 备注
	 */
	@ApiModelProperty("备注")
	private String remark;
	
	/**
	 * IP地址
	 */
	@ApiModelProperty("IP地址")
	private String ip;
	
	/**
	 * 待审核、已审核,正常、故障、调试、拆改、启停用
	 */
	@ApiModelProperty("待审核、已审核,正常、故障、调试、拆改、启停用")
	private String ywstatus;
	
	/**
	 * 制造商/品牌厂商代码，字典
	 */
	@ApiModelProperty("制造商/品牌厂商")
	private String manufacturer;
	
	/**
	 * 所在项目Id
	 */
	@ApiModelProperty("所在项目")
	private String project;
	
	/**
	 * 所在项目名称
	 */
	@ApiModelProperty("所在项目名称")
	private String projectname;
	
	/**
	 * 建设单位
	 */
	@ApiModelProperty("建设单位")
	private String projectDepartment;
	
	/**
	 * 承建单位
	 */
	@ApiModelProperty("承建单位")
	private String company;
	
	/**
	 * 监理单位
	 */
	@ApiModelProperty("监理单位")
	private String supervisioncompany;
	
	/**
	 * 运维单位
	 */
	@ApiModelProperty("运维单位")
	private String maintenancecompany;
	
	/**
	 * tags，多个用逗号分隔
	 */
	/*@ApiModelProperty("标签")
	private List<CommonTagsEntity> tags;*//*@ApiModelProperty("标签")
	private List<CommonTagsEntity> tags;*/
	
	/**
	 * 审核状态(0:未审核, 1:审核通过, 2:审核未通过), 默认为未审核状态
	 */
	@ApiModelProperty("审核状态(0:未审核, 1:审核通过, 2:审核未通过), 默认为未审核状态")
	private BigDecimal approvestatus;
	
	/**
	 * 审核状态
	 */
	@ApiModelProperty("审核状态名称")
	private String approveStatusName;
	
	/**
	 * 审核人 用户code
	 */
	@ApiModelProperty("审核人 用户code")
	private String approvetor;
	
	/**
	 * 审核时间
	 */
	@ApiModelProperty("审核时间")
	private Date approvetime;
	
	/**
	 * 审核说明
	 */
	@ApiModelProperty("审核说明")
	private String approveinfo;
	
	/**
	 * 抓拍方向 0：抓拍车尾 1：抓拍车头
	 */
	@ApiModelProperty("抓拍方向 0：抓拍车尾 1：抓拍车头")
	private BigDecimal headstock;
	
	/**
	 * 拼音
	 */
	@ApiModelProperty("拼音")
	private String bopomofo;
	
	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	private Date createdtime;
	
	/**
	 * 创建人
	 */
	@ApiModelProperty("创建人")
	private String creator;
	
	/**
	 * 修改人
	 */
	@ApiModelProperty("修改人")
	private String modifier;
	
	/**
	 * 修改时间
	 */
	@ApiModelProperty("修改时间")
	private Date modifiedtime;

	/**
	 * 服务扩展字段值
	 */
	@ApiModelProperty("服务扩展字段值")
	private String servicefieldvalue;

	/**
	 * 设备类型名称
	 */
	@ApiModelProperty("设备类型名称")
	private String devicetypename;

	/**
	 * 功能类型名称
	 */
	@ApiModelProperty("功能类型名称")
	private String functiontypename;

	/**
	 * 运维状态名称
	 */
	@ApiModelProperty("运维状态名称")
	private String  ywstatusname;

	/**
	 * 制造商/品牌厂商名称
	 */
	@ApiModelProperty("制造商/品牌厂商名称")
	private String manufacturername;

	/**
	 * 建设单位名称
	 */
	@ApiModelProperty("建设单位名称")
	private String projectDepartmentname;

	/**
	 * 所属管辖单位名称
	 */
	@ApiModelProperty("所属管辖单位名称")
	private String departmentname;

	/**
	 * 承建单位名称
	 */
	@ApiModelProperty("承建单位名称")
	private String companyname;

	/**
	 * 监理单位名称
	 */
	@ApiModelProperty("监理单位名称")
	private String supervisioncompanyname;

	/**
	 * 运维单位名称
	 */
	@ApiModelProperty("运维单位名称")
	private String maintenancecompanyname;
}
