package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import com.denghq.projectbuilder.component.remote.client.CommonFrontDeviceClient;
import com.denghq.projectbuilder.component.remote.model.CommonFrontdeviceModel;
import com.denghq.projectbuilder.component.remote.service.CommonFrontDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("commonFrontDeviceService")
public class CommonFrontDeviceServiceImpl implements CommonFrontDeviceService {

    @Autowired
    private CommonFrontDeviceClient commonFrontDeviceClient;

    @Override
    public List<CommonFrontdeviceModel> search(Map<String, Object> params) {
        return CloudUtils.getResult(commonFrontDeviceClient.searchSimple(params));
    }

}
