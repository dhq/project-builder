package com.denghq.projectbuilder.component.pojo.api;

import com.baomidou.mybatisplus.plugins.Page;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import springfox.documentation.annotations.ApiIgnore;

@Data
public class PageReq {
	
	@ApiModelProperty(value = "是否分页，默认为否",position=0,example="false")
	private Boolean isPagination = false;
	
	@ApiModelProperty(value = "第几页，isPagination为true时有效",position=0,example="1")
	private Integer pageNo;
	@ApiModelProperty(value = "每页大小，isPagination为true时有效",position=0,example="10")
	private Integer pageSize;

	@ApiModelProperty(value = "是否返回总条数，默认为是",position=0,example="true")
	private Boolean searchCount = true;

	@ApiModelProperty(value = "排序字段",position=0,example="createTime")
	private String sortField;

	@ApiModelProperty(value = "排序方向",position=0,example="desc")
	private String sortOrder;

	@ApiIgnore
	@JsonIgnore
	public <T> Page <T> getPage(){
		if(pageNo==null||pageNo<1) {
			pageNo = 1;
		}
		if(pageSize==null) {
			pageSize = 10;
		}
		Page<T> page = new Page<>(pageNo, pageSize);
		if(searchCount!=null && !searchCount){
			page.setSearchCount(false);
		}
		return page;
	}

}
