package com.denghq.projectbuilder.component.remote.model;

import java.math.BigDecimal;
import java.util.Date;


import lombok.Data;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 道路点位信息
 * 
 * @author denghq
 * @email 
 * @date 2018/10/29
 */
@ApiModel("道路点位信息")
@Data
public class CommonSpottingModel{

	/**
	 * 点位ID
	 */
	
	@ApiModelProperty("点位ID")
	private String spottingid;
	/**
	 * 点位编号(可以为厂家分配的点位编号)
	 */
	@ApiModelProperty("点位编号(可以为厂家分配的点位编号)")
	private String spottingno;
	/**
	 * 点位名称
	 */
	@ApiModelProperty("点位名称")
	private String spottingname;
	/**
	 * 上传六合一标准代码
	 */
	@ApiModelProperty("上传六合一标准代码")
	private String uniquecode;
	/**
	 * 经度坐标值
	 */
	@ApiModelProperty("经度坐标值")
	private BigDecimal longitude;
	/**
	 * 纬度坐标值
	 */
	@ApiModelProperty("纬度坐标值")
	private BigDecimal latitude;
	/**
	 * 所在管理部门
	 */
	@ApiModelProperty("所在管理部门")
	private String departmentid;
	/**
	 * 来源类型 local:基础数据路口 其它值表示第三方调用接口创建，自己填写
	 */
	@ApiModelProperty("来源类型 local:基础数据路口 其它值表示第三方调用接口创建，自己填写")
	private String sourcekind;
	/**
	 * 创建用户
	 */
	@ApiModelProperty("创建用户")
	private String creator;
	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	private Date createdtime;
	/**
	 * 修改人
	 */
	@ApiModelProperty("修改人")
	private String modifier;
	/**
	 * 修改时间
	 */
	@ApiModelProperty("修改时间")
	private Date modifiedtime;
	/**
	 * 备注
	 */
	@ApiModelProperty("备注")
	private String remark;
	/**
	 * 所在地区编号（行政区划代码）
	 */
	@ApiModelProperty("所在地区编号（行政区划代码）")
	private String areacode;
	/**
	 * 拼音简称
	 */
	@ApiModelProperty("拼音简称")
	private String bopomofo;
	/**
	 * 点位类型(字典信息字典 ，Kind 为 1003 ， 十字路口/丁字路口/圆形转盘/其它)
	 */
	@ApiModelProperty("点位类型(字典信息字典 ，Kind 为 1003 ， 十字路口/丁字路口/圆形转盘/其它)")
	private String spottingtype;
	/**
	 * 是否停用(0 未停用， 1 停用)，默认为0
	 */
	@ApiModelProperty("是否停用(0 未停用， 1 停用)，默认为0")
	private BigDecimal disabled;
	/**
	 * 违法处理部门，为空取DEPARTMENTID
	 */
	@ApiModelProperty("违法处理部门，为空取DEPARTMENTID")
	private String punishdepartment;
	/**
	 * 违法数据行政区划，为空取AREACODE
	 */
	@ApiModelProperty("违法数据行政区划，为空取AREACODE")
	private String divisioncode;
	/**
	 * 审核状态(0:未审核, 1:审核通过, 2:审核未通过), 默认为未审核状态
	 */
	@ApiModelProperty("审核状态(0:未审核, 1:审核通过, 2:审核未通过), 默认为未审核状态")
	private BigDecimal approvestatus;
	/**
	 * 审核用户代码
	 */
	@ApiModelProperty("审核用户代码")
	private String approveuserid;
	/**
	 * 审核时间
	 */
	@ApiModelProperty("审核时间")
	private Date approvetime;
	/**
	 * 审核说明
	 */
	@ApiModelProperty("审核说明")
	private String approveinfo;
	/**
	 * 最大限重(KG)
	 */
	@ApiModelProperty("最大限重(KG)")
	private BigDecimal maxweight;
	/**
	 * 最大限高(m)
	 */
	@ApiModelProperty("最大限高(m)")
	private BigDecimal maxheight;
	/**
	 * 路口性质，字典 暂未使用
	 */
	@ApiModelProperty("路口性质，字典 暂未使用")
	private String spottingkind;

}
