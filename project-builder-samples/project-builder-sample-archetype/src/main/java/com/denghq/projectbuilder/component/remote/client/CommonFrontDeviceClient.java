package com.denghq.projectbuilder.component.remote.client;

import com.denghq.projectbuilder.component.remote.constants.Constant;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.denghq.projectbuilder.component.remote.model.CommonFrontdeviceModel;
import com.special.FeignTokenSupportConfig;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;


@FeignClient(name=Constant.SERVICE_FMW_CODE,configuration = FeignTokenSupportConfig.class) //请求头需要携带默认token才配置configuration = FeignTokenSupportConfig.class，一般情况下需删除configuration = FeignTokenSupportConfig.class配置。
public interface CommonFrontDeviceClient {

    @PostMapping("/fmw/commonFrontdevice/searchSimple")
    ResultInfoModel<List<CommonFrontdeviceModel>> searchSimple(@RequestBody Map<String, Object> params);
      
}
