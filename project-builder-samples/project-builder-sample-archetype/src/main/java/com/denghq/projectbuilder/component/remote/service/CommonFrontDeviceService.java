package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.CommonFrontdeviceModel;

import java.util.List;
import java.util.Map;

public interface CommonFrontDeviceService {

    List<CommonFrontdeviceModel> search(Map<String, Object> params);

}
