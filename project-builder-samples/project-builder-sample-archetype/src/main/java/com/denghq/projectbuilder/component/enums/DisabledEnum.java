package com.denghq.projectbuilder.component.enums;

import java.math.BigDecimal;

public enum DisabledEnum {

	NO("启用"),YES("停用");

	private String name;

	DisabledEnum(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}

	public static String getName(BigDecimal status){

		if(status!=null) {
			if(status.intValue() == 0) {
				return DisabledEnum.NO.getName();
			}else if(status.intValue() == 1) {
				return DisabledEnum.YES.getName();
			}
		}
		//0:未审核, 1:审核通过, 2:审核未通过
		return DisabledEnum.NO.getName();

	}
}
