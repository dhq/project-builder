package com.special;

import com.denghq.projectbuilder.common.Constant;
import com.denghq.projectbuilder.component.remote.utils.UserContextHolder;
import feign.Logger;
import feign.RequestInterceptor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignTokenSupportConfig {

    private static final String AUTHORIZATION = "Basic 0FA7WB2KP3R317519OBPM05";

    @Bean
    public RequestInterceptor headerInterceptor() {
        String token = UserContextHolder.getAuthorization();
        if (StringUtils.isBlank(token)) {
            return requestTemplate ->
                    requestTemplate.header(Constant.SERVICE_TOKEN_NAME, AUTHORIZATION);
        } else {
            return requestTemplate ->
                    requestTemplate.header(Constant.SERVICE_TOKEN_NAME, token);
        }

    }

    //@Bean
    public Logger.Level level() {
        return Logger.Level.FULL;
    }

}