package com.denghq.projectbuilder.component.web.config;

import com.denghq.projectbuilder.component.web.Filter.NoCacheResourceFilter;
import com.denghq.projectbuilder.component.web.xss.XssFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.DispatcherType;

/**
 * Filter配置
 *
 * @author denghq
 */
@Configuration
public class FilterConfig {

    @Bean
    //在application.properties配置"com.denghq.projectbuilder.xssfilter"，对应的值为true
    @ConditionalOnProperty(prefix = "com.denghq.projectbuilder", name = "xssfilter", havingValue = "true", matchIfMissing = true)
    public FilterRegistrationBean<XssFilter> xssFilterRegistration() {
        FilterRegistrationBean<XssFilter> registration = new FilterRegistrationBean<XssFilter>();
        registration.setDispatcherTypes(DispatcherType.REQUEST);
        registration.setFilter(new XssFilter());
        registration.addUrlPatterns("/*");
        registration.setName("xssFilter");
        registration.setOrder(Integer.MAX_VALUE);
        return registration;
    }

    @Bean
    public FilterRegistrationBean<NoCacheResourceFilter> noCacheResourceFilterRegistration() {
        FilterRegistrationBean<NoCacheResourceFilter> registration = new FilterRegistrationBean<>();
        registration.setDispatcherTypes(DispatcherType.REQUEST);
        registration.setFilter(new NoCacheResourceFilter());
        registration.addUrlPatterns("/*");
        registration.setName("NoCacheResourceFilter");
        registration.setOrder(Integer.MAX_VALUE);
        return registration;
    }

}

