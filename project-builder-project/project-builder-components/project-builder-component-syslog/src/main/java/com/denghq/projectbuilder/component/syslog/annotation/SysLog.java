package com.denghq.projectbuilder.component.syslog.annotation;

import java.lang.annotation.*;

/**
 * 系统日志注解
 *
 * @author denghq
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

    //功能代码
    String functionCode();

    // 动作点代码，多个逗号分隔
    String actionCode();

    // 是否编辑接口，编辑接口可以设置两个code 01,02取第一个做为新增，第二个做为编辑
    boolean isEdit() default false;

    //是否保存操作日志
    boolean isSaveLog() default true;

    //是否校验权限
    boolean isCheckPermission() default true;

    // id字段名称，区分大小写，需与实体字段一致
    String idPropertyName() default "id";

    // 通过数据id获取详情接口地址,如果地址中不带?则认为接口参数为id，否则需指明参数名
    // /smw/Account/GetAccountDetail?UserGUID=  则请求时地址为 /smw/Account/GetAccountDetail?UserGUID=数据id
    // /smw/Account/GetAccountDetail  则请求时地址为 /smw/Account/GetAccountDetail?id=数据id
    String detailDataUrl() default "";

    String remark() default "";
}
