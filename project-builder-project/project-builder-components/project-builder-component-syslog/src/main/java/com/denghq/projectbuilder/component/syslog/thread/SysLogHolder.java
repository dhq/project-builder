package com.denghq.projectbuilder.component.syslog.thread;

import com.denghq.projectbuilder.component.remote.model.SysLogModel;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Slf4j
public class SysLogHolder {


    private final BlockingQueue<SysLogModel> blockingQueue;

    private SysLogHolder() {
        blockingQueue = new LinkedBlockingQueue<>(1000);
    }

    public SysLogModel getOne() {
        return blockingQueue.poll();//非阻塞模式，不然会卡死，之前有的数据一直得不到保存。。
    }

    public void putOne(SysLogModel sysLogEntity) {
        try {
            blockingQueue.put(sysLogEntity);
        } catch (InterruptedException e) {
            log.info("线程被打断，msg:{}", e.getMessage());
        }
    }

    private static class SingleTonHolder {
        private static SysLogHolder INSTANCE = new SysLogHolder();
    }

    public static SysLogHolder getInstance() {
        return SingleTonHolder.INSTANCE;
    }

}
