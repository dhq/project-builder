package com.denghq.projectbuilder.component.syslog.autoconfigure;

import com.denghq.projectbuilder.component.remote.service.OprLogService;
import com.denghq.projectbuilder.component.syslog.thread.SysLogInsertExec;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SysLogAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(SysLogAspect.class)
    public SysLogAspect sysLogAspect(OprLogService oprLogService) {
        SysLogInsertExec sysLogInsertExec = new SysLogInsertExec(oprLogService);
        Thread thread = new Thread(sysLogInsertExec);
        thread.start();
        return new SysLogAspect();
    }


}
