package com.denghq.projectbuilder.component.syslog.enums;

/**
 * 响应结果枚举
 */
public enum ResponseEnum {

    success("成功"), error("失败");

    private String name;

    ResponseEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
