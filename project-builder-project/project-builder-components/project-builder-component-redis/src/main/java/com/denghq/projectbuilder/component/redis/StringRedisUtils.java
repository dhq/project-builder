package com.denghq.projectbuilder.component.redis;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.StringRedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

//@Component
public class StringRedisUtils {

    private final static int BATCH_SIZE = 300;

    /**
     * 注入redisTemplate bean
     */
    @Autowired
    @Qualifier("stringRedisTemplate")
    private StringRedisTemplate redisTemplate;

    public Set<String> keys(String keyReg) {
        return redisTemplate.keys(keyReg);
    }

    /**
     * 指定缓存失效时间
     *
     * @param key  键
     * @param time 时间(秒)
     * @return
     */
    public boolean expire(String key, long time) {
        try {
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 根据key获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */
    public long getExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true 存在 false不存在
     */
    public boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除缓存
     *
     * @param key 可以传一个值 或多个
     */
    @SuppressWarnings("unchecked")
    public void del(String... key) {
        if (key != null && key.length > 0) {
            if (key.length == 1) {
                redisTemplate.delete(key[0]);
            } else {
                List<List<String>> batchList = splitList(CollectionUtils.arrayToList(key), 300);
                batchList.forEach(list -> redisTemplate.delete(list));
            }
        }
    }
// ============================String(字符串)=============================

    /**
     * 普通缓存获取
     *
     * @param key 键
     * @return 值
     */
    public Object get(String key) {
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }

    /**
     * 普通缓存放入
     *
     * @param key   键
     * @param value 值
     * @return true成功 false失败
     */
    public boolean set(String key, String value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public boolean set(String key, String value, long time) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 递增
     *
     * @param key   键
     * @param delta 要增加几(大于0)
     * @return
     */
    public long incr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递增因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, delta);
    }

    /**
     * 递减
     *
     * @param key   键
     * @param delta 要减少几(小于0)
     * @return
     */
    public long decr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, -delta);
    }
// ================================Hash(哈希)=================================

    /**
     * HashGet
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return 值
     */
    public Object hget(String key, String item) {
        return redisTemplate.opsForHash().get(key, item);
    }

    /**
     * 获取hashKey对应的所有键值
     *
     * @param key 键
     * @return 对应的多个键值
     */
    public Map<Object, Object> hmget(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * HashSet
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */
    public boolean hmset(String key, Map<String, Object> map) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * HashSet 并设置时间
     *
     * @param key  键
     * @param map  对应多个键值
     * @param time 时间(秒)
     * @return true成功 false失败
     */
    public boolean hmset(String key, Map<String, Object> map, long time) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @return true 成功 false失败
     */
    public boolean hset(String key, String item, Object value) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒) 注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false失败
     */
    public boolean hset(String key, String item, Object value, long time) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除hash表中的值
     *
     * @param key  键 不能为null
     * @param item 项 可以使多个 不能为null
     */
    public void hdel(String key, Object... item) {
        redisTemplate.opsForHash().delete(key, item);
    }

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return true 存在 false不存在
     */
    public boolean hHasKey(String key, String item) {
        return redisTemplate.opsForHash().hasKey(key, item);
    }

    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     *
     * @param key  键
     * @param item 项
     * @param by   要增加几(大于0)
     * @return
     */
    public double hincr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, by);
    }

    /**
     * hash递减
     *
     * @param key  键
     * @param item 项
     * @param by   要减少记(小于0)
     * @return
     */
    public double hdecr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, -by);
    }
// ============================Set(集合)=============================

    /**
     * 根据key获取Set中的所有值
     *
     * @param key 键
     * @return
     */
    public Set<String> sGet(String key) {
        try {
            return redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */
    public boolean sHasKey(String key, Object value) {
        try {
            return redisTemplate.opsForSet().isMember(key, value);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将数据放入set缓存
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSet(String key, String... values) {
        try {
            return redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 将set数据放入缓存
     *
     * @param key    键
     * @param time   时间(秒)
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSetAndTime(String key, long time, String... values) {
        try {
            Long count = redisTemplate.opsForSet().add(key, values);
            if (time > 0)
                expire(key, time);
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 获取set缓存的长度
     *
     * @param key 键
     * @return
     */
    public long sGetSetSize(String key) {
        try {
            return redisTemplate.opsForSet().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 移除值为value的
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public long setRemove(String key, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().remove(key, values);
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
// ===============================List(列表)=================================

    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束 0 到 -1代表所有值
     * @return
     */
    public List<String> lGet(String key, long start, long end) {
        try {
            return redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取list缓存的长度
     *
     * @param key 键
     * @return
     */
    public long lGetListSize(String key) {
        try {
            return redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 通过索引 获取list中的值
     *
     * @param key   键
     * @param index 索引 index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */
    public Object lGetIndex(String key, long index) {
        try {
            return redisTemplate.opsForList().index(key, index);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, String value) {
        try {
            redisTemplate.opsForList().leftPush(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, String value, long time) {
        try {
            redisTemplate.opsForList().leftPush(key, value);
            if (time > 0)
                expire(key, time);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, List<String> value) {
        try {
            redisTemplate.opsForList().leftPushAll(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, List<String> value, long time) {
        try {
            redisTemplate.opsForList().leftPushAll(key, value);
            if (time > 0)
                expire(key, time);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 根据索引修改list中的某条数据
     *
     * @param key   键
     * @param index 索引
     * @param value 值
     * @return
     */
    public boolean lUpdateIndex(String key, long index, String value) {
        try {
            redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 移除N个值为value
     *
     * @param key   键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */
    public long lRemove(String key, long count, Object value) {
        try {
            Long remove = redisTemplate.opsForList().remove(key, count, value);
            return remove;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * @param keyItemMap key:redis key,value:redis value
     * @param timeMap    key:redis key,value:过期时间
     */
    public void batchWriteRedisByList(Map<String, List<String>> keyItemMap, Map<String, Long> timeMap) {
        keyItemMap.forEach((k, v) -> {
            batchWriteRedisByList(k, v, timeMap == null ? null : timeMap.get(k));
        });
    }

    /**
     * @param key        key:redis key
     * @param valueList  redis valueList
     * @param expireTime
     */
    public void batchWriteRedisByList(String key, List<String> valueList, Long expireTime) {

        List<List<String>> batchList = splitList(valueList, BATCH_SIZE);
        for (List<String> values : batchList) {
            redisTemplate.executePipelined(new SessionCallback<Object>() {
                @Override
                public <K, V> Object execute(RedisOperations<K, V> operations) throws DataAccessException {

                    if (!CollectionUtils.isEmpty(values)) {

                        operations.opsForList().leftPushAll((K) key, (List) values);

                    }
                    return null;
                }
            });
        }

        // 有效期
        if (expireTime != null && expireTime > 0) {
            this.expire(key, expireTime);
        }
    }

    public void batchWriteRedisByHash(String key, Map<String, String> keyItemMap, Long expireTime) {

        Set<String> keySet = keyItemMap.keySet();
        List<List<String>> batchList = splitSet(keySet, BATCH_SIZE);

        for (List<String> batch : batchList) {
            redisTemplate.executePipelined(new SessionCallback<Object>() {

                @Override
                public <K, V> Object execute(RedisOperations<K, V> operations) throws DataAccessException {

                    for (String mapKey : batch) {
                        operations.opsForHash().put((K) key, mapKey, keyItemMap.get(mapKey));
                    }

                    return null;
                }
            });
        }

        // 有效期
        if (expireTime != null && expireTime > 0) {
            this.expire(key, expireTime);
        }


    }

    public void batchWriteRedisByHash(Map<String, Map<String, String>> keyItemMap, Map<String, Long> timeMap) {
        Set<String> keySet = keyItemMap.keySet();
        for (String key : keySet) {
            batchWriteRedisByHash(key, keyItemMap.get(key), timeMap == null ? null : timeMap.get(key));
        }
    }


    public void batchWriteRedisByString(Map<String, String> keyItemMap, Map<String, Long> timeMap) {

        Set<String> keySet = keyItemMap.keySet();
        List<List<String>> batchList = splitSet(keySet, BATCH_SIZE);

        for (List<String> batch : batchList) {
            redisTemplate.executePipelined(new SessionCallback<Object>() {

                @Override
                public <K, V> Object execute(RedisOperations<K, V> operations) throws DataAccessException {
                    for (String key : batch) {
                        operations.opsForValue().set((K) key, (V) keyItemMap.get(key));
                        // 有效期
                        if (timeMap != null) {

                            Long time = timeMap.get(key);
                            if (time != null && time > 0) {

                                operations.expire((K) key, time, TimeUnit.SECONDS);
                            }
                        }
                    }
                    return null;
                }
            });
        }

    }

    public Map<String, List<String>> batchReadRedisForList(List<String> keys) {
        Map<String, List<String>> res = Maps.newHashMap();
        List<List<String>> batchList = splitList(keys, BATCH_SIZE);
        for (List<String> batch : batchList) {
            // 批量获取key的list值
            List<Object> keyValueList = redisTemplate.executePipelined((RedisCallback<Object>) (RedisConnection connection) -> {

                StringRedisConnection stringConnection = (StringRedisConnection) connection;
                for (String key : batch) {

                    stringConnection.lRange(key, 0, -1);
                }

                return null;
            });
            for (int i = 0; i < batch.size(); i++) {
                res.put(batch.get(i), (keyValueList.get(i) == null ? Lists.newArrayList() : (List<String>) keyValueList.get(i)));
            }
        }

        return res;
    }


    public Map<String, String> batchReadRedisForString(List<String> keys) {

        Map<String, String> res = Maps.newHashMap();
        List<List<String>> batchList = splitList(keys, BATCH_SIZE);

        for (List<String> batch : batchList) {
            // 批量获取key的list值
            List<Object> keyValueList = redisTemplate.executePipelined((RedisCallback<Object>) (RedisConnection connection) -> {

                StringRedisConnection stringConnection = (StringRedisConnection) connection;
                batch.forEach(key -> stringConnection.get(key));
                return null;
            });

            for (int i = 0; i < batch.size(); i++) {
                res.put(batch.get(i), (keyValueList.get(i) == null ? null : (String) keyValueList.get(i)));
            }

        }

        return res;

    }


    public Map<String, String> batchReadRedisForMap(String key, List<String> mapKeys) {

        Map<String, String> res = Maps.newHashMap();
        List<List<String>> batchList = splitList(mapKeys, BATCH_SIZE);
        for (List<String> batch : batchList) {

            // 批量获取key的list值
            List<Object> keyValueList = redisTemplate.executePipelined((RedisCallback<Object>) (RedisConnection connection) -> {

                StringRedisConnection stringConnection = (StringRedisConnection) connection;
                batch.forEach(k -> stringConnection.hGet(key, k));
                return null;
            });

            for (int i = 0; i < batch.size(); i++) {
                res.put(batch.get(i), (keyValueList.get(i) == null ? null : (String) keyValueList.get(i)));
            }
        }

        return res;

    }

    public Map<String, String> batchReadRedisForMap(Map<String, List<String>> mapKeys) {

        Map<String, String> res = Maps.newHashMap();
        mapKeys.forEach((k, v) -> res.putAll(batchReadRedisForMap(k, v)));
        return res;
    }

    public void batchDelRedisForMap(String key, List<String> mapKeys) {

        List<List<String>> batchList = splitList(mapKeys, BATCH_SIZE);
        for (List<String> batch : batchList) {

            // 批量获取key的list值
            redisTemplate.executePipelined((RedisCallback<Object>) (RedisConnection connection) -> {

                StringRedisConnection stringConnection = (StringRedisConnection) connection;

                batch.forEach(k -> stringConnection.hDel(key, k));
                return null;
            });

        }

    }

    public void batchDelRedisForMap(Map<String, List<String>> mapKeys) {
        mapKeys.forEach((k, v) -> batchDelRedisForMap(k, v));
    }

    public static <T> List<List<T>> splitSet(Set<T> dataList, int limit) {
        return splitList(new ArrayList<>(dataList), limit);
    }

    public static <T> List<List<T>> splitList(List<T> dataList, int limit) {
        List<List<T>> resList = new ArrayList<List<T>>();
        if (CollectionUtils.isEmpty(dataList)) {
            return Lists.newArrayList();
        }
        if (dataList.size() <= limit) {
            resList.add(dataList);
            return resList;
        }
        BigDecimal dataSize = new BigDecimal(dataList.size());
        int count = dataSize.divide(new BigDecimal(limit), RoundingMode.CEILING).toBigInteger().intValue();
        for (int i = 0; i < count; i++) {
            if (i == count - 1) {
                resList.add(dataList.subList(i * limit, dataSize.intValue()));
            } else {
                resList.add(dataList.subList(i * limit, (i + 1) * limit));
            }
        }
        return resList;
    }


    /**
     * @param key        key:redis key
     * @param valueList  redis valueList
     * @param expireTime
     */
    public void batchWriteRedisBySet(String key, Set<String> valueList, Long expireTime) {

        List<List<String>> batchList = StringRedisUtils.splitSet(valueList, 300);
        for (List<String> values : batchList) {
            redisTemplate.executePipelined(new SessionCallback<Object>() {
                @Override
                public Object execute(RedisOperations operations) throws DataAccessException {

                    if (!CollectionUtils.isEmpty(values)) {
                        operations.opsForSet().add(key,values.toArray());
                        //operations.opsForSet().add((K) key, (V) values.toArray());
                        //operations.opsForSet().add((K) key, (V) values);
                    }
                    return null;
                }
            });
        }

        // 有效期
        if (expireTime != null && expireTime > 0) {
            expire(key, expireTime);
        }
    }


    /**
     * @param keyItemMap key:redis key,value:redis value
     * @param timeMap    key:redis key,value:过期时间
     */
    public void batchWriteRedisBySet(Map<String, Set<String>> keyItemMap, Map<String, Long> timeMap) {
        keyItemMap.forEach((k, v) -> {
            batchWriteRedisBySet(k, v, timeMap == null ? null : timeMap.get(k));
        });

    }


    public Map<String, Set<String>> batchReadRedisForSet(List<String> keys) {
        Map<String, Set<String>> res = Maps.newHashMap();
        List<List<String>> batchList = splitList(keys, BATCH_SIZE);
        for (List<String> batch : batchList) {
            // 批量获取key的list值
            List<Object> keyValueList = redisTemplate.executePipelined((RedisCallback<Object>) (RedisConnection connection) -> {

                StringRedisConnection stringConnection = (StringRedisConnection) connection;
                for (String key : batch) {
                    stringConnection.sMembers(key);
                }

                return null;
            });
            for (int i = 0; i < batch.size(); i++) {
                res.put(batch.get(i), (keyValueList.get(i) == null ? Sets.newHashSet() : (Set<String>) keyValueList.get(i)));
            }
        }

        return res;
    }

}
