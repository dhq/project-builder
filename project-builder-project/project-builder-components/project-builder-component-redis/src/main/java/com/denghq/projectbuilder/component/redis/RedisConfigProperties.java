package com.denghq.projectbuilder.component.redis;

import com.denghq.projectbuilder.component.config.metadata.annotation.Category;
import com.denghq.projectbuilder.component.config.metadata.annotation.ConfigAttribute;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("com.denghq.redis")
@Category(projectName = "基础配置", categoryNo = "RedisConfig", categoryName = "Redis缓存",autoRegiste = false)
@Data
public class RedisConfigProperties {

    @ConfigAttribute(name = "基础数据Redis")
    private String baseDataRedis;

}
