package com.denghq.projectbuilder.component.config.metadata.enums;

/**
 * 非日期类型值来源  *默认1
 */
public enum DataSourceEnum {

    INPUT(1, "直接录入"),
    SELECT(2, "从限定列表下拉选择"),
    API(3, "从接口获取结果中下拉选择"),
    TEXT_EDITER(4,"大文本编辑器")
    ;

    private final int value;
    private final String description;


    DataSourceEnum(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
