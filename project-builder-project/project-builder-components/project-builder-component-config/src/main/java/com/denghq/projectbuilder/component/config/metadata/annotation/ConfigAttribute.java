package com.denghq.projectbuilder.component.config.metadata.annotation;

import com.denghq.projectbuilder.component.config.metadata.enums.ConfigValueTypeEnum;
import com.denghq.projectbuilder.component.config.metadata.enums.DataSourceEnum;
import com.denghq.projectbuilder.component.config.metadata.enums.DateFormatEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * 配置项信息
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ConfigAttribute {
    /**
     * 配置项key 同配置下唯一
     */
    String key() default "";

    /**
     * 配置项名称
     */
    String name();

    /**
     * 备注
     */
    String remark() default "";

    /**
     * 配置值类型 1:数值类型 2:字符串 3:布尔型 4:日期类型 5:日期区间 6:数组类型
     */
    ConfigValueTypeEnum valueType() default ConfigValueTypeEnum.NULL;

    /**
     * 日期格式化 1：年月日时分秒 2：年月日 3：年月 4：年 5：时分秒 6：时分
     */
    DateFormatEnum dateFormat() default DateFormatEnum.NULL;

    /**
     * 非日期类型值来源 1：直接录入 2：从限定列表下拉选择  3.从接口获取结果中下拉选择
     * 默认1
     */
    DataSourceEnum dataSource() default DataSourceEnum.INPUT;

    /**
     * 自定义数据源 dataSource为2时有效
     */
    OptionDataSource[] customDataList() default {};

    /**
     * 数据源里面的Key字段  默认为 value 指定DataSourceApi时生效，
     */
    String dataSourceValue() default "value";

    /**
     * 数据源里面的Text字段 默认为 text 指定DataSourceApi时生效，多个逗号分隔会按照顺序拼接 如配置了A,B两个字段显示为A--B
     */
    String dataSourceText() default "text";

    /**
     * 下拉选择后显示的字段，比如下拉里面是abc-11111,但是选中后只想显示abc,此时可以用该字段
     */
    String dataSourceValueFormat() default "text";

    /**
     * 获取下拉数据源的API接口地址 只支持get请求 dataSource为3时生效
     */
    String dataSourceApi() default "";

    /**
     * 配置项显示隐藏 依赖于下拉选择配置项的Key
     */
    String parentKey() default "";

    /**
     * 配置项显示隐藏 依赖于下拉选择配置项的哪个值Value
     */
    String parentValue() default "";

    /**
     * 当ValueType为url时，对应的自定义配置页面地址，用于复杂个性化配置项
     */
    String customPage() default "";

    /**
     * 对于valueType为DataList的类型生效，设置列表列是否为主键列，保存时会校验值唯一性
     */
    boolean primaryKey() default false;
}
