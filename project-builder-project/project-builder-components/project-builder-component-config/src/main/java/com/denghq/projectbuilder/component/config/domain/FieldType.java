package com.denghq.projectbuilder.component.config.domain;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

public class FieldType {
    //1.八种基本类型
    private boolean a;
    private Boolean a1;
    private char b;
    private Character b1;
    private int c;
    private Integer c1;
    private byte d;
    private Byte d1;
    private short e;
    private Short e1;
    private long f;
    private Long f1;
    private float g;
    private Float g1;
    private double h;
    private Double h1;

    // 2.时间和日期类型
    private java.sql.Timestamp i;
    private Date i1;

    //3.Number
    private BigInteger j;
    private BigDecimal j1;

    //4.String
    String s;

    //5.数组

    String[] aa;
    //6.集合
    List<Integer> bb;
    ArrayList<Integer> bb1;

    Set<Long> cdc;

    Map<String, Object> map;
    //7.对象
    FieldType ft;

    //1.八种基本类型
    /**
     boolean	class java.lang.Boolean
     char	class java.lang.Character
     int	    class java.lang.Integer
     byte	class java.lang.Byte
     short	class java.lang.Short
     long	class java.lang.Long
     float	class java.lang.Float
     double	class java.lang.Double
     */

    /**
     * 时间和日期类型
     *
     */
    /**
     * Type: class java.sql.Timestamp
     * GenericType: class java.sql.Timestamp
     *
     * Type: class java.util.Date
     * GenericType: class java.util.Date
     *
     */

    /**
     *  Number
     */
    /**
     * BigInteger
     * BigDecimal
     */
    public static void main(String[] args) {

        Class<?> c = FieldType.class;

        Arrays.stream(c.getDeclaredFields()).forEach(field -> {

            //数组类型
            System.out.println("isArray : " + field.getType().isArray());

            //集合类型
            System.out.println("isAssignableFrom Collection :  " + Collection.class.isAssignableFrom(field.getType()));
            if (Collection.class.isAssignableFrom(field.getType())) {
                ParameterizedType listGenericType = (ParameterizedType) field.getGenericType();
                Type[] listActualTypeArguments = listGenericType.getActualTypeArguments();
                System.out.println(listActualTypeArguments[listActualTypeArguments.length - 1]);
                for (int i = 0; i < listActualTypeArguments.length; i++) {
                    System.out.println("Arguments getTypeName: " + listActualTypeArguments[i].getTypeName());
                }
            }

            System.out.format("Type: %s%n", field.getType().getSimpleName());
            System.out.format("GenericType: %s%n", field.getGenericType().getTypeName());
            System.out.format("GenericType class: %s%n", field.getGenericType());

            System.out.println("---------------------------------------------------------------------------");
            System.out.println();
        });

    }
}
