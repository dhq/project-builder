package com.denghq.projectbuilder.component.config.metadata;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.lang.reflect.Field;
import java.util.List;

/**
 * 配置项
 */
@Data
public class ConfigOptionDescriptor {

    /**
     * 配置项key 同配置下唯一，在复杂类型的嵌套情况下可能为null，比如数组只有value没有key.
     */
    private String configKey;

    /**
     * 配置项名称
     */
    private String configName;

    /**
     * 备注
     */
    private String remark;

    /**
     * 配置值类型
     * NUMBER(1, "数值类型"),
     * STRING(2, "字符串"),
     * BOOLEAN(3, "布尔型"),
     * DATE(4, "日期类型"),
     * DATE_RANGE(5, "日期区间"),
     * ARRAY(6, "数组类型"),
     * DATA_LIST(7, "数据列表"),
     * URL(8, "自定义页面"),
     * OBJECT(9,"对象类型"),
     * NULL(-1,"未指定，自动推测")
     */
    private Integer configValueType;

    /**
     * 日期格式化 1：年月日时分秒 2：年月日 3：年月 4：年 5：时分秒 6：时分
     */
    private Integer dateFormat;

    /**
     * 非日期类型值来源 1：直接录入 2：从限定列表下拉选择  3.从接口获取结果中下拉选择
     * 默认1
     */
    private Integer dataSource;

    /**
     * 自定义数据源 dataSource为2时有效
     */
    private List<CustomDataSource> customDataList;

    /**
     * 数据源里面的Key字段  默认为 value 指定DataSourceApi时生效，
     */
    private String dataSourceValue;

    /**
     * 数据源里面的Text字段 默认为 text 指定DataSourceApi时生效，多个逗号分隔会按照顺序拼接 如配置了A,B两个字段显示为A--B
     */
    private String dataSourceText;

    /**
     * 下拉选择后显示的字段，比如下拉里面是abc-11111,但是选中后只想显示abc,此时可以用该字段
     */
    private String dataSourceValueFormat;

    /**
     * 获取下拉数据源的API接口地址 只支持get请求 dataSource为3时生效
     */
    private String dataSourceApi;

    /**
     * 配置项显示隐藏 依赖于下拉选择配置项的Key
     */
    private String parentKey;

    /**
     * 配置项显示隐藏 依赖于下拉选择配置项的哪个值Value
     */
    private Object parentValue;

    /**
     * 当ValueType为url时，对应的自定义配置页面地址，用于复杂个性化配置项
     */
    private String customPage;

    /**
     * configValueType为数据列表、obj 时配置信息，用来描述元素的字段信息
     */
    private List<ConfigOptionDescriptor> OptionSettings;

    /**
     * 配置项值
     */
    private Object configValue;

    /**
     *
     * 是否为主键列
     *
     */
    private Boolean primaryKey;

    /**
     * 配置项默认值
     */
    @JsonIgnore
    private Object defaultConfigValue;


    /**
     * 配置项对应配置类的field
     */
    @JsonIgnore
    private Field field;
}
