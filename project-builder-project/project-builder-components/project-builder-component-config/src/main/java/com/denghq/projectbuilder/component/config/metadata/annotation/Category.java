package com.denghq.projectbuilder.component.config.metadata.annotation;


import com.denghq.projectbuilder.component.config.metadata.enums.ConfigTypeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 配置类信息
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Category {

    /**
     * 配置类型：1.业务逻辑配置 2.运维配置 默认1
     *
     * @return
     */
    ConfigTypeEnum configType() default ConfigTypeEnum.BUSINESS;

    /**
     * 是否自动注册，默认 true
     *
     * @return
     */
    boolean autoRegiste() default true;

    /**
     * 所属项目/应用名称
     */
    String projectName();

    /**
     * 所属服务标识
     *
     * @return
     */
    String appNo() default "";

    /**
     * 配置项分类编号(标识)
     */
    String categoryNo();

    /**
     * 配置项分类名称
     */
    String categoryName();

    /**
     * 保存配置项值后端校验接口地址
     */
    String saveVerifyApi() default "";

}
