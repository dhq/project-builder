package com.denghq.projectbuilder.component.config.metadata.enums;

/**
 * 配置类型
 */
public enum ConfigTypeEnum {

    BUSINESS(1, "业务逻辑配置"),
    PROGRAM(2, "程序本身的配置");

    private final int value;
    private final String description;


    ConfigTypeEnum(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
