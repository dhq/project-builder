package com.denghq.projectbuilder.component.config.metadata.enums;

/**
 * 日期格式化
 */
public enum DateFormatEnum {
    DATETIME(1, "年月日时分秒"),
    DATE(2, "年月日"),
    YEAR_MONTH(3, "年月"),
    YEAR(4, "年"),
    TIME(5, "时分秒"),
    HOUR_MIN(6, "时分"),
    NULL(-1,"未指定" );

    private final int value;
    private final String description;

    DateFormatEnum(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
