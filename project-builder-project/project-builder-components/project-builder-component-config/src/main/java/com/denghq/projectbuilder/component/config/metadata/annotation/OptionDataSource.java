package com.denghq.projectbuilder.component.config.metadata.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义数据源格式
 */
@Target({})
@Retention(RetentionPolicy.RUNTIME)
public @interface OptionDataSource {

    /**
     * 值
     *
     * @return 值
     */
    String value();

    /**
     * 显示文本
     *
     * @return 显示文本
     */
    String text();
}
