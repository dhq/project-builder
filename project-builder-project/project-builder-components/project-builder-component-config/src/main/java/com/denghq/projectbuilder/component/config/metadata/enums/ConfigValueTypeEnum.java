package com.denghq.projectbuilder.component.config.metadata.enums;

/**
 * 配置值类型
 */
public enum ConfigValueTypeEnum {

    NUMBER(1, "数值类型"),
    STRING(2, "字符串"),
    BOOLEAN(3, "布尔型"),
    DATE(4, "日期类型"),
    DATE_RANGE(5, "日期区间"),
    ARRAY(6, "数组类型"),
    DATA_LIST(7, "数据列表"),
    URL(8, "自定义页面"),
    OBJECT(9,"对象类型"),
    NULL(-1,"未指定，自动推测")
    ;

    private final int value;
    private final String description;

    ConfigValueTypeEnum(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

}
