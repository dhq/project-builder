package com.denghq.projectbuilder.component.config.metadata;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

/**
 * 配置类描述信息
 */
@Data
public class ConfigDescriptor {

    /**
     * 配置类型：1.业务逻辑配置 2.运维配置 默认1
     *
     * @return
     */
    private int configType;

    /**
     * 是否需要自动配置
     */
    @JsonIgnore
    private boolean autoRegiste;

    /**
     * 参数统一前缀
     */
    @JsonIgnore
    private String prefix;


    /**
     * 所属项目/应用名称
     */
    private String projectName;

    /**
     * 所属项目/应用标识
     */
    private String appNo;

    /**
     * 配置项分类编号(标识)
     */
    private String categoryNo;

    /**
     * 配置项分类名称
     */
    private String categoryName;

    /**
     * 保存配置项值后端校验接口地址
     */
    private String saveVerifyApi;

    /**
     * 配置下配置项
     */
    private List<ConfigOptionDescriptor> optionSettings;


    /**
     * 配置类实际类型
     */
    @JsonIgnore
    private Class configClass;
}
