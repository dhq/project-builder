package com.denghq.projectbuilder.component.config.domain;


import com.denghq.projectbuilder.component.config.metadata.annotation.Category;
import com.denghq.projectbuilder.component.config.util.ClassScaner;
import lombok.Data;

import javax.annotation.PostConstruct;
import java.util.Set;

@Data
public class ConfigClassCollector {

    private Set<Class> configClasses;

    private static final String DEFAULT_BASE_PACKAGE = "com.denghq";

    private String basePackage;

    public ConfigClassCollector() {
        this.basePackage = DEFAULT_BASE_PACKAGE;
    }

    public ConfigClassCollector(String basePackage) {
        this.basePackage = basePackage;
    }

    @PostConstruct
    private void collect() {
        configClasses = ClassScaner.scan(basePackage, Category.class);
    }


}
