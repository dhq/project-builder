package com.denghq.projectbuilder.component.config.util;

import lombok.Builder;
import lombok.Data;
import org.springframework.core.type.classreading.MetadataReader;

@Data
@Builder
public class ClassScanResult {

    private Class aClass;

    private MetadataReader metadataReader;
}
