package com.denghq.projectbuilder.component.config.metadata;

import lombok.Data;

@Data
public class CustomDataSource {

    /**
     * 值
     */
    private String value;

    /**
     * 显示文本
     */
    private String text;

}
