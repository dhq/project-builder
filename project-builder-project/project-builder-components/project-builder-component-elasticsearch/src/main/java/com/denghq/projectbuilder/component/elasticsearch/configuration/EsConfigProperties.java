package com.denghq.projectbuilder.component.elasticsearch.configuration;

import com.denghq.projectbuilder.component.config.metadata.annotation.Category;
import com.denghq.projectbuilder.component.config.metadata.annotation.ConfigAttribute;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("com.denghq.es")
@Category(projectName = "基础配置", appNo = "common", categoryNo = "common/ElasticsearchProperties", categoryName = "Elasticsearch配置", autoRegiste = false)
@Data
public class EsConfigProperties {

    @ConfigAttribute(name = "ELK连接信息")
    private String eLKAddress;

    @ConfigAttribute(name = "ES集群地址")
    private String eSClusterAddress;
}
