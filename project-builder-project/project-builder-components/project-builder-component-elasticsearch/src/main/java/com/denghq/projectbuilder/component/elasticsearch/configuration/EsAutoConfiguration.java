package com.denghq.projectbuilder.component.elasticsearch.configuration;

import org.frameworkset.elasticsearch.boot.BBossESProperties;
import org.frameworkset.elasticsearch.boot.BBossESStarter;
import org.frameworkset.elasticsearch.boot.BaseESProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({BBossESProperties.class, EsConfigProperties.class})
public class EsAutoConfiguration {


    @Bean(
            initMethod = "start"
    )
    @ConditionalOnMissingBean
    public BBossESStarter bbossESStarter(BBossESProperties bBossESProperties, EsConfigProperties esConfigProperties) {
        if(bBossESProperties.getElasticsearch() == null){
            BaseESProperties.Elasticsearch elasticsearch = new BBossESProperties.Elasticsearch();
            elasticsearch.setRest(new BaseESProperties.Rest());
            bBossESProperties.setElasticsearch(elasticsearch);
        }
        bBossESProperties.getElasticsearch().getRest().setHostNames(esConfigProperties.getESClusterAddress());
        return new BBossESStarter();
    }

}
