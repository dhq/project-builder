package com.denghq.projectbuilder.component.msgbus.autoconfigure.rabbitmq.property;

import lombok.Data;

@Data
public class BindingKey {

    /**
     * 队列名称
     */
    private String queueName;

    /**
     * 绑定的路由键
     */
    private String routingKey;

}
