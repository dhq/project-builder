package com.denghq.projectbuilder.component.msgbus.sender.impl;

import com.denghq.projectbuilder.component.msgbus.sender.IMsgSendEvent;
import com.denghq.projectbuilder.component.msgbus.sender.IMsgSendEventHandler;

public abstract class SimpleMsgSendEventHandler implements IMsgSendEventHandler{

    @Override
    public void onSuccessEvent(IMsgSendEvent event) {

    }

    @Override
    public void onFailEvent(IMsgSendEvent event) {

    }

    @Override
    public void onMissEvent(IMsgSendEvent event) {

    }
}
