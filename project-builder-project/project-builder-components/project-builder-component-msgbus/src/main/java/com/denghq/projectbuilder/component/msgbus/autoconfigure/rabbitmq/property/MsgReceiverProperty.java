package com.denghq.projectbuilder.component.msgbus.autoconfigure.rabbitmq.property;

import lombok.Data;

/**
 * MsgReceiverProperty
 */
@Data
public class MsgReceiverProperty {

    /**
     * 消息处理启用的最大线程数，默认10
     */
    private Integer maxMsgProcessThreadNum = 10;

    /**
     * 消息处理最大消费者数量，默认1
     */
    private Integer maxConcurrentConsumers = 1;

    /**
     * 消息处理初始消费者数量，默认1
     */
    private Integer concurrentConsumers = 1;
}
