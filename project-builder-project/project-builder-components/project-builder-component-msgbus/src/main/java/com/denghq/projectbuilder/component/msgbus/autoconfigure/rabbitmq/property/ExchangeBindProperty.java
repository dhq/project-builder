package com.denghq.projectbuilder.component.msgbus.autoconfigure.rabbitmq.property;

import lombok.Data;

import java.util.List;


/**
 * 用于绑定路由键及发消息的主题
 */
@Data
public class ExchangeBindProperty {

    /**
     * 交换机名称
     */
    private String name;

    /**
     * 交换机类型
     */
    private String type;

    /**
     * 是否持久化
     */
    private boolean durable;

    /**
     * 是否自动删除
     */
    private boolean autoDelete;

    /**
     * 绑定的路由键
     */
    private List<BindingKey> bindings;

}
