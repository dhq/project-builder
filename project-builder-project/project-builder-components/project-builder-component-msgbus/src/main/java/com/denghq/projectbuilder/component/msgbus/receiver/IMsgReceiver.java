package com.denghq.projectbuilder.component.msgbus.receiver;

/**
 * 消息接收器
 */
public interface IMsgReceiver {

    /**
     * 开始接受消息
     */
    void startWork();

    /**
     * 结束接收消息
     */
    void stopWork();

}
