package com.denghq.projectbuilder.component.msgbus.sender;

/**
 * 消息发送事件处理器接口
 */
public interface IMsgSendEventHandler {

   /**
    * 发送成功
    * @param event 消息事件
    */
   void onSuccessEvent(IMsgSendEvent event);

   /**
    * 发送失败
    * @param event 消息事件
    */
   void onFailEvent(IMsgSendEvent event);

   /**
    * 消息丢失
    * @param event 消息事件
    */
   void onMissEvent(IMsgSendEvent event);
}
