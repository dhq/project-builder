package com.denghq.projectbuilder.component.msgbus.bus.impl;

import com.denghq.projectbuilder.component.msgbus.bus.IMsgProcessor;
import com.denghq.projectbuilder.component.msgbus.bus.IMsgBus;
import com.denghq.projectbuilder.component.msgbus.sender.IMsg;
import org.springframework.util.CollectionUtils;

import java.util.Collection;

/**
 * AbstractMsgBus
 */
public abstract class AbstractMsgBus implements IMsgBus{

    @Override
    public void subscribe(Collection<String> topicList, IMsgProcessor processer) {
        if (!CollectionUtils.isEmpty(topicList)) {
            topicList.forEach(topic ->
                    this.subscribe(topic, processer)
            );
        }

    }

    @Override
    public void publish(Collection<String> topicList, IMsg message) {
        if (!CollectionUtils.isEmpty(topicList)) {
            topicList.forEach(topic ->
                    this.publish(topic, message)
            );
        }
    }

    @Override
    public void publish(Collection<String> topicList, String message) {
        if (!CollectionUtils.isEmpty(topicList)) {
            topicList.forEach(topic ->
                    this.publish(topic, message)
            );
        }
    }
}
