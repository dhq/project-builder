package com.denghq.projectbuilder.component.msgbus.receiver.impl;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 绑定路由键
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BindRoutingKey {

    /**
     * 队列名称
     */
    private String queueName;

    /**
     * 交换机名称
     */
    private String exchangeName;

    /**
     * 路由键
     */
    private String routingKey;

}
