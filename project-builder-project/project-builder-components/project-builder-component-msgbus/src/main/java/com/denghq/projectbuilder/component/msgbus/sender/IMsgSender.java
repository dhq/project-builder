package com.denghq.projectbuilder.component.msgbus.sender;

/**
 * 消息发送接口
 */
public interface IMsgSender {

    /**
     * 设置消息确认事件监听器
     * @param listener 消息确认事件监听器
     */
    void setSendEventHandler(IMsgSendEventHandler listener);

    /**
     * 发送消息
     * @param topic 主题
     * @param msg 消息
     */
    void sendMsg(String topic,IMsg msg);
}
