package com.denghq.projectbuilder.component.msgbus.bus;

/**
 * 消息处理器接口
 */
public interface IMsgProcessor {

    /**
     * 处理消息
     * @param topic 来自主题
     * @param msg 消息内容
     */
    void execute(String topic,String msg);
}
