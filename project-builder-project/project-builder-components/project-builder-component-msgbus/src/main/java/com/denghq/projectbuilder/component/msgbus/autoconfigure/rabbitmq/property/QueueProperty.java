package com.denghq.projectbuilder.component.msgbus.autoconfigure.rabbitmq.property;

import lombok.Data;

import java.util.Map;

/**
 * QueueProperty
 */
@Data
public class QueueProperty {

    /**
     * 队列名称
     */
    private String name;

    /**
     * 是否持久化
     */
    private boolean durable = true;

    /**
     * 是否专用
     */
    private boolean exclusive = false;

    /**
     * 是否自动删除
     */
    private boolean autoDelete = false;

    /**
     * 其他参数
     */
    private Map<String, Object> arguments;

}
