package com.denghq.projectbuilder.component.msgbus.sender;

/**
 * 消息
 */
public interface  IMsg {

    /**
     * 消息内容
     * @return 消息内容
     */
    String getContent();

    /**
     * 消息标识
     * @return 消息标识，可用于做消息发送异步确认
     */
    String getMsgId();

}
