package com.denghq.projectbuilder.component.msgbus.bus;

import com.denghq.projectbuilder.component.msgbus.sender.IMsg;

import java.util.Collection;

/**
 * 消息总线接口
 */
public interface IMsgBus {

    /**
     * 订阅多个主题的消息
     * @param topicList 主题列表
     * @param msgProcesser 消息处理器
     */
    void subscribe(Collection<String> topicList, IMsgProcessor msgProcesser);

    /**
     * 订阅消息
     * @param topic 主题
     * @param msgProcesser 消息处理器
     */
    void subscribe(String topic, IMsgProcessor msgProcesser);

    /***
     * 发布消息(携带消息元数据)
     * @param topic 主题
     * @param message 消息
     */
    void publish(String topic, IMsg message);

    /***
     * 发布消息
     * @param topic 主题
     * @param message 消息
     */
    void publish(String topic, String message);

    /***
     * 发布消息到多个主题
     * @param topicList 主题列表
     * @param message 消息
     */
    void publish(Collection<String> topicList, IMsg message);

    /***
     * 发布消息到多个主题
     * @param topicList 主题列表
     * @param message 消息
     */
    void publish(Collection<String> topicList, String message);

}
