package com.denghq.projectbuilder.component.msgbus.sender.impl;

import com.denghq.projectbuilder.component.msgbus.sender.IMsg;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SimpleMsg implements IMsg{

    private String content;

    private String msgId;

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String getMsgId() {
        return msgId;
    }
}
