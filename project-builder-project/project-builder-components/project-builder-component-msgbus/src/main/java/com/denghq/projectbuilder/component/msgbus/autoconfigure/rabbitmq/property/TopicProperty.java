package com.denghq.projectbuilder.component.msgbus.autoconfigure.rabbitmq.property;

import lombok.Data;

/**
 * TopicProperty
 */
@Data
public class TopicProperty {
    /**
     * 主题名称
     */
    private String name;

    /**
     * 发送到的交换机
     */
    private String exchange;
}
