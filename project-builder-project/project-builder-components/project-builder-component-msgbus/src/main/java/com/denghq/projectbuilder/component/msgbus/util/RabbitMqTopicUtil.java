package com.denghq.projectbuilder.component.msgbus.util;

import java.util.regex.Pattern;

/**
 * 路由匹配工具类
 */
public class RabbitMqTopicUtil {

    public static boolean routingKeyMatch(String routingKeyExpression, String routingKey) {
        return Pattern.matches(routingKeyExpression.replaceAll("\\.", "\\\\" + ".").replaceAll("#", ".+"), routingKey);
    }
}
