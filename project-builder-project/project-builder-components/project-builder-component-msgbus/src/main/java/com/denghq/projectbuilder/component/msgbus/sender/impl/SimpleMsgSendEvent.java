package com.denghq.projectbuilder.component.msgbus.sender.impl;

import com.denghq.projectbuilder.component.msgbus.sender.IMsgSendEvent;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class SimpleMsgSendEvent implements IMsgSendEvent {

    private String msgId;

    @Override
    public String getMsgId() {
        return msgId;
    }

}
