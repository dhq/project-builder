package com.denghq.projectbuilder.component.msgbus.bus.impl;


import com.denghq.projectbuilder.common.util.BeanTool;
import com.denghq.projectbuilder.component.amqp.MsgDecorator;
import com.denghq.projectbuilder.component.amqp.MsgObjDecorator;
import com.denghq.projectbuilder.component.msgbus.sender.impl.SimpleMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 基于消息总线的消息发送工具类
 */
@Component
public class RabbitMqMsgBusUtil {

    @Autowired
    private RabbitMqMsgBus rabbitMqMsgBus;

    public void sendMsg(String topic, String msgId, MsgDecorator<?> msg) {
        this.rabbitMqMsgBus.publish(topic, new SimpleMsg(msgId, BeanTool.objectToJsonStr(msg)));
        this.rabbitMqMsgBus.publish(topic, new SimpleMsg(msgId, BeanTool.objectToJsonStr(msg)));
    }

    public void sendMsg(String topic, String msgId, MsgObjDecorator<?> msg) {
        this.rabbitMqMsgBus.publish(topic, new SimpleMsg(msgId, BeanTool.objectToJsonStr(msg)));
    }

    public void sendMsg(String topic, MsgDecorator<?> msg) {
        this.rabbitMqMsgBus.publish(topic, BeanTool.objectToJsonStr(msg));
    }

    public void sendMsg(String topic, MsgObjDecorator<?> msg) {
        this.rabbitMqMsgBus.publish(topic, BeanTool.objectToJsonStr(msg));
    }

}
