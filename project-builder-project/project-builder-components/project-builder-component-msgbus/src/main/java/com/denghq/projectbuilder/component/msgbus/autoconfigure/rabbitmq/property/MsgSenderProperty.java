package com.denghq.projectbuilder.component.msgbus.autoconfigure.rabbitmq.property;

import lombok.Data;

import java.util.List;

/**
 * MsgSenderProperty
 */
@Data
public class MsgSenderProperty {

    /**
     * 主题发送到的交换机配置
     */
    private List<TopicProperty> topics;

    /**
     * 主题默认发送的交换机
     */
    private String defaultExchange;
}
