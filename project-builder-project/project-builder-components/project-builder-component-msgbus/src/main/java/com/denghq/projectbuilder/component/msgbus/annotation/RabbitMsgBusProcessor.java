package com.denghq.projectbuilder.component.msgbus.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * rabbitmq消息处理器
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RabbitMsgBusProcessor {

    /**
     * 监听的主题，必填
     * @return
     */
    String topic() default "";
}
