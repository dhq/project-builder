package com.denghq.projectbuilder.component.msgbus.sender;

/**
 * 消息发送事件接口
 */
public interface IMsgSendEvent {

   /**
    * 获取消息id
    * @return 消息id
    */
   String getMsgId();
}
