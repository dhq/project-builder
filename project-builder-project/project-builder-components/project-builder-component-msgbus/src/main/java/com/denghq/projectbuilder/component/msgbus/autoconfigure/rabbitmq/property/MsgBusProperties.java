package com.denghq.projectbuilder.component.msgbus.autoconfigure.rabbitmq.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 *  MsgBusProperties
 */
@Data
@ConfigurationProperties(prefix = "com.denghq.component.msgbus.rabbitmq")
public class MsgBusProperties {

    /**
     * 声明d的队列
     */
    private List<QueueProperty> queueList;

    /**
     * 声明交换机及绑定路由键
     */
    private List<ExchangeBindProperty> exchangeBinder;

    private MsgSenderProperty sender;

    public MsgReceiverProperty msgReceiver;
}
