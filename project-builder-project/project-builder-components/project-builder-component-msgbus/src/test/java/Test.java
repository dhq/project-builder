import com.denghq.projectbuilder.component.msgbus.bus.IMsgBus;
import com.denghq.projectbuilder.component.msgbus.bus.impl.RabbitMqMsgBus;
import com.denghq.projectbuilder.component.msgbus.receiver.impl.BindRoutingKey;
import com.denghq.projectbuilder.component.msgbus.receiver.impl.RabbitMqMsgReceiver;
import com.denghq.projectbuilder.component.msgbus.sender.IMsgSendEvent;
import com.denghq.projectbuilder.component.msgbus.sender.IMsgSendEventHandler;
import com.denghq.projectbuilder.component.msgbus.sender.impl.RabbitMqMsgSender;
import com.denghq.projectbuilder.component.msgbus.sender.impl.SimpleMsgSendEventHandler;
import com.google.common.collect.Lists;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class Test {

    public static void main(String[] args) throws IOException, TimeoutException {

        //CachingConnectionFactory factory = new CachingConnectionFactory();
        //        factory.setAddresses();
        //
        //        factory.setUri("amqp://zhihao.miao:123456@192.168.1.131:5672");
        //        factory.setPublisherConfirms(true);

        CachingConnectionFactory factory = new CachingConnectionFactory();
        factory.setHost("192.168.0.41");
        factory.setPort(5672);
        factory.setUsername("admin");
        factory.setPassword("denghq");
        factory.setVirtualHost("/");//虚拟主机，可通过控制台查看

        Exchange exchange = new TopicExchange("denghq.Exchange.TianjinEPPV5");
        //1.队列名称,2.声明一个持久队列,3.声明一个独立队列,4.如果服务器在不再使用时自动删除队列
        Queue queue = new Queue("192.168.7.96_test", true, false, true);
        //sender
        RabbitMqMsgSender msgSender = new RabbitMqMsgSender(factory);
        IMsgSendEventHandler handler = new SimpleMsgSendEventHandler() {
            @Override
            public void onSuccessEvent(IMsgSendEvent event) {
                System.out.println("-----------------onSucessEvent-------------");
                System.out.println(event);
                System.out.println("-----------------onSucessEvent-------------");
            }

            @Override
            public void onFailEvent(IMsgSendEvent event) {
                System.out.println("-----------------onFailEvent-------------");
                System.out.println(event);
                System.out.println("-----------------onFailEvent-------------");
            }

            @Override
            public void onMissEvent(IMsgSendEvent event) {
                System.out.println("-----------------onMissEvent-------------");
                System.out.println(event);
                System.out.println("-----------------onMissEvent-------------");
            }
        };
        msgSender.setSendEventHandler(handler);

        //receiver
        RabbitMqMsgReceiver msgReceiver = new RabbitMqMsgReceiver(factory);

        List<BindRoutingKey> bindRoutingKeyList = Lists.newArrayList();
        bindRoutingKeyList.add(new BindRoutingKey(queue.getName(), exchange.getName(), "test.eventBus.#"));
        bindRoutingKeyList.add(new BindRoutingKey(queue.getName(), exchange.getName(), "test.eventBus1.1"));
        //绑定路由键
        msgReceiver.setBindRoutingKeyList(bindRoutingKeyList);

        //指定消费的队列
        msgReceiver.setConsumeQueueList(new String[]{queue.getName()});

        //声明队列
        List<Queue> qs = Lists.newArrayList();
        qs.add(queue);
        msgReceiver.setDeclareQueueList(qs);

        //声明交换机
        msgReceiver.setDeclareExchangeList(Arrays.asList(exchange));

        IMsgBus msgBus = new RabbitMqMsgBus(msgSender, msgReceiver);

        msgBus.publish("test.eventBus1.1","publish msg1");
        msgBus.publish("test.eventBus.2","publish msg2");
        msgBus.publish("test.eventBus.3","publish msg3");
        msgBus.publish("test.eventBus.4","publish msg4");
        msgBus.publish("test.eventBus.5","publish msg5");

        msgBus.subscribe("test.eventBus1.1",(String topic,String msg)->{
            while (true){
                System.out.println("111111111111111111111111>>>");
                System.out.println(topic);
                System.out.println(msg);
                System.out.println("<<<<111111111111111111111111");
            }
        });

        msgBus.subscribe("test.eventBus.#",(String topic,String msg)->{
            while (true){
                System.out.println(topic);
                System.out.println(msg);
            }
        });

        msgBus.publish("test.eventBus1.1","publish msg1.1-2");
    }
}
