/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.denghq.projectbuilder.component.kafka.autoconfigure;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Arrays;
import java.util.List;


/**
 * @author Gary Russell
 * @author Stephane Nicoll
 * @author Eddú Meléndez
 * @author Nakul Mishra
 * @since 1.5.0
 */
@Configuration
@ConditionalOnClass(KafkaTemplate.class)
@AutoConfigureBefore(org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration.class)
@EnableConfigurationProperties({KafkaProperties.class, KafkaConfigProperties.class})
public class KafkaAutoConfiguration {

    private final KafkaProperties properties;

    private final KafkaConfigProperties kafkaConfigProperties;


    public KafkaAutoConfiguration(KafkaProperties properties, KafkaConfigProperties kafkaConfigProperties) {
        this.properties = properties;
        this.kafkaConfigProperties = kafkaConfigProperties;
        this.compose(properties, kafkaConfigProperties);

    }

    private void compose(KafkaProperties properties, KafkaConfigProperties kafkaConfigProperties) {
        List<String> bootstrapServers = Arrays.asList(kafkaConfigProperties.getBootstrapServers().split(","));
        properties.getConsumer().setBootstrapServers(bootstrapServers);
        properties.getProducer().setBootstrapServers(bootstrapServers);
        properties.setBootstrapServers(bootstrapServers);
    }


}
