package com.denghq.projectbuilder.component.kafka.autoconfigure;

import com.denghq.projectbuilder.component.config.metadata.annotation.Category;
import com.denghq.projectbuilder.component.config.metadata.annotation.ConfigAttribute;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "spring.kafka")
@Category(projectName = "基础配置", appNo = "smw", categoryNo = "smw/KafkaConfig", categoryName = "Kafka消息队列配置")
@Data
public class KafkaConfigProperties {

    @ConfigAttribute(name = "集群服务地址", remark = "多个地址逗号分隔，eg：192.168.10.10:9092,192.168.10.11:9092")
    private String bootstrapServers;

}
