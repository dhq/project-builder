package com.denghq.projectbuilder.component.basedatacache.autoconfigure;

import com.denghq.projectbuilder.component.basedatacache.cache.ICacheDomain;

/**
 * 自定义扩展缓存标识接口，实现该接口并添加到spring容器，可实现扩展缓存组件管理的缓存
 * @param <T>
 */
public interface ICustomCacheDomain<T> extends ICacheDomain<T> {

}
