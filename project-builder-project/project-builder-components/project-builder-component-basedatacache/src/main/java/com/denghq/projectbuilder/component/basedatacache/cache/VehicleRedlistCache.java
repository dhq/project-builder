package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import com.denghq.projectbuilder.component.remote.model.VehicleRedlistModel;
import com.google.common.collect.Lists;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 红名单缓存
 */
@Data
@Component
public class VehicleRedlistCache extends AbstractCacheDomain<List<VehicleRedlistModel>> implements ICommonCacheDomain<List<VehicleRedlistModel>> {

    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private List<VehicleRedlistModel> cacheData;

    @Autowired
    private com.denghq.projectbuilder.component.remote.service.VehicleRedlistService VehicleRedlistService;

    public VehicleRedlistCache() {
        this.cacheName = CacheTypeEnum.AllVehicleRedlist.getCacheName();
        this.cacheKey = CacheTypeEnum.AllVehicleRedlist.name();
        this.topicName = CacheTypeEnum.AllVehicleRedlist.getTopic();
        this.loadSuccess = false;
        cacheData = Lists.newArrayList();
    }

    @Override
    public int initLoad() {
        List<VehicleRedlistModel> all = VehicleRedlistService.all();
       /* this.cacheData.clear();
        this.cacheData.addAll(all);*/
        this.cacheData = all;
        return all.size();
    }

    @Override
    public int update(String msg) {
        return initLoad();
    }
}
