package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import com.denghq.projectbuilder.component.remote.model.CommonStaffModel;
import com.denghq.projectbuilder.component.remote.service.CommonStaffService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 人员缓存
 */
@Data
@Component
public class StaffCodeMapCache extends AbstractCacheDomain<Map<String, CommonStaffModel>> implements ICommonCacheDomain<Map<String, CommonStaffModel>> {

    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private ConcurrentHashMap<String, CommonStaffModel> cacheData;

    @Autowired
    private CommonStaffService commonStaffService;

    public StaffCodeMapCache() {
        this.cacheName = CacheTypeEnum.AllCodeMapStaff.getCacheName();
        this.cacheKey = CacheTypeEnum.AllCodeMapStaff.name();
        this.topicName = CacheTypeEnum.AllCodeMapStaff.getTopic();
        this.loadSuccess = false;
        cacheData = new ConcurrentHashMap<>();
    }

    @Override
    public int initLoad() {

        List<CommonStaffModel> all = commonStaffService.all();

       /* this.cacheData.clear();
        if (!CollectionUtils.isEmpty(all)) {
            all.stream().forEach(d -> this.cacheData.put(d.getDeviceid(), d));
        }*/
        ConcurrentHashMap<String, CommonStaffModel> newData = new ConcurrentHashMap<>();
        if (!CollectionUtils.isEmpty(all)) {
            all.stream().forEach(d -> newData.put(d.getStaffno(), d));
        }
        this.cacheData = newData;
        return all.size();
    }

    @Override
    public int update(String msg) {
        return initLoad();
    }
}
