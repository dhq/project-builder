package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import com.denghq.projectbuilder.component.remote.model.UserModel;
import com.denghq.projectbuilder.component.remote.service.SysUserService;
import com.google.common.collect.Lists;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户信息缓存
 */
@Data
@Component
public class UserCache extends AbstractCacheDomain<List<UserModel>> implements ICommonCacheDomain<List<UserModel>> {

    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private List<UserModel> cacheData;

    @Autowired
    private SysUserService sysUserService;

    public UserCache() {
        this.cacheName = CacheTypeEnum.AllUser.getCacheName();
        this.cacheKey = CacheTypeEnum.AllUser.name();
        this.topicName = CacheTypeEnum.AllUser.getTopic();
        this.loadSuccess = false;
        cacheData = Lists.newArrayList();
    }

    @Override
    public int initLoad() {
        List<UserModel> all = sysUserService.findAll();
       /* this.cacheData.clear();
        this.cacheData.addAll(all);*/
        this.cacheData = all;
        return all.size();
    }

    @Override
    public int update(String msg) {
        return initLoad();
    }
}
