package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.remote.model.RecogRuleModel;
import com.denghq.projectbuilder.component.remote.service.RecogRuleService;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 人脸识别范围缓存
 */
@Data
@Component
public class FaceRecogRangeMapCache extends AbstractCacheDomain<Map<String, RecogRuleModel>> implements ICommonCacheDomain<Map<String, RecogRuleModel>> {

    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private ConcurrentHashMap<String, RecogRuleModel> cacheData;

    private final String ruleType = "002";

    @Autowired
    private RecogRuleService recogRuleService;

    public FaceRecogRangeMapCache() {
        this.cacheName = CacheTypeEnum.AllKeyMapFaceRecogRange.getCacheName();
        this.cacheKey = CacheTypeEnum.AllKeyMapFaceRecogRange.name();
        this.topicName = CacheTypeEnum.AllKeyMapFaceRecogRange.getTopic();
        this.loadSuccess = false;
        cacheData = new ConcurrentHashMap<>();
    }

    @Override
    public int initLoad() {
        List<RecogRuleModel> all = recogRuleService.all(ruleType);
       /* this.cacheData.clear();
        if (!CollectionUtils.isEmpty(all)) {
            all.stream().forEach(d -> this.cacheData.put(d.getAssetsNo(), d));
        }*/
        ConcurrentHashMap<String, RecogRuleModel> newData = new ConcurrentHashMap<>();
        if (!CollectionUtils.isEmpty(all)) {
            all.stream().forEach(d -> newData.put(d.getAssetsNo(), d));
        }
        this.cacheData = newData;
        return all.size();
    }

    @Override
    public int update(String msg) {
        return initLoad();
    }
}
