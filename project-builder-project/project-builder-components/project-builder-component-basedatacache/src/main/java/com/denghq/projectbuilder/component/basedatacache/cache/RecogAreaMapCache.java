package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.remote.model.RecogAreaModel;
import com.denghq.projectbuilder.component.remote.service.RecogAreaService;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 人脸识别范围缓存
 */
@Data
@Component
public class RecogAreaMapCache extends AbstractCacheDomain<Map<String, RecogAreaModel>> implements ICommonCacheDomain<Map<String, RecogAreaModel>> {

    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private ConcurrentHashMap<String, RecogAreaModel> cacheData;

    @Autowired
    private RecogAreaService recogAreaService;

    public RecogAreaMapCache() {
        this.cacheName = CacheTypeEnum.AllKeyMapRecogArea.getCacheName();
        this.cacheKey = CacheTypeEnum.AllKeyMapRecogArea.name();
        this.topicName = CacheTypeEnum.AllKeyMapRecogArea.getTopic();
        this.loadSuccess = false;
        cacheData = new ConcurrentHashMap<>();
    }

    @Override
    public synchronized int initLoad() {
        List<RecogAreaModel> all = recogAreaService.all();
        /*this.cacheData.clear();
        if (!CollectionUtils.isEmpty(all)) {
            all.stream().forEach(d -> this.cacheData.put(d.getAssetsNo() + d.getLaneNo(), d));
        }*/
        ConcurrentHashMap<String, RecogAreaModel> newData = new ConcurrentHashMap<>();
        if (!CollectionUtils.isEmpty(all)) {
            all.stream().forEach(d -> newData.put(d.getAssetsNo() + d.getLaneNo(), d));
        }
        this.cacheData = newData;
        return all.size();
    }

    @Override
    public synchronized int update(String msg) {
        return initLoad();
    }
}
