package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.remote.model.DictModel;
import com.denghq.projectbuilder.component.remote.service.SysDictService;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * 字典缓存
 */
@Data
public class SomeDictCache extends AbstractCacheDomain<Map<String, List<DictModel>>> implements ICommonCacheDomain<Map<String, List<DictModel>>> {

    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private Map<String, List<DictModel>> cacheData;

    private final List<String> codeList;

    private SysDictService sysDictService;

    public SomeDictCache(List<String> codeList, SysDictService sysDictService) {
        this.cacheName = CacheTypeEnum.SomeDict.getCacheName();
        this.cacheKey = CacheTypeEnum.SomeDict.name();
        this.topicName = CacheTypeEnum.SomeDict.getTopic();
        this.loadSuccess = false;
        this.codeList = codeList;
        this.sysDictService = sysDictService;
        cacheData = Maps.newConcurrentMap();
    }

    @Override
    public int initLoad() {
        if (CollectionUtils.isEmpty(codeList)) {
            return 0;
        }
        Map<String, List<DictModel>> someDicts = sysDictService.getSomeDictsWithOutCache(codeList);
       /* this.cacheData.clear();
        this.cacheData.putAll(someDicts);*/
        this.cacheData = someDicts;
        return someDicts.size();
    }

    @Override
    public int update(String msg) {
        return initLoad();
    }

    public Map<String, DictModel> getDictMapByKind(String kind) {
        Map<String, List<DictModel>> loadedCacheData = this.getLoadedCacheData();
        List<DictModel> dictModels = loadedCacheData.get(kind);
        Map<String, DictModel> res = Maps.newHashMap();
        dictModels.forEach(e -> res.put(e.getDictionaryNo(), e));
        return res;
    }

    public Map<String, DictModel> getDictMapByKindAndParentId(String kind, String parentId, boolean recursion) {
        Map<String, List<DictModel>> loadedCacheData = this.getLoadedCacheData();
        List<DictModel> dictList = loadedCacheData.get(kind);
        Map<String, DictModel> res = Maps.newHashMap();
        fillMapChildren(dictList, parentId, res, recursion);
        return res;
    }

    private void fillMapChildren(List<DictModel> dictList, String parentId, Map<String, DictModel> res, boolean recursion) {
        dictList.forEach(e -> {
            if (StringUtils.isBlank(parentId)) {
                if (StringUtils.isBlank(e.getParentId())) {
                    res.put(e.getDictionaryNo(), e);
                    if (recursion) {
                        fillMapChildren(dictList, e.getDictionaryNo(), res, true);
                    }
                }
            } else if (parentId.equals(e.getParentId())) {
                res.put(e.getDictionaryNo(), e);
                if (recursion) {
                    fillMapChildren(dictList, e.getDictionaryNo(), res, true);
                }
            }
        });
    }

    public List<DictModel> getDictListByKind(String kind) {
        Map<String, List<DictModel>> loadedCacheData = this.getLoadedCacheData();
        return loadedCacheData.get(kind);
    }


    public List<DictModel> getDictListByKindAndParentId(String kind, String parentId, boolean recursion) {
        List<DictModel> dictList = this.getDictListByKind(kind);
        List<DictModel> res = Lists.newArrayList();
        fillChildren(dictList, parentId, res, recursion);
        return res;
    }

    private void fillChildren(List<DictModel> dictList, String parentId, List<DictModel> res, boolean recursion) {
        dictList.forEach(e -> {
            if (StringUtils.isBlank(parentId)) {
                if (StringUtils.isBlank(e.getParentId())) {
                    res.add(e);
                    if (recursion) {
                        fillChildren(dictList, e.getDictionaryNo(), res, true);
                    }
                }
            } else if (parentId.equals(e.getParentId())) {
                res.add(e);
                if (recursion) {
                    fillChildren(dictList, e.getDictionaryNo(), res, true);
                }
            }
        });
    }
}
