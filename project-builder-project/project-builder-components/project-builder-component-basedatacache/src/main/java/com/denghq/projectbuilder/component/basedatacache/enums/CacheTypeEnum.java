package com.denghq.projectbuilder.component.basedatacache.enums;

/**
 * 通用缓存信息枚举
 */
public enum CacheTypeEnum {

    AllUser("Database.Changed.User", "所有用户信息缓存"),
    AllDevice("Database.Changed.FrontDevice", "所有设备信息"),
    AllIdMapDevice("Database.Changed.FrontDevice", "所有设备信息(id map)"),
    AllCodeMapDevice("Database.Changed.FrontDevice", "所有设备信息(设备编号 map)"),
    AllSpotting("Database.Changed.Spotting", "所有路口信息"),
    SomeDict("Database.Changed.Dictionary", "字典数据信息"),
    AllDeviceGroup("Database.Changed.FrontdeviceGroup", "所有设备组信息"),
    AllSpottingGroup("Database.Changed.SpottingGroup", "所有路口组信息"),
    AllVehicleRedlist("Database.Changed.VehicleRedlist", "所有红名单信息"),
    AllAreaConfig("Database.Changed.Area", "所有区域配置信息"),
    AllIdMapSpotting("Database.Changed.Spotting", "所有路口信息(id map)"),
    AllCodeMapSpotting("Database.Changed.Spotting", "所有路口信息(id map)"),
    AllIdMapCompany("Database.Changed.Company", "所有单位信息(id map)"),
    AllIdMapProject("Database.Changed.Project", "所有项目信息(id map)"),
    AllIdMapStaff("Database.Changed.Staff", "所有人员信息(id map)"),
    AllCodeMapStaff("Database.Changed.Staff", "所有人员信息(code map)"),
    Department("Database.Changed.Department", "部门信息缓存"),

    AllAreaConfigRangeMap("Database.Changed.Area", "所有区域配置范围(id map)"),
    AllKeyMapRecogArea("Database.Changed.StructRecogArea", "识别区域> map)"),
    AllKeyMapVehicleRecogRange("Database.Changed.StructRecogRule,Database.Changed.FrontdeviceGroup", "识别规则<车辆识别范围> map)"),
    AllKeyMapFaceRecogRange("Database.Changed.StructRecogRule,Database.Changed.FrontdeviceGroup", "识别规则<人脸识别范围> map)"),
    AllSpottingWithGroupMap("Database.Changed.SpottingGroup", "所有关联了路口组的路口信息，包含了关联的路口组信息");

    private final String topic;

    private final String cacheName;

    CacheTypeEnum(String topic, String cacheName) {
        this.topic = topic;
        this.cacheName = cacheName;
    }

    public String getCacheName() {
        return this.cacheName;
    }

    public String getTopic() {
        return this.topic;
    }
}
