package com.denghq.projectbuilder.component.basedatacache.cache;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import com.denghq.projectbuilder.component.remote.service.CommonAreaConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * 自定义区域范围缓存
 */
@Data
@Component
public class AreaConfigRangeMapCache extends AbstractCacheDomain<Map<String, List<BigDecimal[]>>> implements ICommonCacheDomain<Map<String, List<BigDecimal[]>>> {

    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private ConcurrentHashMap<String, List<BigDecimal[]>> cacheData;

    @Autowired
    private CommonAreaConfigService commonAreaConfigService;

    public AreaConfigRangeMapCache() {
        this.cacheName = CacheTypeEnum.AllAreaConfigRangeMap.getCacheName();
        this.cacheKey = CacheTypeEnum.AllAreaConfigRangeMap.name();
        this.topicName = CacheTypeEnum.AllAreaConfigRangeMap.getTopic();
        this.loadSuccess = false;
        cacheData = new ConcurrentHashMap<>();
    }

    @Override
    public int initLoad() {
        this.cacheData = commonAreaConfigService.allRange();
        return this.cacheData.size();
    }

    @Override
    public int update(String msg) {
        return initLoad();
    }
}
