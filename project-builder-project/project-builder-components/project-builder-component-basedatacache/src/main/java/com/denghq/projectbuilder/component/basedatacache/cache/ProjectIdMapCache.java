package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import com.denghq.projectbuilder.component.remote.model.CommonProjectModel;
import com.denghq.projectbuilder.component.remote.service.CommonProjectService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 路口缓存
 */
@Data
@Component
public class ProjectIdMapCache extends AbstractCacheDomain<Map<String, CommonProjectModel>> implements ICommonCacheDomain<Map<String, CommonProjectModel>> {
    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private ConcurrentHashMap<String, CommonProjectModel> cacheData;

    @Autowired
    private CommonProjectService commonProjectService;

    public ProjectIdMapCache() {
        this.cacheName = CacheTypeEnum.AllIdMapProject.getCacheName();
        this.cacheKey = CacheTypeEnum.AllIdMapProject.name();
        this.topicName = CacheTypeEnum.AllIdMapProject.getTopic();
        this.loadSuccess = false;
        cacheData = new ConcurrentHashMap<>();
    }

    @Override
    public int initLoad() {
        List<CommonProjectModel> all = commonProjectService.all();
        /*this.cacheData.clear();
        if (!CollectionUtils.isEmpty(all)) {
            all.stream().forEach(e -> this.cacheData.put(e.getProjectid(), e));
        }*/
        ConcurrentHashMap<String, CommonProjectModel> newData = new ConcurrentHashMap<>();
        if (!CollectionUtils.isEmpty(all)) {
            all.stream().forEach(e -> newData.put(e.getProjectid(), e));
        }
        this.cacheData = newData;
        return all.size();
    }

    @Override
    public int update(String msg) {
        return initLoad();
    }
}
