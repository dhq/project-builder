package com.denghq.projectbuilder.component.basedatacache.cache;

/**
 * 缓存域模型对象抽象接口
 * @param <T>
 */
public abstract class AbstractCacheDomain<T> implements ICacheDomain<T> {

    /**
     * 获取已加载的缓存数据
     * @return 已加载的缓存数据
     */
    public T getLoadedCacheData() {
        if (this.getLoadSuccess()) {
            return this.getCacheData();
        } else {
            this.initLoad();
            this.setLoadSuccess(true);
            return this.getCacheData();
        }
    }
}
