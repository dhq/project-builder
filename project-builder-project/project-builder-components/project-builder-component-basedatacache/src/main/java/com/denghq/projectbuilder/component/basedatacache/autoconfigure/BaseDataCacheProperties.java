package com.denghq.projectbuilder.component.basedatacache.autoconfigure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * BaseDataCacheProperties
 */
@ConfigurationProperties(prefix = "com.denghq.component.basedatacache")
@Data
public class BaseDataCacheProperties {

    /**
     * 失败重试次数
     */
    private Integer failRetry = 3;

    /**
     *  缓存失效时间，单位：分
     */
    private Integer cacheTimeOutMin = 30;

    /**
     * 最大启动线程数
     */
    private Integer maxThreadNum = 10;

    /**
     * 失败重试最大时间间隔，单位：秒
     */
    private Integer failRetryMaxSecond = 10;

    /**
     * 失败重试最小时间间隔，单位：秒
     */
    private Integer failRetryMinSecond = 3;

    /**
     * 使用的通用缓存
     */
    private List<String> useCache;

    /**
     * 加载的字典编码列表
     */
    public List<String> dictCodes;
}
