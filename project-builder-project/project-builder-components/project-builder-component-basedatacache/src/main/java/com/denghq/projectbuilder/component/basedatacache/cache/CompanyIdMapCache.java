package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.remote.model.CommonCompanyModel;
import com.denghq.projectbuilder.component.remote.service.CommonCompanyService;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 路口缓存
 */
@Data
@Component
public class CompanyIdMapCache extends AbstractCacheDomain<Map<String, CommonCompanyModel>> implements ICommonCacheDomain<Map<String, CommonCompanyModel>> {
    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private ConcurrentHashMap<String, CommonCompanyModel> cacheData;

    @Autowired
    private CommonCompanyService commonCompanyService;

    public CompanyIdMapCache() {
        this.cacheName = CacheTypeEnum.AllIdMapCompany.getCacheName();
        this.cacheKey = CacheTypeEnum.AllIdMapCompany.name();
        this.topicName = CacheTypeEnum.AllIdMapCompany.getTopic();
        this.loadSuccess = false;
        cacheData = new ConcurrentHashMap<>();
    }

    @Override
    public int initLoad() {
        List<CommonCompanyModel> all = commonCompanyService.all();
       /* this.cacheData.clear();
        if (!CollectionUtils.isEmpty(all)) {
            all.stream().forEach(e -> this.cacheData.put(e.getCompanyid(), e));
        }*/
        ConcurrentHashMap<String, CommonCompanyModel> newData = new ConcurrentHashMap<>();
        if (!CollectionUtils.isEmpty(all)) {
            all.stream().forEach(e -> newData.put(e.getCompanyid(), e));
        }
        this.cacheData = newData;
        return all.size();
    }

    @Override
    public int update(String msg) {
        return initLoad();
    }
}
