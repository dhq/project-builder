package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import com.denghq.projectbuilder.component.remote.model.CommonFrontdeviceGroupModel;
import com.denghq.projectbuilder.component.remote.service.CommonFrontdeviceGroupService;
import com.google.common.collect.Lists;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 设备组缓存
 */
@Component
@Data
public class DeviceGroupCache extends AbstractCacheDomain<List<CommonFrontdeviceGroupModel>> implements ICommonCacheDomain<List<CommonFrontdeviceGroupModel>> {

    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private List<CommonFrontdeviceGroupModel> cacheData;

    @Autowired
    private CommonFrontdeviceGroupService commonFrontdeviceGroupService;

    public DeviceGroupCache(){
        this.cacheName = CacheTypeEnum.AllDeviceGroup.getCacheName();
        this.cacheKey = CacheTypeEnum.AllDeviceGroup.name();
        this.topicName = CacheTypeEnum.AllDeviceGroup.getTopic();
        this.loadSuccess = false;
        cacheData = Lists.newArrayList();
    }
    @Override
    public int initLoad() {
        List<CommonFrontdeviceGroupModel> all = commonFrontdeviceGroupService.all();
       /* this.cacheData.clear();
        this.cacheData.addAll(all);*/
        this.cacheData = all;
        return all.size();
    }

    @Override
    public int update(String msg) {
        return initLoad();
    }
}
