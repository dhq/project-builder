package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import com.denghq.projectbuilder.component.remote.model.CommonSpottingGroupModel;
import com.denghq.projectbuilder.component.remote.service.CommonSpottingGroupService;
import com.google.common.collect.Lists;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 路口组缓存
 */
@Component
@Data
public class SpottingGroupCache extends AbstractCacheDomain<List<CommonSpottingGroupModel>> implements ICommonCacheDomain<List<CommonSpottingGroupModel>> {

    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private List<CommonSpottingGroupModel> cacheData;

    @Autowired
    private CommonSpottingGroupService commonSpottingGroupService;

    public SpottingGroupCache() {
        this.cacheName = CacheTypeEnum.AllSpottingGroup.getCacheName();
        this.cacheKey = CacheTypeEnum.AllSpottingGroup.name();
        this.topicName = CacheTypeEnum.AllSpottingGroup.getTopic();
        this.loadSuccess = false;
        cacheData = Lists.newArrayList();
    }

    @Override
    public int initLoad() {
        List<CommonSpottingGroupModel> all = commonSpottingGroupService.all();
       /* this.cacheData.clear();
        this.cacheData.addAll(all);*/
        this.cacheData = all;
        return all.size();
    }

    @Override
    public int update(String msg) {
        return initLoad();
    }
}
