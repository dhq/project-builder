package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.common.util.BeanTool;
import com.denghq.projectbuilder.component.amqp.MqChangeTypeEnum;
import com.denghq.projectbuilder.component.amqp.MsgDecorator;
import com.denghq.projectbuilder.component.amqp.MsgDecoratorData;
import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import com.denghq.projectbuilder.component.remote.model.CommonSpottingModel;
import com.denghq.projectbuilder.component.remote.service.CommonSpottingService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 路口缓存
 */
@Data
@Component
@Slf4j
public class SpottingCache extends AbstractCacheDomain<List<CommonSpottingModel>> implements ICommonCacheDomain<List<CommonSpottingModel>> {
    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private List<CommonSpottingModel> cacheData;

    @Autowired
    private CommonSpottingService commonSpottingService;

    public SpottingCache() {
        this.cacheName = CacheTypeEnum.AllSpotting.getCacheName();
        this.cacheKey = CacheTypeEnum.AllSpotting.name();
        this.topicName = CacheTypeEnum.AllSpotting.getTopic();
        this.loadSuccess = false;
        cacheData = Lists.newArrayList();
    }

    @Override
    public synchronized int initLoad() {
        List<CommonSpottingModel> all = commonSpottingService.all();
       /* this.cacheData.clear();
        this.cacheData.addAll(all);*/
        this.cacheData = all;
        return all.size();
    }

    @Override
    public synchronized int update(String msg) {
        if (!this.loadSuccess) {
            return this.getLoadedCacheData().size();
        } else {
            MsgDecorator<MsgDecoratorData> msgBean = BeanTool.jsonStrToObject(msg, new TypeReference<MsgDecorator<MsgDecoratorData>>() {
            });
            //找到删除的路口，新增的路口，修改的路口。

            if (msgBean != null && !CollectionUtils.isEmpty(msgBean.getData())) {
                //1、查出修改和新增的
                List<MsgDecoratorData> data = msgBean.getData();
                List<String> delIdList = Lists.newArrayList();
                List<String> updateIdList = Lists.newArrayList();
                List<String> newIdList = Lists.newArrayList();
                Set<String> refreshIdSet = Sets.newHashSet();//需要拉取最新数据的id
                data.forEach(e -> {
                    if (MqChangeTypeEnum.Insert.name().equals(e.getChangeType())) {
                        newIdList.add(e.getId());
                        refreshIdSet.add(e.getId());
                    } else if (MqChangeTypeEnum.Update.name().equals(e.getChangeType())) {
                        updateIdList.add(e.getId());
                        refreshIdSet.add(e.getId());
                    } else if (MqChangeTypeEnum.Delete.name().equals(e.getChangeType())) {
                        delIdList.add(e.getId());
                    }
                });

                Map<String, CommonSpottingModel> dbMap = getSpottingByIds(commonSpottingService, refreshIdSet).stream().collect(Collectors.toMap(CommonSpottingModel::getSpottingid, a -> a, (k1, k2) -> k1));
                Collection<CommonSpottingModel> loadedCacheData = this.getLoadedCacheData();
                List<CommonSpottingModel> newData = Lists.newArrayList();
                //使用copyOnWrite模式
                loadedCacheData.forEach(e -> {
                    String id = e.getSpottingid();
                    //删除
                    if (delIdList.contains(id)) {
                        return;
                    }
                    //修改
                    if (updateIdList.contains(id)) {
                        if (dbMap.get(id) != null && dbMap.get(id).getSpottingid() != null) {
                            newData.add(dbMap.get(id));
                        }
                        return;
                    } else {//没变
                        newData.add(e);
                        return;
                    }

                });
                //新增
                newIdList.forEach(id -> {
                    CommonSpottingModel model = dbMap.get(id);
                    if (model != null) {
                        newData.add(model);
                    }
                });

                this.cacheData = newData;
            }

            return this.cacheData.size();
        }
    }

    public static Collection<CommonSpottingModel> getSpottingByIds(CommonSpottingService commonSpottingService, Set<String> refreshIdSet) {
        if (CollectionUtils.isEmpty(refreshIdSet)) {
            log.debug("未加载路口记录");
            return Lists.newArrayList();
        }
        List<CommonSpottingModel> spottingList = commonSpottingService.getSpottingByIds(refreshIdSet);
        log.debug("重新加载{}条路口记录", spottingList.size());
        return spottingList;
    }

}
