package com.denghq.projectbuilder.component.basedatacache.cache;

/**
 * 缓存域模型对象
 * @param <T>
 */
public interface ICacheDomain<T>{

    /**
     * 缓存名称
     * @return 缓存名称
     */
    String getCacheName();

    /**
     *  缓存标识
     * @return 缓存标识
     */
    String getCacheKey();

    /**
     * 获取缓存数据
     * @return 获取缓存数据
     */
    T getCacheData();

    /**
     * 是否成功加载过缓存
     * @return 是否成功加载过缓存
     */
    Boolean getLoadSuccess();

    /**
     * 设置是否成功加载过缓存
     * @param bool
     */
    void setLoadSuccess(Boolean bool);

    /**
     * 获取监听的主题（消息路由键），返回null表示不监听消息
     * @return 监听的主题（消息路由键）
     */
    String getTopicName();

    /**
     * 初始化加载缓存
     * @return 影响条数
     */
    int initLoad();

    /**
     * 更新缓存
     * @return 影响条数
     * @param msg
     */
    int update(String msg);
}
