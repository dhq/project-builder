package com.denghq.projectbuilder.component.basedatacache.builder;

import com.denghq.projectbuilder.component.basedatacache.cache.ICacheDomain;

import java.util.List;

/**
 * 缓存对象容器接口
 */
public interface ICacheDomainListContainer {

    /**
     * 获取需加载的缓存对象列表
     * @return 需加载的缓存对象列表
     */
    List<ICacheDomain> getCacheDomainList();
}
