package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.common.util.BeanTool;
import com.denghq.projectbuilder.component.amqp.MqChangeTypeEnum;
import com.denghq.projectbuilder.component.amqp.MsgDecorator;
import com.denghq.projectbuilder.component.amqp.MsgDecoratorData;
import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.remote.model.CommonSpottingModel;
import com.denghq.projectbuilder.component.remote.service.CommonSpottingService;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * 路口缓存
 */
@Data
@Component
public class SpottingIdMapCache extends AbstractCacheDomain<Map<String, CommonSpottingModel>> implements ICommonCacheDomain<Map<String, CommonSpottingModel>> {
    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private Map<String, CommonSpottingModel> cacheData;

    @Autowired
    private CommonSpottingService commonSpottingService;

    public SpottingIdMapCache() {
        this.cacheName = CacheTypeEnum.AllIdMapSpotting.getCacheName();
        this.cacheKey = CacheTypeEnum.AllIdMapSpotting.name();
        this.topicName = CacheTypeEnum.AllIdMapSpotting.getTopic();
        this.loadSuccess = false;
        cacheData = new HashMap<>();
    }

    @Override
    public int initLoad() {
        List<CommonSpottingModel> all = commonSpottingService.all();
        /*this.cacheData.clear();
        if (!CollectionUtils.isEmpty(all)) {
            all.stream().forEach(e -> this.cacheData.put(e.getSpottingid(), e));
        }*/
        ConcurrentHashMap<String, CommonSpottingModel> newData = new ConcurrentHashMap<>();
        if (!CollectionUtils.isEmpty(all)) {
            all.stream().forEach(e -> newData.put(e.getSpottingid(), e));
        }
        this.cacheData = newData;
        return all.size();
    }

    @Override
    public int update(String msg) {
        if (!this.loadSuccess) {
            return this.getLoadedCacheData().size();
        } else {
            MsgDecorator<MsgDecoratorData> msgBean = BeanTool.jsonStrToObject(msg, new TypeReference<MsgDecorator<MsgDecoratorData>>() {
            });
            //找到删除的设备，新增的设备，修改的设备。

            if (msgBean != null && !CollectionUtils.isEmpty(msgBean.getData())) {
                //1、查出修改和新增的
                List<MsgDecoratorData> data = msgBean.getData();
                List<String> delIdList = Lists.newArrayList();
                List<String> updateIdList = Lists.newArrayList();
                List<String> newIdList = Lists.newArrayList();
                Set<String> refreshIdSet = Sets.newHashSet();//需要拉取最新数据的id
                data.forEach(e -> {
                    if (MqChangeTypeEnum.Insert.name().equals(e.getChangeType())) {
                        newIdList.add(e.getId());
                        refreshIdSet.add(e.getId());
                    } else if (MqChangeTypeEnum.Update.name().equals(e.getChangeType())) {
                        updateIdList.add(e.getId());
                        refreshIdSet.add(e.getId());
                    } else if (MqChangeTypeEnum.Delete.name().equals(e.getChangeType())) {
                        delIdList.add(e.getId());
                    }
                });

                Map<String, CommonSpottingModel> dbMap = SpottingCache.getSpottingByIds(commonSpottingService,refreshIdSet).stream().collect(Collectors.toMap(CommonSpottingModel::getSpottingid, a -> a, (k1, k2) -> k1));
                Collection<CommonSpottingModel> loadedCacheData = this.getLoadedCacheData().values();
                Map<String, CommonSpottingModel> newData = Maps.newHashMap();
                //使用copyOnWrite模式
                loadedCacheData.forEach(e -> {
                    String id = e.getSpottingid();
                    //删除
                    if (delIdList.contains(id)) {
                        return;
                    }
                    //修改
                    if (updateIdList.contains(id)) {
                        if (dbMap.get(id) != null && dbMap.get(id).getSpottingid() != null) {
                            newData.put(dbMap.get(id).getSpottingid(), dbMap.get(id));
                            //newData.add(dbMap.get(id));
                        }
                        return;
                    } else {//没变
                        // newData.add(e);
                        newData.put(e.getSpottingid(), e);
                        return;
                    }

                });
                //新增
                newIdList.forEach(id -> {
                    CommonSpottingModel model = dbMap.get(id);
                    if (model != null) {
                        //newData.add(model);
                        newData.put(model.getSpottingid(), model);
                    }
                });

                this.cacheData = newData;
            }

            return this.cacheData.size();
        }
    }
}
