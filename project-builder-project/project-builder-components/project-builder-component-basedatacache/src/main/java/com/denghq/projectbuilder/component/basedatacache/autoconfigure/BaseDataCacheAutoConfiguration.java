package com.denghq.projectbuilder.component.basedatacache.autoconfigure;

import com.denghq.projectbuilder.component.remote.service.SysDictService;
import com.denghq.projectbuilder.component.basedatacache.builder.BaseDataCacheHolder;
import com.denghq.projectbuilder.component.basedatacache.builder.ICacheDomainListContainer;
import com.denghq.projectbuilder.component.basedatacache.builder.SimpleCacheDomainListContainer;
import com.denghq.projectbuilder.component.basedatacache.cache.SomeDictCache;
import com.denghq.projectbuilder.component.msgbus.bus.impl.RabbitMqMsgBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 缓存组件自动配置类
 */
@Configuration
@ConditionalOnMissingBean(BaseDataCacheHolder.class)
@EnableConfigurationProperties(BaseDataCacheProperties.class)
public class BaseDataCacheAutoConfiguration {

    private final BaseDataCacheProperties properties;

    @Autowired(required = false)
    private List<ICustomCacheDomain> customCacheDomainList;

    @Autowired(required = false)
    private List<ICommonCacheDomain> commonCacheDomainList;

    @Autowired
    private SysDictService sysDictService;

    public BaseDataCacheAutoConfiguration(BaseDataCacheProperties properties){
        this.properties = properties;
    }

    @Bean
    @ConditionalOnMissingBean(ICacheDomainListContainer.class)
    public ICacheDomainListContainer cacheDomainListContainer(SomeDictCache dictCache){
        commonCacheDomainList.add(dictCache);
        //构建cache列表
        return new SimpleCacheDomainListContainer(properties,customCacheDomainList,commonCacheDomainList);
    }


    @Bean
    public BaseDataCacheHolder baseDataCacheHolder(ICacheDomainListContainer cacheDomainListContainer, RabbitMqMsgBus eventBus) {

        return BaseDataCacheHolder.getInstance(this.properties,cacheDomainListContainer==null?null:cacheDomainListContainer.getCacheDomainList(),eventBus);
    }


    @Bean
    public SomeDictCache dictCache(){

        return new SomeDictCache(this.properties.getDictCodes(),sysDictService);
    }
}
