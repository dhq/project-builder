package com.denghq.projectbuilder.component.basedatacache.cache;

import java.util.List;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import com.denghq.projectbuilder.component.remote.model.CommonAreaConfigModel;
import com.denghq.projectbuilder.component.remote.service.CommonAreaConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

import lombok.Data;

/**
 * 自定义区域缓存
 */
@Data
@Component
public class AreaConfigCache extends AbstractCacheDomain<List<CommonAreaConfigModel>> implements ICommonCacheDomain<List<CommonAreaConfigModel>> {

    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private List<CommonAreaConfigModel> cacheData;

    @Autowired
    private CommonAreaConfigService commonAreaConfigService;

    public AreaConfigCache() {
        this.cacheName = CacheTypeEnum.AllAreaConfig.getCacheName();
        this.cacheKey = CacheTypeEnum.AllAreaConfig.name();
        this.topicName = CacheTypeEnum.AllAreaConfig.getTopic();
        this.loadSuccess = false;
        cacheData = Lists.newArrayList();
    }

    @Override
    public int initLoad() {
        List<CommonAreaConfigModel> all = commonAreaConfigService.all();
       /* this.cacheData.clear();
        this.cacheData.addAll(all);*/
        this.cacheData = all;
        return all.size();
    }

    @Override
    public int update(String msg) {
        return initLoad();
    }
}
