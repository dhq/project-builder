package com.denghq.projectbuilder.component.basedatacache.autoconfigure;

import com.denghq.projectbuilder.component.basedatacache.cache.ICacheDomain;

/**
 * 通用缓存标识接口，缓存组件管理的缓存域模型对象
 * @param <T>
 */
public interface ICommonCacheDomain<T> extends ICacheDomain<T> {
}
