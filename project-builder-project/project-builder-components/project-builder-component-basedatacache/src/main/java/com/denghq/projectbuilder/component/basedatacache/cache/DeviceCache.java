package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.common.util.BeanTool;
import com.denghq.projectbuilder.component.amqp.MqChangeTypeEnum;
import com.denghq.projectbuilder.component.amqp.MsgDecorator;
import com.denghq.projectbuilder.component.amqp.MsgDecoratorData;
import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.remote.model.CommonFrontdeviceModel;
import com.denghq.projectbuilder.component.remote.service.CommonFrontDeviceService;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 设备缓存
 */
@Data
@Component
@Slf4j
public class DeviceCache extends AbstractCacheDomain<List<CommonFrontdeviceModel>> implements ICommonCacheDomain<List<CommonFrontdeviceModel>> {

    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private List<CommonFrontdeviceModel> cacheData;
    private static final String[] devTypes = {"09", "10"};

    @Autowired
    private CommonFrontDeviceService commonFrontDeviceService;

    public DeviceCache() {
        this.cacheName = CacheTypeEnum.AllDevice.getCacheName();
        this.cacheKey = CacheTypeEnum.AllDevice.name();
        this.topicName = CacheTypeEnum.AllDevice.getTopic();
        this.loadSuccess = false;
        cacheData = Lists.newArrayList();
    }

    @Override
    public synchronized int initLoad() {

        List<CommonFrontdeviceModel> all = getCacheDeviceList(commonFrontDeviceService);
        //this.cacheData.clear();
        //this.cacheData.addAll(all);
        this.cacheData = all;
        return all.size();
    }

    @Override
    public synchronized int update(String msg) {

        if (!this.loadSuccess) {
            return this.getLoadedCacheData().size();
        } else {
            MsgDecorator<MsgDecoratorData> msgBean = BeanTool.jsonStrToObject(msg, new TypeReference<MsgDecorator<MsgDecoratorData>>() {
            });
            //找到删除的设备，新增的设备，修改的设备。

            if (msgBean != null && !CollectionUtils.isEmpty(msgBean.getData())) {
                //1、查出修改和新增的
                List<MsgDecoratorData> data = msgBean.getData();
                List<String> delIdList = Lists.newArrayList();
                List<String> updateIdList = Lists.newArrayList();
                List<String> newIdList = Lists.newArrayList();
                Set<String> refreshIdSet = Sets.newHashSet();//需要拉取最新数据的id
                data.forEach(e -> {
                    if (MqChangeTypeEnum.Insert.name().equals(e.getChangeType())) {
                        newIdList.add(e.getId());
                        refreshIdSet.add(e.getId());
                    } else if (MqChangeTypeEnum.Update.name().equals(e.getChangeType())) {
                        updateIdList.add(e.getId());
                        refreshIdSet.add(e.getId());
                    } else if (MqChangeTypeEnum.Delete.name().equals(e.getChangeType())) {
                        delIdList.add(e.getId());
                    }
                });

                Map<String, CommonFrontdeviceModel> dbDevMap = DeviceCache.getDeviceByIds(commonFrontDeviceService, refreshIdSet).stream().collect(Collectors.toMap(CommonFrontdeviceModel::getDeviceid, a -> a, (k1, k2) -> k1));
                Collection<CommonFrontdeviceModel> loadedCacheData = this.getLoadedCacheData();
                List<CommonFrontdeviceModel> newData = Lists.newArrayList();
                //使用copyOnWrite模式
                loadedCacheData.forEach(e -> {
                    String id = e.getDeviceid();
                    //删除
                    if (delIdList.contains(id)) {
                        return;
                    }
                    //修改
                    if (updateIdList.contains(id)) {
                        if (dbDevMap.get(id) != null && dbDevMap.get(id).getDeviceno() != null) {
                            newData.add(dbDevMap.get(id));
                        }
                        return;
                    } else {//没变
                        newData.add(e);
                        return;
                    }

                });
                //新增
                newIdList.forEach(id -> {
                    CommonFrontdeviceModel model = dbDevMap.get(id);
                    if (model != null) {
                        newData.add(model);
                    }
                });

                this.cacheData = newData;
            }

            return this.cacheData.size();
        }

    }

    public static List<CommonFrontdeviceModel> getCacheDeviceList(CommonFrontDeviceService commonFrontDeviceService) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("isPagination", false);
        params.put("devicetypelist", devTypes);
        return commonFrontDeviceService.searchSimple(params);
    }

    public static List<CommonFrontdeviceModel> getDeviceByIds(CommonFrontDeviceService commonFrontDeviceService, Collection<String> deviceIdList) {
        if(CollectionUtils.isEmpty(deviceIdList)){
            log.debug("未重新加载设备记录");
            return Lists.newArrayList();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("deviceIdList", deviceIdList);
        params.put("devicetypelist", devTypes);
        List<CommonFrontdeviceModel> deviceList = commonFrontDeviceService.searchSimple(params);
        log.debug("重新加载{}条设备记录", deviceList.size());
        return deviceList;
    }

}
