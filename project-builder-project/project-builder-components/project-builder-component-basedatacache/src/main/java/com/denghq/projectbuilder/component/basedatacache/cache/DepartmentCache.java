package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import com.denghq.projectbuilder.component.remote.model.DepartmentModel;
import com.denghq.projectbuilder.component.remote.service.SmwDepartmentService;
import com.google.common.collect.Lists;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * 路口缓存
 */
@Data
@Component
public class DepartmentCache extends AbstractCacheDomain<Map<String, DepartmentModel>> implements ICommonCacheDomain<Map<String, DepartmentModel>> {
    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private ConcurrentHashMap<String, DepartmentModel> cacheData;

    @Autowired
    private SmwDepartmentService smwDepartmentService;

    public DepartmentCache() {
        this.cacheName = CacheTypeEnum.Department.getCacheName();
        this.cacheKey = CacheTypeEnum.Department.name();
        this.topicName = CacheTypeEnum.Department.getTopic();
        this.loadSuccess = false;
        cacheData = new ConcurrentHashMap<>();
    }

    @Override
    public int initLoad() {
        List<DepartmentModel> all = smwDepartmentService.all();
       /* this.cacheData.clear();
        if (!CollectionUtils.isEmpty(all)) {
            all.stream().forEach(e -> this.cacheData.put(e.getCompanyid(), e));
        }*/
        ConcurrentHashMap<String, DepartmentModel> newData = new ConcurrentHashMap<>();
        if (!CollectionUtils.isEmpty(all)) {
            all.stream().forEach(e -> newData.put(e.getDepartmentId(), e));
        }
        this.cacheData = newData;
        return all.size();
    }

    @Override
    public int update(String msg) {
        return initLoad();
    }

    public DepartmentModel findById(String departmentId) {
        return cacheData.get(departmentId);
    }

    public List<DepartmentModel> getAllDepartment() {
        return new ArrayList<>(cacheData.values());
    }

    /**
     * 获取部门以及子级部门数据,如果参数部门id为空则返回所有部门数据
     *
     * @return
     */
    public List<DepartmentModel> findChildDepartment(String departmentId) {
        if (StringUtils.isBlank(departmentId)) {
            return getAllDepartment();
        }

        List<DepartmentModel> res = Lists.newArrayList();
        List<DepartmentModel> allDepartment = getAllDepartment();
        //找到当前节点
        for (int i = 0, len = allDepartment.size(); i < len; i++) {
            DepartmentModel departmentModel = allDepartment.get(i);
            if(departmentId.equals(departmentModel.getDepartmentId())){
                res.add(departmentModel);
                break;
            }
        }
        //填充子节点
        fillChild(allDepartment, departmentId, res);
        return res;
    }

    private void fillChild(List<DepartmentModel> allDepartment, String departmentId, List<DepartmentModel> res) {
        for (int i = 0, len = allDepartment.size(); i < len; i++) {
            DepartmentModel departmentModel = allDepartment.get(i);
            if(departmentId.equals(departmentModel.getParentId())){
                res.add(departmentModel);
                fillChild(allDepartment, departmentModel.getDepartmentId(),res);
            }
        }
    }

    /**
     * 获取部门以及子级部门Id,如果参数部门id为空则返回所有部门Id
     *
     * @return
     */
    public List<String> findChildDepartmentId(String departmentId) {
        return findChildDepartment(departmentId).stream().map(DepartmentModel::getDepartmentId).collect(Collectors.toList());
    }
}
