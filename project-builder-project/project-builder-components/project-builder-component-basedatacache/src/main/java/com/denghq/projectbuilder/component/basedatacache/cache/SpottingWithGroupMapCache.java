package com.denghq.projectbuilder.component.basedatacache.cache;

import com.denghq.projectbuilder.component.basedatacache.autoconfigure.ICommonCacheDomain;
import com.denghq.projectbuilder.component.basedatacache.enums.CacheTypeEnum;
import com.denghq.projectbuilder.component.remote.service.CommonSpottingGroupService;
import com.google.common.collect.Maps;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;

/**
 * 路口组缓存
 */
@Component
@Data
public class SpottingWithGroupMapCache extends AbstractCacheDomain<Map<String, Set<String>>> implements ICommonCacheDomain<Map<String, Set<String>>> {

    private String cacheName;
    private String cacheKey;
    private Boolean loadSuccess;
    private String topicName;
    private Map<String, Set<String>> cacheData;

    @Autowired
    private CommonSpottingGroupService commonSpottingGroupService;

    public SpottingWithGroupMapCache() {
        this.cacheName = CacheTypeEnum.AllSpottingWithGroupMap.getCacheName();
        this.cacheKey = CacheTypeEnum.AllSpottingWithGroupMap.name();
        this.topicName = CacheTypeEnum.AllSpottingWithGroupMap.getTopic();
        this.loadSuccess = false;
        cacheData = Maps.newConcurrentMap();
    }

    @Override
    public int initLoad() {
        Map<String, Set<String>> all = commonSpottingGroupService.spottingWithGroupMap();
       /* this.cacheData.clear();
        this.cacheData.addAll(all);*/
        this.cacheData = all;
        return all.size();
    }

    @Override
    public int update(String msg) {
        return initLoad();
    }
}
