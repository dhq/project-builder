package com.denghq.projectbuilder.component.amqp.Configuration;

import com.denghq.projectbuilder.component.config.metadata.annotation.Category;
import com.denghq.projectbuilder.component.config.metadata.annotation.ConfigAttribute;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "com.denghq.rabbitmq")
@Category(projectName = "基础配置", categoryNo = "RabbitMQConfig", categoryName = "消息队列配置", autoRegiste = false)
@Data
public class RabbitConfigurationProperties {

    @ConfigAttribute(name = "端口")
    private Integer port;

    @ConfigAttribute(name = "主机地址")
    private String host;

    @ConfigAttribute(name = "用户名")
    private String userName;

    @ConfigAttribute(name = "密码")
    private String password;

    @ConfigAttribute(name = "交换器名称")
    private String exchangeName;

}
