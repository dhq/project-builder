package com.denghq.projectbuilder.component.amqp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:发布给MQ的消息的 指定格式包装
 *
 * @ClassName: MsgDecorator
 * @author: weiq
 * @param <T>
 * @date: 2016年7月1日 上午11:51:40
 */
@AllArgsConstructor
@NoArgsConstructor
public class MsgObjDecorator<T> implements Serializable {

	private static final long serialVersionUID = 7348291268343083251L;

	@JsonProperty(value = "DataType")
	private String dataType;

	@JsonProperty(value = "Data")
	private T data;

	@JsonProperty(value = "ReportedTime")
	@JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyyMMddHHmmss")
	private Date reportedTime = new Date();

	// get
	@JsonIgnore
	public String getDataType() {
		return dataType;
	}

	@JsonIgnore
	public T getData() {
		return data;
	}

	@JsonIgnore
	public Date getReportedTime() {
		return reportedTime;
	}

	// set
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public void setData(T data) {
		this.data = data;
	}

	public void setReportedTime(Date reportedTime) {
		this.reportedTime = reportedTime;
	}

	public static <T> Builder<T> build() {
		return new Builder<T>();
	}

	public static class Builder<T> {

		private String dataType;

		private T data;

		private Date reportedTime;

		Builder() {
			this.dataType = null;
			this.data = null;
			this.reportedTime = new Date();
		}

		public Builder<T> setDataType(final String dataType) {
			this.dataType = dataType;
			return this;
		}

		public Builder<T> setData(final T data) {
			this.data = data;
			return this;
		}

		public Builder<T> setReportedTime(final Date reportedTime) {
			this.reportedTime = reportedTime;
			return this;
		}

		public MsgObjDecorator<T> build() {

			return new MsgObjDecorator<T>(dataType, data, reportedTime);
		}

	}
}
