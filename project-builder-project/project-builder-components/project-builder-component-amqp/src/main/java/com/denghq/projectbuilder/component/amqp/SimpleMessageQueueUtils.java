package com.denghq.projectbuilder.component.amqp;

import com.denghq.projectbuilder.common.util.BeanTool;
import com.denghq.projectbuilder.component.amqp.Configuration.RabbitConfigurationProperties;
import lombok.Data;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.stereotype.Component;

@Data
@Component
public class SimpleMessageQueueUtils {

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private RabbitProperties rabbitProperties;

    @Autowired
    private RabbitConfigurationProperties rabbitMQConfig;

    public void sendMsg(String routingKey, String message) {
        template.convertAndSend(rabbitMQConfig.getExchangeName(), routingKey, message);
    }


    public void sendMsg(String routingKey, MsgDecorator<?> msg) {
        this.sendMsg(routingKey, convertMsg(msg));
    }


    public String convertMsg(MsgDecorator<?> msg) {
        String message = "";
        // 为了把空值展示出来 首字母大写
	/*	message = JSON.toJSONString(msg, new PascalNameFilter(),SerializerFeature.WriteMapNullValue,
				SerializerFeature.WriteNullStringAsEmpty);*/

        message = BeanTool.objectToJsonStr(msg);
        return message;
    }


}
