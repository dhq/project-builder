package com.denghq.projectbuilder.component.amqp;

/**
 * 	是否已删除
 * 
 * 	DeletedState 状态
 * 	@author denghq
 *
 */
public enum MqChangeTypeEnum {

	Update("修改"),Insert("添加"),Delete("删除");

	private String description;

	public String getDescription() {
		return description;
	}

	MqChangeTypeEnum(String description) {
		this.description = description;
	} 
	
}
