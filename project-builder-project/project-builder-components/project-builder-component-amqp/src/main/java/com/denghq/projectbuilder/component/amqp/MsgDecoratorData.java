package com.denghq.projectbuilder.component.amqp;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description:发布给MQ的消息的 指定格式包装
 *
 * @ClassName:  MsgDecorator 
 * @author:  weiq
 * @date:   2016年7月1日 上午11:51:40
 */
@SuppressWarnings("serial")
@AllArgsConstructor
@NoArgsConstructor
public class MsgDecoratorData implements Serializable {
	
	@JsonProperty(value = "ChangeType")
	private String changeType;

	
	@JsonProperty(value = "Id")
	private String id;
	
	//get
	@JsonIgnore
	public String getChangeType() {
		return changeType;
	}

	@JsonIgnore
	public String getId() {
		return id;
	}
	
	//set
	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}
	
	public void setId(String id) {
		this.id = id;
	}
}

