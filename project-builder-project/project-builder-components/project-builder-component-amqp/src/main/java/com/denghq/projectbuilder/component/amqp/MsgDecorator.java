package com.denghq.projectbuilder.component.amqp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description:发布给MQ的消息的 指定格式包装
 *
 * @ClassName: MsgDecorator
 * @author: weiq
 * @param <T>
 * @date: 2016年7月1日 上午11:51:40
 */
@AllArgsConstructor
@NoArgsConstructor
public class MsgDecorator<T> implements Serializable {

	private static final long serialVersionUID = 7348291268343083251L;

	@JsonProperty(value = "DataType")
	private String dataType;

	@JsonProperty(value = "Data")
	private List<T> data = new ArrayList<T>();

	@JsonProperty(value = "ReportedTime")
	@JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyyMMddHHmmss")
	private Date reportedTime = new Date();

	// get
	@JsonIgnore
	public String getDataType() {
		return dataType;
	}

	@JsonIgnore
	public List<T> getData() {
		return data;
	}

	@JsonIgnore
	public Date getReportedTime() {
		return reportedTime;
	}

	// set
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public void setReportedTime(Date reportedTime) {
		this.reportedTime = reportedTime;
	}

	public static <T> Builder<T> build() {
		return new Builder<T>();
	}

	public static class Builder<T> {

		private String dataType;

		private List<T> data;

		private Date reportedTime;

		private Builder() {
			this.dataType = null;
			this.data = Lists.newArrayList();
			this.reportedTime = new Date();
		}

		public Builder<T> setDataType(final String dataType) {
			this.dataType = dataType;
			return this;
		}

		public Builder<T> setData(final List<T> data) {
			this.data = data;
			return this;
		}

		public Builder<T> setReportedTime(final Date reportedTime) {
			this.reportedTime = reportedTime;
			return this;
		}

		public MsgDecorator<T> build() {

			return new MsgDecorator<T>(dataType, data, reportedTime);
		}

	}
}
