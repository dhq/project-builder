package com.denghq.projectbuilder.component.apm.autoconfigure;

import com.denghq.projectbuilder.component.config.metadata.annotation.Category;
import com.denghq.projectbuilder.component.config.metadata.annotation.ConfigAttribute;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "com.denghq.component.apm")
@Category(projectName = "基础配置", categoryNo = "smw/ApmServerConfig", categoryName = "应用程序性能监控配置", appNo = "smw",autoRegiste = false)
@Data
public class ApmConfigProperties {

    //监控的包名
    private String watchPackages = "com.denghq.controller";

    private String logLevel = "ERROR";

    //是否开启APM
    @ConfigAttribute(name="是否开启APM")
    private Boolean enableApm;

    //APM Server地址
    @ConfigAttribute(name="APM Server地址")
    private String apmServer;



}
