package com.denghq.projectbuilder.component.apm.autoconfigure;

import com.denghq.projectbuilder.component.apm.startup.ApmStarter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@ConditionalOnMissingBean(ApmStarter.class)
@EnableConfigurationProperties(ApmConfigProperties.class)
@Slf4j
public class ApmAutoConfiguration {

    @Value("${spring.application.name}")
    private String appNo;

    @Autowired
    private ApmConfigProperties apmConfigProperties;

    @PostConstruct
    public void init() {
        try {
            new ApmStarter(apmConfigProperties, appNo).run(null);
        } catch (Exception e) {
            log.warn("初始apm出错，msg:{}", e.getMessage());
        }
    }
}
