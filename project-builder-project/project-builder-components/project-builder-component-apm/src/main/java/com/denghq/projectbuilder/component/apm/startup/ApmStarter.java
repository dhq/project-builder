package com.denghq.projectbuilder.component.apm.startup;

import co.elastic.apm.attach.ElasticApmAttacher;
import com.denghq.projectbuilder.component.apm.autoconfigure.ApmConfigProperties;
import com.google.common.collect.Maps;

import java.util.Map;

public class ApmStarter {

    private final ApmConfigProperties apmConfigProperties;
    private final String serviceName;

    public ApmStarter(ApmConfigProperties apmConfigProperties, String serviceName) {
        this.apmConfigProperties = apmConfigProperties;
        this.serviceName = serviceName;
    }

    public void run(String... args) throws Exception {

        if (apmConfigProperties.getEnableApm() != null && apmConfigProperties.getEnableApm()) {
            Map<String, String> conf = Maps.newHashMap();
            conf.put("service_name", serviceName);
            conf.put("application_packages", apmConfigProperties.getWatchPackages());
            conf.put("server_url", apmConfigProperties.getApmServer());
            conf.put("log_level", apmConfigProperties.getLogLevel());
            ElasticApmAttacher.attach(conf);
        }

    }
}
