package com.denghq.projectbuilder.component.consul.config.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.style.ToStringCreator;
import org.springframework.validation.annotation.Validated;

@ConfigurationProperties("com.denghq.component.consul.config")
@Validated
public class ConsulConfigProperties {
    private boolean enabled = true;
    @Value("${consul.token:${CONSUL_TOKEN:${spring.cloud.consul.token:${SPRING_CLOUD_CONSUL_TOKEN:}}}}")
    private String aclToken;

    private ConsulConfigProperties.Watch watch;

    private boolean failFast;

    @Setter
    @Getter
    private String basePackage;


    public ConsulConfigProperties() {

        this.watch = new ConsulConfigProperties.Watch();
        this.failFast = true;
    }

    public boolean isEnabled() {
        return this.enabled;
    }


    public String getAclToken() {
        return this.aclToken;
    }

    public ConsulConfigProperties.Watch getWatch() {
        return this.watch;
    }

    public boolean isFailFast() {
        return this.failFast;
    }


    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }


    public void setAclToken(String aclToken) {
        this.aclToken = aclToken;
    }

    public void setWatch(ConsulConfigProperties.Watch watch) {
        this.watch = watch;
    }

    public void setFailFast(boolean failFast) {
        this.failFast = failFast;
    }


    public String toString() {
        return (new ToStringCreator(this))
                .append("enabled", this.enabled)
                .append("aclToken", this.aclToken)
                .append("watch", this.watch)
                .append("failFast", this.failFast)
                .toString();
    }


    public static class Watch {
        private int waitTime = 55;
        private boolean enabled = true;
        private int delay = 1000;

        public Watch() {
        }

        public int getWaitTime() {
            return this.waitTime;
        }

        public boolean isEnabled() {
            return this.enabled;
        }

        public int getDelay() {
            return this.delay;
        }

        public void setWaitTime(int waitTime) {
            this.waitTime = waitTime;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public void setDelay(int delay) {
            this.delay = delay;
        }

        public String toString() {
            return (new ToStringCreator(this)).append("waitTime", this.waitTime).append("enabled", this.enabled).append("delay", this.delay).toString();
        }
    }
}
