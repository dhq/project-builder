package com.denghq.projectbuilder.component.consul.config.configuration;


import com.denghq.projectbuilder.component.config.domain.ConfigClassParser;
import com.denghq.projectbuilder.component.config.metadata.ConfigDescriptor;

public class ConsulConfigManager {

    private final ConfigClassParser configClassParser;
    private final ConsulConfigAutoRegister consulConfigAutoRegister;

    public ConsulConfigManager(ConfigClassParser consulConfigClassParser, ConsulConfigAutoRegister consulConfigAutoRegister) {
        this.configClassParser = consulConfigClassParser;
        this.consulConfigAutoRegister = consulConfigAutoRegister;
    }

    /**
     * 直接把配置实例写入配置中心
     *
     * @return 是否成功写入
     */
    public Boolean writeConfigDirect(Object configInst) {
        //获取配置类描述（元信息）
        ConfigDescriptor configDescriptor = configClassParser.getConfigDescriptor(configInst);
        //写入consul
        return consulConfigAutoRegister.writeConfigToConsul(configDescriptor);

    }

}
