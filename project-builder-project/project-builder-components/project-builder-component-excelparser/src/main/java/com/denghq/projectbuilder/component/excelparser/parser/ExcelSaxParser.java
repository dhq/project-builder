package com.denghq.projectbuilder.component.excelparser.parser;


import com.denghq.projectbuilder.component.excelparser.handler.IExcelParseHandler;
import com.denghq.projectbuilder.component.excelparser.handler.impl.Excel2003ParseHandler;
import com.denghq.projectbuilder.component.excelparser.handler.impl.Excel2007ParseHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.poifs.filesystem.DocumentFactoryHelper;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.util.IOUtils;

import java.io.InputStream;

@Slf4j
public class ExcelSaxParser<T> extends AbstractExcelParser<T> {

    public IExcelParseHandler<T> createHandler(InputStream excelInputStream) {
        try {
            byte[] header8 = IOUtils.peekFirst8Bytes(excelInputStream);
            if (NPOIFSFileSystem.hasPOIFSHeader(header8)) {
                return new Excel2003ParseHandler<>();
            } else if (DocumentFactoryHelper.hasOOXMLHeader(excelInputStream)) {
                return new Excel2007ParseHandler<>();
            } else {
                throw new IllegalArgumentException("Your InputStream was neither an OLE2 stream, nor an OOXML stream");
            }
        } catch (Exception e) {
            log.error("getParserInstance Error!", e);
            throw new RuntimeException(e);
        }
    }

}
