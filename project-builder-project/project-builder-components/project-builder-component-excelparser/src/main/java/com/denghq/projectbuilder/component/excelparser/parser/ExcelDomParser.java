package com.denghq.projectbuilder.component.excelparser.parser;

import com.denghq.projectbuilder.component.excelparser.handler.IExcelParseHandler;
import com.denghq.projectbuilder.component.excelparser.handler.impl.ExcelDomParseHandler;

import java.io.InputStream;

public class ExcelDomParser<T> extends AbstractExcelParser<T> {

    private IExcelParseHandler<T> excelParseHandler;

    public ExcelDomParser() {
        this.excelParseHandler = new ExcelDomParseHandler<>();
    }

    @Override
    protected IExcelParseHandler<T> createHandler(InputStream excelInputStream) {
        return this.excelParseHandler;
    }
}
