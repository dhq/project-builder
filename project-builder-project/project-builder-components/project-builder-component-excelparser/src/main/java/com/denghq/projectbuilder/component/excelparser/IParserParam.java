package com.denghq.projectbuilder.component.excelparser;

import java.io.InputStream;
import java.util.List;

public interface IParserParam {

    Integer FIRST_SHEET = 0;
    Integer FIRST_ROW = 0;

    Integer getFirstRow(); 
    
    InputStream getExcelInputStream();

    Class getTargetClass();

    Integer getColumnSize();

    Integer getSheetNum();

    List<String> getHeader();
}
