package com.denghq.projectbuilder.component.excelparser.bean;

public class Result<T> {

    private long rowNo;

    private T data;

    public long getRowNo() {
        return rowNo;
    }

    public void setRowNo(long rowNo) {
        this.rowNo = rowNo;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static Builder build() {
        return new Builder();
    }

    public static class Builder<T> {
        private long rowNo;

        private T data;

        public Builder<T> rowNo(long rowNo) {
            this.rowNo = rowNo;
            return this;
        }

        public Builder<T> data(T data) {
            this.data = data;
            return this;
        }

        public Result<T> build() {
            Result<T> tResult = new Result<>();
            tResult.setRowNo(this.rowNo);
            tResult.setData(this.data);
            return tResult;
        }
    }
}
