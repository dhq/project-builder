package com.denghq.projectbuilder.component.excelparser.parser;


import com.denghq.projectbuilder.component.excelparser.IExcelParser;
import com.denghq.projectbuilder.component.excelparser.IParserParam;
import com.denghq.projectbuilder.component.excelparser.bean.Result;
import com.denghq.projectbuilder.component.excelparser.handler.IExcelParseHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

abstract class AbstractExcelParser<T> implements IExcelParser<T> {


    public List<Result<T>> parse(IParserParam parserParam) {
        checkParserParam(parserParam);
        IExcelParseHandler<T> handler = this.createHandler(parserParam.getExcelInputStream());
        try {
            return handler.process(parserParam);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (parserParam.getExcelInputStream() != null) {
                    parserParam.getExcelInputStream().close();
                }
            } catch (IOException e) {
                // doNothing
            }
        }
    }

    protected abstract IExcelParseHandler<T> createHandler(InputStream excelInputStream);

    private void checkParserParam(IParserParam parserParam) {
        if (parserParam == null || parserParam.getExcelInputStream() == null
                || parserParam.getColumnSize() == null || parserParam.getTargetClass() == null
                || parserParam.getSheetNum() == null) {
            throw new IllegalArgumentException(String.format("ParserParam has null value,ParserParam value is %s",
                    Objects.toString(parserParam)));
        }
    }
}
