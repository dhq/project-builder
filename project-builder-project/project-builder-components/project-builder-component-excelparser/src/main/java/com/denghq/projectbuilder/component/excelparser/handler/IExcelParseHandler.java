package com.denghq.projectbuilder.component.excelparser.handler;

import com.denghq.projectbuilder.component.excelparser.IParserParam;
import com.denghq.projectbuilder.component.excelparser.bean.Result;

import java.util.List;

public interface IExcelParseHandler<T> {

    List<Result<T>> process(IParserParam parserParam) throws Exception;

}
