package com.denghq.projectbuilder.component.excelparser;

import com.denghq.projectbuilder.component.excelparser.bean.Result;

import java.util.List;

public interface IExcelParser<T> {
    List<Result<T>> parse(IParserParam parserParam);
}
