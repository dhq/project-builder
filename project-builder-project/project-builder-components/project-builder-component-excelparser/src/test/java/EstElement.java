import com.denghq.projectbuilder.component.excelparser.annotation.ExcelField;

import java.util.ArrayList;
import java.util.List;

public class EstElement {

    @ExcelField(index = 0)
    private String type;

    @ExcelField(index = 1)
    private String name;

    @ExcelField(index = 2)
    private String image;

    public EstElement() {

    }

    public EstElement(String type, String name, String image) {
        super();
        this.type = type;
        this.name = name;
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "EstElementPojo [type=" + type + ", name=" + name + ", image=" + image + "]";
    }

    public static List<String> getHeader() {
        List<String> header = new ArrayList<>();
        header.add("元素类型");
        header.add("元素名称");
        header.add("元素图片");
        return header;
    }

}
