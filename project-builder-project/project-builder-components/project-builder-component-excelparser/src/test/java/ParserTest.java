import com.denghq.projectbuilder.component.excelparser.IExcelParser;
import com.denghq.projectbuilder.component.excelparser.IParserParam;
import com.denghq.projectbuilder.component.excelparser.bean.Result;
import com.denghq.projectbuilder.component.excelparser.param.DefaultParserParam;
import com.denghq.projectbuilder.component.excelparser.parser.ExcelDomParser;
import com.denghq.projectbuilder.component.excelparser.parser.ExcelSaxParser;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.fail;

public class ParserTest {

    private IExcelParser<User> parser;

    @Test
    public void testDomXlsx() {

        parser = new ExcelDomParser<>();

        IParserParam parserParam = DefaultParserParam
                .builder()
                .excelInputStream(Thread.currentThread() //要解析的文件流
                        .getContextClassLoader()
                        .getResourceAsStream("test01.xlsx"))
                .columnSize(4)//解析的列数
                .sheetNum(IParserParam.FIRST_SHEET) // 解析的工作薄索引
                .targetClass(User.class) // 解析出的对象
                .header(User.getHeader())// 校验标题，不传表示不校验标题
                .firstRow(0) // 解析开始行
                .build();

        List<Result<User>> user = parser.parse(parserParam);
        System.out.println(user);
    }

    @Test
    public void testDomXls() {

        parser = new ExcelDomParser<>();

        IParserParam parserParam = DefaultParserParam.builder()
                .excelInputStream(Thread.currentThread().getContextClassLoader().getResourceAsStream("test01.xls"))
                .columnSize(4)
                .sheetNum(IParserParam.FIRST_SHEET)
                .targetClass(User.class)
                .header(User.getHeader())
                .firstRow(IParserParam.FIRST_ROW)
                .build();

        List<Result<User>> user = parser.parse(parserParam);
        System.out.println(user);
    }

    @Test
    public void testSaxXlsx() {
        parser = new ExcelSaxParser<>();

        IParserParam parserParam = DefaultParserParam.builder()
                .excelInputStream(Thread.currentThread().getContextClassLoader().getResourceAsStream("test01.xlsx"))
                .columnSize(4)
                .sheetNum(IParserParam.FIRST_SHEET)
                .targetClass(User.class)
                .header(User.getHeader())
                .firstRow(IParserParam.FIRST_ROW)
                .build();

        List<Result<User>> user = parser.parse(parserParam);
        System.out.println(user);
    }


    @Test
    public void testSaxXls() {
        parser = new ExcelSaxParser<>();

        IParserParam parserParam = DefaultParserParam.builder()
                .excelInputStream(Thread.currentThread().getContextClassLoader().getResourceAsStream("test01.xls"))
                .columnSize(4)
                .sheetNum(IParserParam.FIRST_SHEET)
                .targetClass(User.class)
                .header(User.getHeader())
                .firstRow(IParserParam.FIRST_ROW)
                .build();

        List<Result<User>> user = parser.parse(parserParam);
        System.out.println(user);
    }

    @Test
    public void testSheet02Xls() {
        parser = new ExcelSaxParser<>();

        IParserParam parserParam = DefaultParserParam.builder()
                .excelInputStream(Thread.currentThread().getContextClassLoader().getResourceAsStream("test02.xls"))
                .columnSize(4)
                .sheetNum(IParserParam.FIRST_SHEET + 1)
                .targetClass(User.class)
                .header(User.getHeader())
                .firstRow(IParserParam.FIRST_ROW)
                .build();

        List<Result<User>> user = parser.parse(parserParam);
        System.out.println(user);
    }

    @Test
    public void testSheet02Xlsx() {
        parser = new ExcelSaxParser<>();

        IParserParam parserParam = DefaultParserParam.builder()
                .excelInputStream(Thread.currentThread()
                        .getContextClassLoader().getResourceAsStream("test02.xlsx"))
                .columnSize(4)
                .sheetNum(IParserParam.FIRST_SHEET + 1)
                .targetClass(User.class)
                .header(User.getHeader())
                .firstRow(0)
                .build();

        List<Result<User>> obj = parser.parse(parserParam);
        obj.forEach(o -> {
            System.out.println(o.getRowNo());
            System.out.println(o.getData());
        });
    }

    @Test
    public void testCheckHeaderErrorXlsx() {
        parser = new ExcelSaxParser<>();

        IParserParam parserParam = DefaultParserParam.builder()
                .excelInputStream(Thread.currentThread().getContextClassLoader().getResourceAsStream("test02.xlsx"))
                .columnSize(4)
                .sheetNum(IParserParam.FIRST_SHEET)
                .targetClass(User.class)
                .header(User.getErrHeader())
                .build();
        try {
            List<Result<User>> obj = parser.parse(parserParam);
            obj.forEach(o -> {
                System.out.println(o.getRowNo());
                System.out.println(o.getData());
            });
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    @Test
    public void testCheckHeaderErrorXls() {
        parser = new ExcelSaxParser<>();

        IParserParam parserParam = DefaultParserParam.builder()
                .excelInputStream(Thread.currentThread().getContextClassLoader().getResourceAsStream("test02.xls"))
                .columnSize(4)
                .sheetNum(IParserParam.FIRST_SHEET + 1)
                .targetClass(User.class)
                .header(User.getErrHeader())
                .build();
        try {
            List<Result<User>> user = parser.parse(parserParam);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        fail();
    }


    @Test
    public void testDomXls1() {

        ExcelDomParser<EstElement> parser1 = new ExcelDomParser<>();

        IParserParam parserParam = DefaultParserParam.builder()
                .excelInputStream(Thread.currentThread().getContextClassLoader().getResourceAsStream("电子沙盘元素批量导入.xls"))
                .columnSize(3)
                .sheetNum(IParserParam.FIRST_SHEET)
                .targetClass(EstElement.class)
                .header(EstElement.getHeader())
                .firstRow(5)
                .build();

        List<Result<EstElement>> obj = parser1.parse(parserParam);
        obj.forEach(o -> {
            System.out.println(o.getRowNo());
            System.out.println(o.getData());
        });
    }

    @Test
    public void testExcelSaxParserXls1() {

        ExcelSaxParser<EstElement> parser1 = new ExcelSaxParser<EstElement>();

        IParserParam parserParam = DefaultParserParam.builder()
                .excelInputStream(Thread.currentThread().getContextClassLoader().getResourceAsStream("电子沙盘元素批量导入.xls"))
                .columnSize(3)
                .sheetNum(IParserParam.FIRST_SHEET)
                .targetClass(EstElement.class)
                .header(EstElement.getHeader())
                .firstRow(5)
                .build();

        List<Result<EstElement>> obj = parser1.parse(parserParam);
        obj.forEach(o -> {
            System.out.println(o.getRowNo());
            System.out.println(o.getData());
        });
        System.out.println(obj);
    }

}
