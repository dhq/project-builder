package com.denghq.projectbuilder.component.aif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *@program: key-areas-security-control
 *@description: 注册Path
 *@author: Liu yang
 *@create: 2018-10-17 10:15
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterPath {

    /**
     * 表示服务的类型，对于WebApi服务，类型固定"http"
     */
    private String scheme;
    /**
     * 服务的路径
     */
    private String path;
    /**
     * 服务的版本号。
     */
    private String version;
    /**
     * 服务的特征码，一个用于唯一标示服务的字符串，
     * 防止不同的服务误用了相同的服务路径，
     * 对于path和version相同的服务，特征码必须相同。
     */
    private String feature;
    /**
     * 是一个字符串数组,方便根据标签查找WebAPI
     */
    private String tags;
    /**
     * HTTP请求方式，通常为POST
     */
    private String method;
    /**
     * 本WebAPI的详细描述
     */
    private String summary;

}
