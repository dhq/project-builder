package com.denghq.projectbuilder.component.aif.model;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.Data;

@Data
//字典表
public class DictModel{
	
	private String dictionaryId; //字典Id
	private Integer kind; //字典类型
	private String dictionaryNo; //字典编号
	private String dictionaryValue; //字典值
	private String parentId;//父级id
	private Integer sortNo;//当前层级排序值
	private String showOrder;//显示顺序 用于树形和列表排序用
	private String hierarchyCode;//hierarchyCode
	private String remark;//备注
	private Boolean isSystem; //boolean是否系统级数据 默认为 0否
	private String bopomofo;//拼音
	
	public static ResultInfoModel<List<DictModel>> convert(String json){
		Type jsonType = new TypeToken<ResultInfoModel<List<DictModel>>>() {}.getType();
		ResultInfoModel<List<DictModel>> bean = new Gson().fromJson(json, jsonType);
		return bean;
	} 
	
	public static ResultInfoModel<Map<String,List<DictModel>>> convertMap(String json){
		Type jsonType = new TypeToken<ResultInfoModel<Map<String,List<DictModel>>>>() {}.getType();
		ResultInfoModel<Map<String,List<DictModel>>> bean = new Gson().fromJson(json, jsonType);
		return bean;
	} 
}
