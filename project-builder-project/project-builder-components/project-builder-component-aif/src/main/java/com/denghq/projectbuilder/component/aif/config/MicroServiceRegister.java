package com.denghq.projectbuilder.component.aif.config;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.CommandLineRunner;

import com.denghq.projectbuilder.component.aif.RegisterConfig;
import com.denghq.projectbuilder.component.aif.utils.RegisterUtils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: key-areas-security-control
 * @description: api接口注册
 * @author: Liu yang
 * @create: 2018-10-17 10:04
 **/
@Slf4j
@Data
public class MicroServiceRegister implements CommandLineRunner {
    
	private RegisterUtils registerUtils;

    private RegisterConfig registerConfig;

    @Override
    public void run(String... args) throws Exception {
        try{
        	log.info(">>>>>>>>>>>>>>服务注册开始启动>>>>>>>>>>>>>>>>>>>");
        	registerUtils.register();
            log.info(">>>>>>>>>>>>>>服务注册成功>>>>>>>>>>>>>>>>>>>");
            if(registerConfig.getAutoRegiste()){
                new Thread(new AutoRegisteThread()).start();
            }

        }
        catch (Exception e){
            log.error(">>>>>>>>>>>>>>打包时此错误可无视>>>>>>>>>>>>>>>>>>>");
            log.error(">>>>>>>>>>>>>>服务注册错误>>>>>>>>>>>>>>>>>>>"+e.getMessage());
        }

    }

    /**
     * 过期处理线程
     */
    class AutoRegisteThread implements Runnable {
        public void run() {
            while (true) {
                try {
                    //System.out.println("【基础数据服务】Auto Register run ...");
                    log.debug("【基础数据服务】Auto Register run ...");
                    TimeUnit.SECONDS.sleep(registerConfig.getAutoRegisteTimeinterval());
                    registerUtils.register();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
