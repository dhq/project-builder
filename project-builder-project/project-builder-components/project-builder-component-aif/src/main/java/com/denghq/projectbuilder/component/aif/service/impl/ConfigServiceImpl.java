package com.denghq.projectbuilder.component.aif.service.impl;

import com.denghq.projectbuilder.component.aif.ApiConfig;
import com.denghq.projectbuilder.component.aif.Scheme;
import com.denghq.projectbuilder.component.aif.SysConfConfig;
import com.denghq.projectbuilder.component.aif.model.ExceptionModel;
import com.denghq.projectbuilder.component.aif.model.MapModel;
import com.denghq.projectbuilder.component.aif.model.RabbitMQConfigModel;
import com.denghq.projectbuilder.component.aif.model.ResultInfoModel;
import com.denghq.projectbuilder.component.aif.service.ConfigService;
import com.denghq.projectbuilder.component.aif.service.MicroServiceCaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service("configService")
public class ConfigServiceImpl implements ConfigService {

    @Autowired
    MicroServiceCaller microServiceCaller;

    @Autowired
    private ApiConfig config;

    @Autowired
    private SysConfConfig sysConfConfig;

    @Override
    public Map<String, Object> getConfigByCategoryNo(String categoryNo) {

        //String authorization = UserContextHolder.getAuthorization();
        Map<String,String> heads = new HashMap<String,String>();
       // heads.put("Authorization", authorization);
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("categoryNo", categoryNo);
        String res = microServiceCaller.callApi(config.getGetConfigByCategoryNo(), Scheme.HTTP, config.getGetConfigByCategoryNoMethod(),params, heads, String.class);
        ResultInfoModel<Map<String, Object> > infoModel = MapModel.convert(res);
        if(infoModel != null && infoModel.getCode() == 0){
            return infoModel.getResult();
        } else{
            throw ExceptionModel.convert(res);
        }
    }

    @Override
    public RabbitMQConfigModel getRabbitMQConfig() {
        //String authorization = UserContextHolder.getAuthorization();
        Map<String,String> heads = new HashMap<String,String>();
        // heads.put("Authorization", authorization);
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("categoryNo", sysConfConfig.getRabbitMQConfigcategoryNo());
        String res = microServiceCaller.callApi(config.getGetConfigByCategoryNo(), Scheme.HTTP, config.getGetConfigByCategoryNoMethod(),params, heads, String.class);
        ResultInfoModel<RabbitMQConfigModel> infoModel = RabbitMQConfigModel.convert(res);
        if(infoModel != null && infoModel.getCode() == 0){
            return infoModel.getResult();
        } else{
            throw ExceptionModel.convert(res);
        }
    }
}
