package com.denghq.projectbuilder.component.aif;

import lombok.Data;

/**
 * @program: key-areas-security-control
 * @description: ${description}
 * @author: Liu yang
 * @create: 2018-10-23 15:44
 **/
@Data
public class GetNode {

    private String scheme;

    private String path;

}
