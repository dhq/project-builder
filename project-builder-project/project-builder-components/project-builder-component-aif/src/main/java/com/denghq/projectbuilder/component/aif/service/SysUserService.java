package com.denghq.projectbuilder.component.aif.service;

import java.util.List;
import java.util.Set;

import com.denghq.projectbuilder.component.aif.model.DepartmentModel;
import com.denghq.projectbuilder.component.aif.model.UserModel;
import com.denghq.projectbuilder.component.aif.model.UserRightViewModel;
import com.denghq.projectbuilder.component.aif.pojo.PermissonDepartmentInfo;

/**
 * 系统日志
 * 
 * @author denghq
 * 
 * @date 2018/10/29
 */
public interface SysUserService{

	public String getUserId();

	UserRightViewModel getUserInfo();


	List<DepartmentModel> getDepartment();

	/**
	 * 批量获取部门信息
	 * @param departmentIds
	 * @return
	 */
	List<DepartmentModel> getSomeDepartment(List<String> departmentIds);

	List<DepartmentModel> getSomeDepartment(Set<String> departmentIds);

	/**
	 * 获取用户可以访问的部门权限信息
	 * @return
	 */
	PermissonDepartmentInfo getPermissonDepartmentInfo(String functionCode,String actionCode);

	/**
	 * 获取用户查询路口时可以访问的部门权限信息
	 * @return
	 */
	PermissonDepartmentInfo getSpottingSearchPermissonDepartmentInfo();

	/**
	 * 获取部门以及子级部门Id,如果参数部门id为空则返回所有部门Id
	 * @param departmentid
	 * @return
	 */
    List<String> queryChildDepartmentId(String departmentid);

	/**
	 * 获取用户查询设备时可以访问的部门权限信息
	 * @return
	 */
	PermissonDepartmentInfo getFrontdeviceSearchPermissonDepartmentInfo();

	/**
	 * 通过用户编码获取用户信息
	 * @param userCode
	 * @return
	 */
    UserModel getSomeAccountByCode(String userCode);
}
