package com.denghq.projectbuilder.component.aif;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@PropertySource("classpath:cloud.properties")
@ConfigurationProperties(prefix="denghq.cloud")
public class ApiConfig {

	private String getUserInfo;

	private String getUserInfoMethod;

	private String getDictByKind;
	
	private String getDictByKindMethod;
	
	public String getSomeDicts;

	public String getSomeDictsMethod;
	
	private String getDepartment;
	
	private String getDepartmentMethod;

	private String getSameDepartment;

	private String getSameDepartmentMethod;
	
	private String attachmentUpdateSourceId;
	
	private String attachmentUpdateSourceIdMethod;
	
	private String attachmentUpdateOrRemove;
	
	private String attachmentUpdateOrRemoveMethod;
	
	private String attachmentBatchUpdateOrRemove;
	
	private String attachmentBatchUpdateOrRemoveMethod;

    public String attachmentBatchDeleteBySourceId;

    public String attachmentBatchDeleteBySourceIdMethod;

	public String attachmentFindBySourceId;

	public String attachmentFindBySourceIdMethod;

	public String attachmentFindBySourceIds;

	public String attachmentFindBySourceIdsMethod;

	public String getConfigByCategoryNo;

	public String getConfigByCategoryNoMethod;

    public String getPermissonDepartmentByFACode;

	public String getPermissonDepartmentByFACodeMethod;

    public String queryChildDepartmentId;

	public String queryChildDepartmentIdMethod;

    public String getSomeAccountByCode;

	public String getSomeAccountByCodeMethod;
}
