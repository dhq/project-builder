package com.denghq.projectbuilder.component.aif.config;

import javax.annotation.PreDestroy;

import com.denghq.projectbuilder.component.aif.Register;
import com.denghq.projectbuilder.component.aif.utils.JsonUtil;
import com.denghq.projectbuilder.component.aif.utils.RegisterUtils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 容器销毁的时候去注册中心注销服务
 * @author Administrator
 */
@Slf4j
@Data
public class MicroServiceDestroyer {

	
	RegisterUtils registerUtil;

	@PreDestroy
    public void destory() {
        try {
            log.info(">>>>>>>>>>>>>>服务销毁了 去注册中心注销服务>>>>>>>>>>>>>>>>>>>");
            Register registerVo = registerUtil.getRegisterServiceVo();
            log.info("unregisterUrl-->" + registerUtil.getUnRegisterServiceUrl());
            String requestBody = JsonUtil.beanToString(registerVo);
            log.info("服务销毁了 去注册中心取消注册requestBody" + requestBody);
            String resbody = RegisterUtils.unRegister(registerUtil.getUnRegisterServiceUrl(), requestBody);
            //log.info(">>>>>>>>>>>>>>服务销毁了 去中心取消注册 成功>>>>>>>>>>>>>>>>>>>");
            log.info(">>>>>>>>>>>>>>服务销毁了 去注册中心注销服务 成功>>>>>>>>>>>>>>>>>>>resbody:" + resbody);
        } catch (Exception e) {
            log.error(">>>>>>>>>>>>>>服务销毁了 去注册中心注销服务 错误>>>>>>>>>>>>>>>>>>>" + e.getMessage());
        }
    }
}
