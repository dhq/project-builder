package com.denghq.projectbuilder.component.aif.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.Data;

import java.lang.reflect.Type;

@Data
public class RabbitMQConfigModel {

    private Integer port;
    private String host;
    private String userName;
    private String password;
    private String exchangeName;
    private Boolean durable;

    public static ResultInfoModel<RabbitMQConfigModel> convert(String json){
        Type jsonType = new TypeToken<ResultInfoModel<RabbitMQConfigModel>>() {}.getType();
        ResultInfoModel<RabbitMQConfigModel> bean = new Gson().fromJson(json, jsonType);
        return bean;
    }
}
