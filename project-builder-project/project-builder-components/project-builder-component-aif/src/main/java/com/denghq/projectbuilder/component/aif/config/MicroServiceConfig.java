package com.denghq.projectbuilder.component.aif.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.denghq.projectbuilder.component.aif.RegisterConfig;
import com.denghq.projectbuilder.component.aif.utils.RegisterUtils;

@Configuration
public class MicroServiceConfig {

    @Autowired
    RegisterUtils registerUtil;

    @Autowired
    RegisterConfig registerConfig;

    @Bean
    @ConditionalOnProperty(name = "denghq.cloud.open", havingValue = "true")
    public MicroServiceRegister microServiceRegister(){
        MicroServiceRegister serviceRegister = new MicroServiceRegister();
        serviceRegister.setRegisterUtils(registerUtil);
        serviceRegister.setRegisterConfig(registerConfig);
        return serviceRegister;
    }

    @Bean
    @ConditionalOnProperty(name = "denghq.cloud.open", havingValue = "true")
    public MicroServiceDestroyer microServiceDestroyer(){
        MicroServiceDestroyer serviceDestroyer = new MicroServiceDestroyer();
        serviceDestroyer.setRegisterUtil(registerUtil);
        return serviceDestroyer;
    }
}
