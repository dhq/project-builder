package com.denghq.projectbuilder.component.aif;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@PropertySource("classpath:cloud.properties")
@ConfigurationProperties(prefix="denghq.cloud.dictkind")
public class SysDictKindConfig {
	
	private String companyType;

	private String spottingGroupType;

	private String deviceGroupType;
	
	private String deviceFunctionType;

	private String deviceYWStatus;
	
	private String deviceManufacturer;

	private String roadType;
	
	private String drivingSkill;
	
	private String education;
	
	private String staffRank;
	
	private String staffType;
	
	private String gender;

	private String projectSection;

	private String projectPeriod;

	private String areaCode;

}
