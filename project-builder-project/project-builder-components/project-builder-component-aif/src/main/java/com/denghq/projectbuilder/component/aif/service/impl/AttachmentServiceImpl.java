package com.denghq.projectbuilder.component.aif.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.denghq.projectbuilder.component.aif.service.AttachmentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.denghq.projectbuilder.component.aif.ApiConfig;
import com.denghq.projectbuilder.component.aif.Scheme;
import com.denghq.projectbuilder.component.aif.model.AttachmentModel;
import com.denghq.projectbuilder.component.aif.model.ExceptionModel;
import com.denghq.projectbuilder.component.aif.model.ResultInfoModel;
import com.denghq.projectbuilder.component.aif.service.MicroServiceCaller;
import com.denghq.projectbuilder.component.aif.utils.UserContextHolder;
import com.google.common.collect.Maps;

@Service
public class AttachmentServiceImpl implements AttachmentService {
	
	@Autowired
	MicroServiceCaller microServiceCaller;
	
	@Autowired
	private ApiConfig config;
	
	@Override
	public Boolean updateSourceId(List<String> attachmentIds, String attachmentType, String sourceId) {
		String authorization = UserContextHolder.getAuthorization();
		if(CollectionUtils.isEmpty(attachmentIds)) {
			return false;
		}
		Map<String,String> heads = new HashMap<String,String>();
		heads.put("Authorization", authorization);
		Map<String,Object> queryParams = new HashMap<String,Object>();
		queryParams.put("sourceId", sourceId);
		queryParams.put("sourceType", attachmentType);
		String res = microServiceCaller.callApi(config.getAttachmentUpdateSourceId(), Scheme.HTTP, config.getAttachmentUpdateSourceIdMethod(), queryParams , attachmentIds, heads,  String.class);
		ResultInfoModel<Object> infoModel = AttachmentModel.convertObj(res);
		if(infoModel != null && infoModel.getCode() == 0){
			return true;
		} else{
			throw ExceptionModel.convert(res);
		}
	}

	@Override
	public Boolean updateOrRemove(List<String> attachmentIds, String attachmentType, String sourceId) {

		String authorization = UserContextHolder.getAuthorization();
		Map<String,String> heads = new HashMap<String,String>();
		heads.put("Authorization", authorization);
		Map<String,Object> params = new HashMap<String,Object>();
		params.put(sourceId, attachmentIds);
		String res = microServiceCaller.callApi(config.getAttachmentBatchUpdateOrRemove(), Scheme.HTTP, config.getAttachmentBatchUpdateOrRemoveMethod(),params, heads, String.class);
		ResultInfoModel<Object> infoModel = AttachmentModel.convertObj(res);
		if(infoModel != null && infoModel.getCode() == 0){
			return true;
		} else{
			throw ExceptionModel.convert(res);
		}

	}

	@Override
	public Boolean updateSourceId(String attachmentId, String attachmentType, String sourceId) {
		if(StringUtils.isNotBlank(attachmentId)) {
			List<String> attachmentIds = new ArrayList<String>();
			attachmentIds.add(attachmentId);
			return this.updateSourceId(attachmentIds , attachmentType, sourceId);
		}
		return false;
	}

	@Override
	public Boolean batchUpdateOrRemove(Map<String, String> attachmentMap) {
		String authorization = UserContextHolder.getAuthorization();
		if(attachmentMap==null||attachmentMap.isEmpty()) {
			return false;
		}
		
		Map<String,String> heads = new HashMap<String,String>();
		heads.put("Authorization", authorization);
		
		Map<String,Object> params = new HashMap<String,Object>();
		attachmentMap.forEach((sourceId,attachmentId)->{
			List<String> attachmentIds = new ArrayList<String>();
			if(StringUtils.isNotBlank(attachmentId)) {
				attachmentIds.add(attachmentId);
			}
			params.put(sourceId, attachmentIds);
		});
		
		String res = microServiceCaller.callApi(config.getAttachmentBatchUpdateOrRemove(), Scheme.HTTP, config.getAttachmentBatchUpdateOrRemoveMethod(),params, heads, String.class);
		ResultInfoModel<Object> infoModel = AttachmentModel.convertObj(res);
		if(infoModel != null && infoModel.getCode() == 0){
			return true;
		} else{
			throw ExceptionModel.convert(res);
		}
	}

	@Override
	public List<AttachmentModel> findBySourceId(String sourceId) {
		String authorization = UserContextHolder.getAuthorization();
		if(StringUtils.isBlank(sourceId)) {
			return null;
		}
		Map<String,String> heads = new HashMap<String,String>();
		heads.put("Authorization", authorization);
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("sourceId", sourceId);
		
		
		String res = microServiceCaller.callApi(config.getAttachmentFindBySourceId(), Scheme.HTTP, config.getAttachmentFindBySourceIdMethod(),true,params, heads, String.class);
		ResultInfoModel<List<AttachmentModel>> infoModel = AttachmentModel.convert(res);
		if(infoModel != null && infoModel.getCode() == 0){
			return infoModel.getResult();
		} else{
			return new ArrayList<AttachmentModel>();
		}
	}

	@Override
	public Map<String,List<AttachmentModel>> findBySourceId(List<String> sourceIds) {
		String authorization = UserContextHolder.getAuthorization();
		if(CollectionUtils.isEmpty(sourceIds)) {
			return null;
		}
		Map<String,String> heads = new HashMap<String,String>();
		heads.put("Authorization", authorization);
		String res = microServiceCaller.callApi(config.getAttachmentFindBySourceIds(), Scheme.HTTP, config.getAttachmentFindBySourceIdsMethod(),sourceIds, heads, String.class);
		ResultInfoModel<Map<String,List<AttachmentModel>>> infoModel = AttachmentModel.convertMapList(res);
		if(infoModel != null && infoModel.getCode() == 0){
			return infoModel.getResult();
		} else{
			return Maps.newHashMap();
		}
	}

	@Override
	public Boolean batchDeleteBySourceId(List<String> sourceIds) {
		String authorization = UserContextHolder.getAuthorization();
		if(sourceIds==null||sourceIds.isEmpty()) {
			return false;
		}

		Map<String,String> heads = new HashMap<String,String>();
		heads.put("Authorization", authorization);

		String res = microServiceCaller.callApi(config.getAttachmentBatchDeleteBySourceId(), Scheme.HTTP, config.getAttachmentBatchDeleteBySourceIdMethod(),sourceIds, heads, String.class);
		ResultInfoModel<Object> infoModel = AttachmentModel.convertObj(res);
		if(infoModel != null && infoModel.getCode() == 0){
			return true;
		} else{
			throw ExceptionModel.convert(res);
		}
	}


}
