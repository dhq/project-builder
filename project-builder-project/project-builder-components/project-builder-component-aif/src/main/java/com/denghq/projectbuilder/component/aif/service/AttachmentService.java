package com.denghq.projectbuilder.component.aif.service;

import java.util.List;
import java.util.Map;

import com.denghq.projectbuilder.component.aif.model.AttachmentModel;

public interface AttachmentService {

	/**
	 * 通过附件Id 更新附件源文件Id
	 * @param attachmentIds
	 * @param attachmentType
	 * @param sourceId
	 */
	Boolean updateSourceId(List<String> attachmentIds, String attachmentType, String sourceId);

	/**
	 * 通过附件Id 更新附件源文件Id，并删除多于的附件
	 * @param attachmentIds
	 * @param attachmentType
	 * @param sourceId
	 */
	Boolean updateOrRemove(List<String> attachmentIds, String attachmentType, String sourceId);

	/**
	 * 通过附件Id 更新附件源文件Id
	 * @param attachmentId
	 * @param attachmentType
	 * @param sourceId
	 */
	Boolean updateSourceId(String attachmentId, String attachmentType, String sourceId);

	/**
	 * 批量更新或移除数据源对应附件信息
	 * @param attachmentMap
	 * @return
	 */
	Boolean batchUpdateOrRemove(Map<String, String> attachmentMap);
	
	/**
	 * 通过来源id获取附件列表
	 * @param sourceId
	 * @return
	 */
	List<AttachmentModel> findBySourceId( String sourceId);

	/**
	 * 通过来源id列表批量获取附件列表
	 * @param sourceIds
	 * @return
	 */
	Map<String,List<AttachmentModel>> findBySourceId(List<String> sourceIds);


	/**
	 * 通过来源id列表批量删除附件
	 * @param idList
	 * @return
	 */
	Boolean batchDeleteBySourceId(List<String> idList);
}
