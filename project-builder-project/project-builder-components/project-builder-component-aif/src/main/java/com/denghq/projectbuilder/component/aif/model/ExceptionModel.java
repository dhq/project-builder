package com.denghq.projectbuilder.component.aif.model;

import com.denghq.projectbuilder.common.exception.CloudException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper=false)
public class ExceptionModel extends CloudException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer code;
    private String codeRemark;


    public static ExceptionModel convert(String json) {

        JsonObject returnData = new JsonParser().parse(json).getAsJsonObject();
        JsonPrimitive code = returnData.getAsJsonPrimitive("code");
        JsonPrimitive codeRemark = returnData.getAsJsonPrimitive("codeRemark");
        ExceptionModel e = new ExceptionModel(code.getAsInt(),codeRemark.getAsString());
        e.setRes(json);
        return  e;
    }
}
