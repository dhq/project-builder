package com.denghq.projectbuilder.component.aif.service;

import com.denghq.projectbuilder.component.aif.model.RabbitMQConfigModel;

import java.util.Map;

public interface ConfigService {

    Map<String,Object> getConfigByCategoryNo(String categoryNo);

    /**
     * 获取消息队列配置
     * @return
     */
    RabbitMQConfigModel getRabbitMQConfig();
}
