package com.denghq.projectbuilder.component.aif.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sun.misc.BASE64Encoder;
/**
 * @program: KASC
 * @description: MD5 工具类
 * @author: Liu yang
 * @create: 2018-10-08 11:01
 **/
@SuppressWarnings("restriction")
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Md5Utils {
    private static MessageDigest MD5 = null;
        
    static {
        try {
            MD5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            log.error(ex.getMessage());
        }
    }
	public static String encode(String value) {
        String result = "";
        if (value == null) {
            return result;
        }
        BASE64Encoder baseEncoder = new BASE64Encoder();
        try {
            result = baseEncoder.encode(MD5.digest(value.getBytes("utf-8")));
        } catch (Exception ex) {
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(encode("ScenarioController"+"scenario"));


    }
}