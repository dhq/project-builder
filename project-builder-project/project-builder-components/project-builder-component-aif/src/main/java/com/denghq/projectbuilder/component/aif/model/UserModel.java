package com.denghq.projectbuilder.component.aif.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.Data;

import java.lang.reflect.Type;
import java.util.List;

@Data
public class UserModel {
	private String id;

    private String userGUID;

    private String userCode;

    private String userName;

    private String buName;

    private String departmentId;

    private boolean isDisabeld;

    public static ResultInfoModel<List<UserModel>> convertList(String json) {
        Type jsonType = new TypeToken<ResultInfoModel<List<UserModel>>>() {}.getType();
        ResultInfoModel<List<UserModel>> bean = new Gson().fromJson(json, jsonType);
        return bean;
    }
}
