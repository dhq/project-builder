package com.denghq.projectbuilder.component.aif.utils;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.denghq.projectbuilder.component.aif.Register;
import com.denghq.projectbuilder.component.aif.RegisterApp;
import com.denghq.projectbuilder.component.aif.RegisterConfig;
import com.denghq.projectbuilder.component.aif.RegisterHost;
import com.denghq.projectbuilder.component.aif.RegisterPath;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class RegisterUtils {

	@Autowired
	private RegisterConfig registerConfig;
	
	@Value("${server.port}")
    String port;
	
	@Value("${server.servlet.context-path}")
	String contextPath;
	
	@Autowired
	private ApiParser apiParser;
	
	public String getRegistServiceUrl() {
		return registerConfig.getRegistServiceUrl();
	}
	
	public String getUnRegisterServiceUrl() {
		return registerConfig.getUnRegisterServiceUrl();
	}
	
	public Register getRegisterServiceVo() {

		Register vo = new Register();

		vo.setApp(registerApp());
		vo.setHosts(registerHosts());
		vo.setPaths(registerPaths());
		return vo;
	}

	private RegisterApp registerApp() {
		RegisterApp app = new RegisterApp();
		app.setIdtf("SysManager");//需要和no一致
		app.setNo("SysManager");//编号，根据实际情况修改
		app.setTitle("基础数据管理服务");
		app.setVersion("1.0.0");
		app.setTags("基础数据管理");//标签
		app.setSummary("基础数据管理");//说明
		return app;
	}

	private Set<RegisterHost> registerHosts() {
		Set<RegisterHost> hosts = new HashSet<>();
		hosts.add(getMenuHost());
		hosts.add(getHttpHost());
		return hosts;
	}

	private RegisterHost getMenuHost() {

		RegisterHost host = new RegisterHost();
		host.setHost(registerConfig.getRegisterIp() + ":" + port);
		host.setScheme("menu");
		return host;
	}

	private RegisterHost getHttpHost() {
		RegisterHost host = new RegisterHost();
		host.setHost(registerConfig.getRegisterIp() + ":" + port);
		host.setScheme("http");
		return host;
	}

	private Set<RegisterPath> registerPaths() {
		Set<RegisterPath> paths = new HashSet<>();
		registerMenu(paths);
		registerApi(paths);
		return paths;
	}

	private void registerApi(Set<RegisterPath> paths) {
		paths.addAll(apiParser.getPathSet());
	}
	private void registerMenu(Set<RegisterPath> paths) {
		paths.add(fmwMenu());
	}
	
	private RegisterPath fmwMenu() {
		RegisterPath path = new RegisterPath();
		path.setScheme("menu");
		path.setPath("/SysManager");
		path.setFeature("/#/");
		path.setMethod("");
		path.setVersion("1.0.0");
		path.setTags("基础数据管理");
		path.setSummary("基础数据管理");
		return path;
	}
	
	public static String unRegister(String url, String requestBody) {
		HttpHeaders headers = new HttpHeaders();
		MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
		headers.setContentType(type);
		headers.add("Accept", MediaType.APPLICATION_JSON.toString());
		HttpEntity<String> httpEntity = new HttpEntity<String>(requestBody, headers);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, httpEntity, String.class);
		return responseEntity.getBody();
	}
	
	public void register() {

		//注册服务
		registerService();
		
		//注册字典
		registerDict();

	}

	private void registerDict() {
		//添加字典类型
		
		//预案类型 10056
		//添加字典
		
		//预案等级 10057
		//添加字典
	
	}

	private void registerService() {
		Register registerVo = this.getRegisterServiceVo();
        String requestBody = JsonUtil.beanToString(registerVo);
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        HttpEntity<String> httpEntity = new HttpEntity<String>(requestBody, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(this.getRegistServiceUrl(),httpEntity, String.class);
        log.info(">>>>>>>>>>>>注册服务>>>>>>>>"+responseEntity.getBody());
	}
	
	
	
}
