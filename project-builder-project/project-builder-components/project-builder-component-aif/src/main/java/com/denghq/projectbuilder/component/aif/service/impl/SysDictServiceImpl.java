package com.denghq.projectbuilder.component.aif.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.denghq.projectbuilder.component.aif.ApiConfig;
import com.denghq.projectbuilder.component.aif.Scheme;
import com.denghq.projectbuilder.component.aif.SysDictKindConfig;
import com.denghq.projectbuilder.component.aif.model.DictModel;
import com.denghq.projectbuilder.component.aif.model.ExceptionModel;
import com.denghq.projectbuilder.component.aif.model.ResultInfoModel;
import com.denghq.projectbuilder.component.aif.service.MicroServiceCaller;
import com.denghq.projectbuilder.component.aif.service.SysDictService;
import com.denghq.projectbuilder.component.aif.utils.UserContextHolder;


@Service("sysDictService")
public class SysDictServiceImpl implements SysDictService {
	
	
	@Autowired
	MicroServiceCaller microServiceCaller;
	
	@Autowired
	private ApiConfig config;
	
	@Autowired
	private SysDictKindConfig sysDictKindConfig; 

	@Override
	public List<DictModel> getDictByKind(String kind) {
		String authorization = UserContextHolder.getAuthorization();
		Map<String,String> heads = new HashMap<String,String>();
		heads.put("Authorization", authorization);
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("kind", kind);
		String res = microServiceCaller.callApi(config.getGetDictByKind(), Scheme.HTTP, config.getGetDictByKindMethod(),params, heads, String.class);
		ResultInfoModel<List<DictModel>> infoModel = DictModel.convert(res);
		if(infoModel != null && infoModel.getCode() == 0){
			return infoModel.getResult();
		} else{
			throw new RuntimeException(res);
		}
		//return ResultInfoModel.convertList(res, new DictModel());
	}
	
	@Override
	public Map<String, List<DictModel>> getSomeDicts(List<String> kinds) {
		String authorization = UserContextHolder.getAuthorization();
		Map<String,String> heads = new HashMap<String,String>();
		heads.put("Authorization", authorization);
		String res = microServiceCaller.callApi(config.getGetSomeDicts(), Scheme.HTTP, config.getGetSomeDictsMethod(),kinds, heads, String.class);
		
		ResultInfoModel<Map<String,List<DictModel>>> infoModel = DictModel.convertMap(res);
		if(infoModel != null && infoModel.getCode() == 0){
			return infoModel.getResult();
		} else{
			throw ExceptionModel.convert(res);
		}
	}

	@Override
	public List<DictModel> getCompanyType() {
		return this.getDictByKind(sysDictKindConfig.getCompanyType());
	}

	@Override
	public List<DictModel> getSpottingGroupType() {
		return this.getDictByKind(sysDictKindConfig.getSpottingGroupType());
	}

	@Override
	public List<DictModel> getDeviceGroupType() {
		return this.getDictByKind(sysDictKindConfig.getDeviceGroupType());
	}

	@Override
	public List<DictModel> getRoadType() {
		return this.getDictByKind(sysDictKindConfig.getRoadType());
	}

}
