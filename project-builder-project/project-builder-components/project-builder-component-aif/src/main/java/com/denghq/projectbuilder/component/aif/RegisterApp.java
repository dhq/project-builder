package com.denghq.projectbuilder.component.aif;

import lombok.Data;

/**
 *@program: key-areas-security-control
 *@description: 注册app
 *@author: Liu yang
 *@create: 2018-10-17 10:14
 **/
@Data
public class RegisterApp {

    /**
     * 运行实例标示 （必须）
     */
    private String idtf;
    /**
     * 当前应用的编号，注册应用时分配的（必须）
     */
    private String no;
    /**
     * 当前应用的标题，应和注册应用时的标题一致 （必须）
     */
    private String title;
    /**
     * 应用的版本信息 （必须）
     */
    private String version;
    /**
     * 应用的描述信息 （必须）
     */
    private String summary;
    /**
     * 是一个字符串数组,方便根据标签查找应用 （必须）
     */
    private String tags;


}
