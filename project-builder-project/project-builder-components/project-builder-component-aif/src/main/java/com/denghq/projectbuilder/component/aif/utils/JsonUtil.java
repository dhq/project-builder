package com.denghq.projectbuilder.component.aif.utils;

import java.io.IOException;

import com.google.gson.Gson;

/**
 * json转换工具类
 */
public class JsonUtil{

    private JsonUtil(){

    }

    public static String beanToString(Object object){
    	
        return new Gson().toJson(object);
    }

    public static <T> T stringToBean(String string, Class<T> clazz) throws IOException{
    	
    	Gson gson = new Gson();
	    T res = gson.fromJson(string, clazz);
	    return res;
    }

}
