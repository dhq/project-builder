package com.denghq.projectbuilder.component.aif;

import lombok.Data;

/**
 *@program: key-areas-security-control
 *@description: 注册Host
 *@author: Liu yang
 *@create: 2018-10-17 10:14
 **/
@Data
public class RegisterHost {

    /**
     * 主机提供的协议类型，目前支持http和menu
     * http表示WebApi接口
     * menu表示集成框架（AIF）菜单项目 (必须)
     */
    private String scheme;

    /**
     * 主机地址，形如192.168.0.217:8081 （必须）
     */
    private String host;
}
