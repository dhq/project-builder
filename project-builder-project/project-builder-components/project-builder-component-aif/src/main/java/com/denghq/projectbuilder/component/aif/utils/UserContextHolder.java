package com.denghq.projectbuilder.component.aif.utils;

import javax.servlet.http.HttpServletRequest;

import com.denghq.projectbuilder.component.aif.model.UserRightViewModel;
import com.denghq.projectbuilder.component.aif.service.SysUserService;

public class UserContextHolder {
	
	public static String getUserId() {
		
		UserRightViewModel userInfo = getUserInfo();
		if(userInfo != null){
			return userInfo.getUserCode();
		}else{
			return null;
		}
	}
	
	
	public static UserRightViewModel getUserInfo(){

		SysUserService sysUserService = SpringContextUtils.getBean(SysUserService.class);
		return sysUserService.getUserInfo();
		
	}
	
	public static String getAuthorization(){
		HttpServletRequest req = HttpContextUtils.getHttpServletRequest();	
		return req.getHeader("Authorization");
	}
	
}
