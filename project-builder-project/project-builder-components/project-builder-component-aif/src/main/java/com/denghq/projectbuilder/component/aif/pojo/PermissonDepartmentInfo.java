package com.denghq.projectbuilder.component.aif.pojo;

import lombok.Data;

import java.util.List;

/**
 * 部门权限信息
 */
@Data
public class PermissonDepartmentInfo {

    //是否可以访问所有的部门数据
    public boolean fullPermisson;

    //访问受限时可访问的部门id列表
    private List<String> departmentIdList;

}
