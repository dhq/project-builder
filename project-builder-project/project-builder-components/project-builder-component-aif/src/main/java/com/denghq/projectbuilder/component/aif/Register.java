package com.denghq.projectbuilder.component.aif;

import lombok.Data;

import java.util.Set;

/**
 *@program: key-areas-security-control
 *@description: 注册
 *@author: Liu yang
 *@create: 2018-10-17 10:13
 **/
@Data
public class Register {

    RegisterApp app;

    Set<RegisterHost> hosts;

    Set<RegisterPath> paths;
}
