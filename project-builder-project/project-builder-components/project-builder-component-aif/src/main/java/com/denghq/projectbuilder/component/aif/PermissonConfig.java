package com.denghq.projectbuilder.component.aif;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Data
@Component
@PropertySource("classpath:cloud.properties")
@ConfigurationProperties(prefix="denghq.cloud.permisson")
public class PermissonConfig {

    private String spottingSearchFc;

    private String spottingSearchFullDeptAc;

    public String frontdeviceSearchFc;

    public String frontdeviceSearchFullDeptAc;
}
