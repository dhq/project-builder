package com.denghq.projectbuilder.component.aif.service;

import java.util.List;
import java.util.Map;

import com.denghq.projectbuilder.component.aif.model.DictModel;

/**
 * 字典
 * 
 * @author denghq
 * 
 * @date 2018/10/29
 */
public interface SysDictService{

	List<DictModel> getDictByKind(String kind);
	
	//批量查询字典
	Map<String,List<DictModel>> getSomeDicts(List<String> kinds);

	/**
	 * 获取公司类型
	 * @return
	 */
	List<DictModel> getCompanyType();

	/**
	 * 获取路口组类型
	 * @return
	 */
	List<DictModel> getSpottingGroupType();

	/**
	 * 获取设备组分类
	 * @return
	 */
	List<DictModel> getDeviceGroupType();

	/**
	 * 获取道路分类
	 * @return
	 */
	List<DictModel> getRoadType();
}
