package com.denghq.projectbuilder.component.aif.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * @描述
 * @创建人 changqijing
 * @创建时间 2018/6/22
 */
@Component
@Configurable
public class RestTemplateConfig {

	/**
     * 启动的时候要注意，由于我们在controller中注入了RestTemplate，所以启动的时候需要实例化该类的一个实例
     */
    @Autowired
    private RestTemplateBuilder builder;
    
    @Bean
    @Primary
    public RestTemplate getRestTemplate(){
    	return builder.build();
    }
    
}
