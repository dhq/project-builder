package com.denghq.projectbuilder.component.aif.config;

import javax.servlet.DispatcherType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

@Configurable
public class FilterConfig {
	
	@Autowired
	private MicroAgentFilter microAgentFilter;
	
    @Bean
    public FilterRegistrationBean<MicroAgentFilter> microAgentFilterRegistration() {
        FilterRegistrationBean<MicroAgentFilter> registration = new FilterRegistrationBean<MicroAgentFilter>();
        registration.setDispatcherTypes(DispatcherType.REQUEST);
        registration.setFilter(microAgentFilter);
        registration.addUrlPatterns("/*");
        registration.setName("microAgentFilter");
        registration.setOrder(Integer.MAX_VALUE -1 );
        return registration;
    }
}
