package com.denghq.projectbuilder.component.aif.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.Data;

import java.lang.reflect.Type;
import java.util.Map;

@Data
public class MapModel {

    public static ResultInfoModel<Map<String,Object>> convert(String json) {
        Type jsonType = new TypeToken<ResultInfoModel<Map<String,Object>>>() {}.getType();
        ResultInfoModel<Map<String,Object>> bean = new Gson().fromJson(json, jsonType);
        return bean;
    }
}
