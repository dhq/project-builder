package com.denghq.projectbuilder.component.aif;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@PropertySource("classpath:cloud.properties")
@ConfigurationProperties(prefix="denghq.cloud")
public class RegisterConfig {

	private String registServiceUrl;
	
	private String unRegisterServiceUrl;
	
	private String getServiceNodeUrl;
	
	private String loginUrl;
	
	private String registerIp;

	private Long autoRegisteTimeinterval;

	private Boolean autoRegiste;
	
}
