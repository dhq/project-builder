package com.denghq.projectbuilder.component.consul.startup;

import com.denghq.projectbuilder.component.consul.service.Discovery;
import com.denghq.projectbuilder.component.consul.service.Register;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ServiceInfoStarter implements CommandLineRunner {

    @Autowired
    Discovery discovery;

    @Autowired
    Register register;

    @Value("${spring.application.name}")
    private String serviceName;

    @Override
    public void run(String... args) {
       /* new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(10_000);
                } catch (InterruptedException e) {

                }
                register.register();//自动重新注册
            }
        }).start();//启动自动注册功能*/
        log.info("【{}】当前存在的服务----------------------:" + discovery.getServices(), serviceName);
    }
}
