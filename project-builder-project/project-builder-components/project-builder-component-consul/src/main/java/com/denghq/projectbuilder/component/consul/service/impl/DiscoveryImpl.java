package com.denghq.projectbuilder.component.consul.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Component;

import com.denghq.projectbuilder.component.consul.service.Discovery;

@Component
public class DiscoveryImpl implements Discovery {

    @Autowired
    private DiscoveryClient discoveryClient;

    @Override
    public List<String> getServices() {
        return discoveryClient.getServices();
    }
}
