package com.denghq.projectbuilder.component.consul.service;

import java.util.List;

public interface Discovery {
    List<String> getServices();
}
