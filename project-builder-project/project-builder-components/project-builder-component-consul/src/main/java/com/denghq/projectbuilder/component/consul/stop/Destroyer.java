package com.denghq.projectbuilder.component.consul.stop;


import javax.annotation.PreDestroy;

import com.denghq.projectbuilder.component.consul.service.Deregister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.denghq.projectbuilder.component.consul.service.Discovery;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class Destroyer {

    @Autowired
    Discovery discovery;

    @Autowired
    Deregister deregister;
    
    @Value("${spring.application.name}")
    private String serviceName;
    
    @PreDestroy
    public void destory() {
        try {
            deregister.deregister();
            log.info("【{}】服务销毁，当前存在的服务----------------------:"+discovery.getServices() , serviceName);
        } catch (Exception e) {
            log.error("【{}】去注册中心注销服务 错误>>>>>>>>>>>>>>>>>>>,{}" , serviceName,e.getMessage());
        }
    }
}
