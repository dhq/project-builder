package com.denghq.projectbuilder.component.consul.service;


public interface Register {
    /**
     * 注册当前服务
     */
    void register();
}
