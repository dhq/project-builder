package com.denghq.projectbuilder.component.consul.autoconfigure;

import com.denghq.projectbuilder.component.consul.startup.ServiceInfoStarter;
import com.denghq.projectbuilder.component.consul.stop.Destroyer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConsulAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(ServiceInfoStarter.class)
    public ServiceInfoStarter serviceInfoStarter() {
        return new ServiceInfoStarter();
    }

    @Bean
    @ConditionalOnMissingBean(Destroyer.class)
    public Destroyer destroyer() {
        return new Destroyer();
    }
}
