package com.denghq.projectbuilder.component.consul.service;

public interface Deregister {

    /**
     * 注销当前服务实例
     */
    void deregister();

    /**
     * 注销指定服务实例
     * @param instanceId
     */
    void deregister(String instanceId);

}
