package com.denghq.projectbuilder.component.consul.service.impl;


import com.denghq.projectbuilder.component.consul.service.Register;
import org.springframework.cloud.consul.serviceregistry.ConsulRegistration;
import org.springframework.cloud.consul.serviceregistry.ConsulServiceRegistry;
import org.springframework.stereotype.Component;

@Component
public class RegisterImpl implements Register {

    private final ConsulServiceRegistry consulServiceRegistry;
    private final ConsulRegistration consulRegistration;

    public RegisterImpl(ConsulServiceRegistry consulServiceRegistry, ConsulRegistration consulRegistration) {
        this.consulServiceRegistry = consulServiceRegistry;
        this.consulRegistration = consulRegistration;
    }

    @Override
    public void register() {
        consulServiceRegistry.register(consulRegistration);
    }
}
