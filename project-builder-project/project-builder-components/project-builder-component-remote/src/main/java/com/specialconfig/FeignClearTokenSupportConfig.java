package com.specialconfig;

import com.denghq.projectbuilder.common.Constant;
import com.denghq.projectbuilder.component.remote.utils.UserContextHolder;
import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignClearTokenSupportConfig {

    @Bean
    public RequestInterceptor headerInterceptor() {
        String token = UserContextHolder.getAuthorization();
        return requestTemplate ->
                requestTemplate.header(Constant.SERVICE_TOKEN_NAME, token);

    }
}