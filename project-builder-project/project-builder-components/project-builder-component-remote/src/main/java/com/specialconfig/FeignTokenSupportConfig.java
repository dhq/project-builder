package com.specialconfig;

import com.denghq.projectbuilder.common.Constant;
import com.denghq.projectbuilder.component.remote.utils.UserContextHolder;
import feign.RequestInterceptor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignTokenSupportConfig {

    public static final String SPECIAL_AUTHORIZATION = "Basic 0FA7WB2KP3R317519OBPM05";

    @Bean
    public RequestInterceptor headerInterceptor() {
        String token = UserContextHolder.getAuthorization();
        if (StringUtils.isBlank(token)) {
            return requestTemplate ->
                    requestTemplate.header(Constant.SERVICE_TOKEN_NAME, SPECIAL_AUTHORIZATION);
        } else {
            return requestTemplate ->
                    requestTemplate.header(Constant.SERVICE_TOKEN_NAME, token);
        }

    }
}