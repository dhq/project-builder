package com.denghq.projectbuilder.component.remote.service.impl;


import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenDepartmentClient;
import com.denghq.projectbuilder.component.remote.model.DepartmentModel;
import com.denghq.projectbuilder.component.remote.service.SmwDepartmentService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SmwDepartmentServiceImpl implements SmwDepartmentService {

    @Autowired
    private WithTokenDepartmentClient departmentClient;

    @Override
    public List<DepartmentModel> all() {
        return CloudUtils.getResult(departmentClient.findAll());
    }
}
