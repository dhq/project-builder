package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.cache.CacheManager;
import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenDictClient;
import com.denghq.projectbuilder.component.remote.config.properties.SysDictKindProp;
import com.denghq.projectbuilder.component.remote.model.DictModel;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.denghq.projectbuilder.component.remote.service.SysDictService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class SysDictServiceImpl implements SysDictService {

	@Autowired
	private WithTokenDictClient dictClient;
	
	@Autowired
	private SysDictKindProp sysDictKindProp;

	@Override
	public List<DictModel> getDictByKind(String kind) {

		List<DictModel> dictData = CacheManager.getDictByKind(kind);
		if(dictData == null){
			ResultInfoModel<List<DictModel>> infoModel = dictClient.getDictByKind(kind);
			dictData = CloudUtils.getResult(infoModel);
			CacheManager.saveDictByKind(kind,dictData);
		}
		return dictData;
	}
	
	@Override
	public Map<String, List<DictModel>> getSomeDicts(List<String> kinds) {

		Map<String, List<DictModel>> dictData = CacheManager.getSomeDicts(kinds);
		if(dictData == null){
			ResultInfoModel<Map<String, List<DictModel>>> infoModel = dictClient.getSomeDicts(kinds);
			dictData = CloudUtils.getResult(infoModel);
			CacheManager.saveSomeDicts(dictData);
		}
		return dictData;
	}

	@Override
	public Map<String, List<DictModel>> getSomeDictsWithOutCache(List<String> kinds) {
		ResultInfoModel<Map<String, List<DictModel>>> infoModel = dictClient.getSomeDicts(kinds);
		return CloudUtils.getResult(infoModel);
	}

	@Override
	public List<DictModel> getCompanyType() {
		return this.getDictByKind(sysDictKindProp.getCompanyType());
	}

	@Override
	public List<DictModel> getSpottingGroupType() {
		return this.getDictByKind(sysDictKindProp.getSpottingGroupType());
	}

	@Override
	public List<DictModel> getDeviceGroupType() {
		return this.getDictByKind(sysDictKindProp.getDeviceGroupType());
	}

	@Override
	public List<DictModel> getRoadType() {
		return this.getDictByKind(sysDictKindProp.getRoadType());
	}

}
