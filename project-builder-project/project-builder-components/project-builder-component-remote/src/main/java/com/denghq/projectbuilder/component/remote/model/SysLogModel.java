package com.denghq.projectbuilder.component.remote.model;

import lombok.Data;

import java.util.Date;

@Data
public class SysLogModel {

    //private String logId;
    private String userCode;
    private String userName;
    private String departmentId;
    private String appNo;
    private String hostIP;
    private String functionCode;
    private String functionName;
    private String actionCode;
    private String actionName;
    private Date createdTime;
    private Object content;
    private String remark;

    private Long elapsedMilliseconds;
    private String requestParam;
    private String response;
}
