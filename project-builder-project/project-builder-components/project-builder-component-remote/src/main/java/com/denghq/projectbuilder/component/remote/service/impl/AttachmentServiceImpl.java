package com.denghq.projectbuilder.component.remote.service.impl;


import com.denghq.projectbuilder.component.remote.client.AttachmentClient;
import com.denghq.projectbuilder.component.remote.model.AttachmentModel;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.denghq.projectbuilder.component.remote.service.AttachmentService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AttachmentServiceImpl implements AttachmentService {

	@Autowired
	private AttachmentClient attachmentClient;

	@Override
	public Boolean updateSourceId(List<String> attachmentIds, String sourceId) {
		if(!CollectionUtils.isEmpty(attachmentIds)){
			ResultInfoModel<Integer> infoModel = attachmentClient.updateSourceId(sourceId,attachmentIds);
			CloudUtils.getResult(infoModel);
			return true;
		}
		return false;
	}

	@Override
	public Boolean updateOrRemove(List<String> attachmentIds, String sourceId) {
		if(!CollectionUtils.isEmpty(attachmentIds)){
			ResultInfoModel<Integer> infoModel = attachmentClient.updateOrRemove(sourceId,attachmentIds);
			CloudUtils.getResult(infoModel);
			return true;
		}
		return false;
	}

	@Override
	public Boolean updateSourceId(String attachmentId, String sourceId) {

		if(StringUtils.isNotBlank(attachmentId)) {
			List<String> attachmentIds = new ArrayList<String>();
			attachmentIds.add(attachmentId);
			return this.updateSourceId(attachmentIds , sourceId);
		}
		return false;
	}

	@Override
	public Boolean batchUpdateOrRemove(Map<String, String> attachmentMap) {

		if(attachmentMap==null||attachmentMap.isEmpty()) {
			return false;
		}

		Map<String,Object> sourceAttachmentIds = new HashMap<String,Object>();
		attachmentMap.forEach((sourceId,attachmentId)->{
			List<String> attachmentIds = new ArrayList<String>();
			if(StringUtils.isNotBlank(attachmentId)) {
				attachmentIds.add(attachmentId);
			}
			sourceAttachmentIds.put(sourceId, attachmentIds);
		});

		ResultInfoModel<Integer> infoModel = attachmentClient.batchUpdateOrRemove(sourceAttachmentIds);
		CloudUtils.getResult(infoModel);
		return true;

	}

	@Override
	public List<AttachmentModel> findBySourceId(String sourceId) {
		if(StringUtils.isBlank(sourceId)) {
			return null;
		}

		ResultInfoModel<List<AttachmentModel>> infoModel = attachmentClient.findBySourceId(sourceId);
		return CloudUtils.getResult(infoModel);

	}

	@Override
	public Map<String,List<AttachmentModel>> findBySourceId(List<String> sourceIds) {
		if(CollectionUtils.isEmpty(sourceIds)) {
			return null;
		}
		ResultInfoModel<Map<String,List<AttachmentModel>>> infoModel = attachmentClient.findBySourceId(sourceIds);
		return CloudUtils.getResult(infoModel);
	}

	@Override
	public Boolean batchDeleteBySourceId(List<String> sourceIds) {
		if(sourceIds==null||sourceIds.isEmpty()) {
			return false;
		}

		ResultInfoModel<Integer> infoModel = attachmentClient.deleteBySourceIds(sourceIds);
		CloudUtils.getResult(infoModel);
		return true;
	}


}
