package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.RecogAreaModel;

import java.util.List;

public interface RecogAreaService {

    //查询所有识别区域
    List<RecogAreaModel> all();

}