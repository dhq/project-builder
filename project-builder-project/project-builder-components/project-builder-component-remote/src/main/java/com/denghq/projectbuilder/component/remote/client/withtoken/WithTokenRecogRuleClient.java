package com.denghq.projectbuilder.component.remote.client.withtoken;

import com.denghq.projectbuilder.component.remote.common.Constant;
import com.denghq.projectbuilder.component.remote.model.RecogRuleModel;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.specialconfig.FeignTokenSupportConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(name = Constant.SERVICE_SAW_CODE, configuration = FeignTokenSupportConfig.class)
public interface WithTokenRecogRuleClient {

    @GetMapping("/saw/structRecogConfig/rule")
    ResultInfoModel<Map<String, List<RecogRuleModel>>> getRule(@RequestParam(value = "ruleType") String ruleType);

}