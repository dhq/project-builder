package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenCommonCompanyClient;
import com.denghq.projectbuilder.component.remote.model.CommonCompanyModel;
import com.denghq.projectbuilder.component.remote.service.CommonCompanyService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CommonCompanyServiceImpl implements CommonCompanyService {

    @Autowired
    private WithTokenCommonCompanyClient commonCompanyClient;

    @Override
    public List<CommonCompanyModel> all() {
        Map<String, Object> params = Maps.newHashMap();
        params.put("isPagination", false);
        return CloudUtils.getResult(commonCompanyClient.search(params));
    }
}