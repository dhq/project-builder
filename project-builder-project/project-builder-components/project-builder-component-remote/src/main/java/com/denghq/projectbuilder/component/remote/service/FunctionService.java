package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.FunctionModel;

import java.util.List;

public interface FunctionService {

    //查询所有功能菜单
    List<FunctionModel> all();
}
