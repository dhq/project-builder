package com.denghq.projectbuilder.component.remote.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 设备分组
 * 
 * @author denghq
 * @email 
 * @date 2018/10/29
 */
@ApiModel("设备分组")
@Data
public class CommonFrontdeviceGroupModel implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 设备分组ID
	 */
	@ApiModelProperty("设备分组ID")
	private String devicegroupid;
	/**
	 * 设备分组名称
	 */
	@ApiModelProperty("设备分组名称")
	private String devicegroupname;
	/**
	 * 组类型
	 */
	@ApiModelProperty("组类型")
	private String devicegrouptype;
	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	private Date createdtime;
	/**
	 * 创建人
	 */
	@ApiModelProperty("创建人")
	private String creator;
	/**
	 * 修改人
	 */
	@ApiModelProperty("修改人")
	private String modifier;
	/**
	 * 修改时间
	 */
	@ApiModelProperty("修改时间")
	private Date modifiedtime;
	/**
	 * 备注
	 */
	@ApiModelProperty("备注")
	private String remark;
	/**
	 * 拼音简称
	 */
	@ApiModelProperty("拼音简称，后台自动设置")
	private String bopomofo;

}
