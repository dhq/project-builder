package com.denghq.projectbuilder.component.remote.model;

import lombok.Data;

@Data
public class FunctionModel {

    private String functionGUID;

    private String functionCode;

    private String functionName;

    private String hierarchyCode;

    private String parentHierarchyCode;

    private String orderCode;

    private String orderHierarchyCode;

    private String functionLevel;

    private String isLastStage;

    private String functionIcon;

    private String functionUrl;

    private String comments;

    Boolean isDisabled;

    private String parentFunctionGUID;

    private String functionType;

    private String groupNo;

    private String appNo;
}
