package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.model.AppModel;
import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenAppClient;
import com.denghq.projectbuilder.component.remote.service.AppService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppServiceImpl implements AppService {

    @Autowired
    private WithTokenAppClient appClient;

    @Override
    public List<AppModel> all() {
        return CloudUtils.getResult(appClient.findAll());
    }
}
