package com.denghq.projectbuilder.component.remote.model;

import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("道路信息查询结果")
@Data
public class CommonRoadModel{
	
	
	/**
	 * 道路ID-主键
	 */
	@ApiModelProperty("道路ID-主键")
	private String roadid;
	
	/**
	 * 道路编号
	 */
	@ApiModelProperty("道路编号")
	private String roadno;
	
	/**
	 * 道路名称
	 */
	@ApiModelProperty("道路名称")
	private String roadname;
	
	/**
	 * 道路类型
	 */
	@ApiModelProperty("道路类型")
	private String roadtypeid;

	/**
	 * 道路类型名称
	 */
	@ApiModelProperty("道路类型")
	private String roadtypename;

	/**
	 * 是否双向车道（1：是 0：否 默认 1）
	 */
	@ApiModelProperty("是否双向车道（1：是 0：否 默认 1）")
	private BigDecimal istwoway;
	
	/**
	 * 创建人
	 */
	@ApiModelProperty("创建人")
	private String creator;
	
	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	private Date createdtime;
	
	/**
	 * 修改人
	 */
	@ApiModelProperty("修改人")
	private String modifier;
	
	/**
	 * 修改时间
	 */
	@ApiModelProperty("修改时间")
	private Date modifiedtime;
	
	/**
	 * 备注
	 */
	@ApiModelProperty("备注")
	private String remark;
	
}
