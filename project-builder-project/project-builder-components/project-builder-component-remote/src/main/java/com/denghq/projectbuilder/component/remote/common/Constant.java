package com.denghq.projectbuilder.component.remote.common;

public class Constant {

	/**
	 * 系统管理服务
	 */
	public static final String SERVICE_SMW_CODE = "smw";
	
	/**
	 * 通用接口服务
	 */
	public static final String SERVICE_COMMON_CODE =  "common";

	/**
	 * 基础数据web服务
	 */
    public static final String SERVICE_FMW_CODE = "fmw";

	/**
	 * 通行监控web服务
	 */
	public static final String SERVICE_TMW_CODE = "tmw";

	/**
	 * 结构化解析web服务
	 */
    public static final String SERVICE_SAW_CODE = "saw";
}
