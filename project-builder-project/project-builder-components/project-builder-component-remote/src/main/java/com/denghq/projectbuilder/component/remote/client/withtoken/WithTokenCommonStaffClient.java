package com.denghq.projectbuilder.component.remote.client.withtoken;

import com.denghq.projectbuilder.component.remote.common.Constant;
import com.denghq.projectbuilder.component.remote.model.CommonStaffModel;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.specialconfig.FeignTokenSupportConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@FeignClient(name = Constant.SERVICE_FMW_CODE, configuration = FeignTokenSupportConfig.class)
public interface WithTokenCommonStaffClient {

    @PostMapping("/fmw/commonStaff/search")
    ResultInfoModel<List<CommonStaffModel>> search(@RequestBody Map<String, Object> params);

}