package com.denghq.projectbuilder.component.remote.service;

import java.util.Map;

import com.denghq.projectbuilder.component.remote.model.RabbitMQConfigModel;

public interface ConfigService {

    Map<String,Object> getConfigByCategoryNo(String categoryNo);

    /**
     * 获取消息队列配置
     * @return
     */
    RabbitMQConfigModel getRabbitMQConfig();
}
