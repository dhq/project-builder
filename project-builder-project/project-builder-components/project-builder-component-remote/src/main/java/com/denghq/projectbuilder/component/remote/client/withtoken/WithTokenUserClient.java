package com.denghq.projectbuilder.component.remote.client.withtoken;

import com.denghq.projectbuilder.component.remote.common.Constant;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.denghq.projectbuilder.component.remote.model.UserModel;
import com.specialconfig.FeignTokenSupportConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * 调用用户管理服务
 */
@FeignClient(name = Constant.SERVICE_SMW_CODE,configuration = FeignTokenSupportConfig.class)
public interface WithTokenUserClient {

    @GetMapping("/smw/Account/FindAll")
    ResultInfoModel<List<UserModel>> findAll();
}
