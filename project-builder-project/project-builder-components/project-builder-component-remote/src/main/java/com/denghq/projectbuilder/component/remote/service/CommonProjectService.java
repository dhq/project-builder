package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.CommonProjectModel;

import java.util.List;

public interface CommonProjectService {

    //查询所有项目
    List<CommonProjectModel> all();

}