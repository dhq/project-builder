package com.denghq.projectbuilder.component.remote.model;

import lombok.Data;

//用户Code Password登录
@Data
public class CPLoginModel {
	
	private String userCode; //用户Code
	private String password; //用户密码
	private String verificationCode; //验证码，如果开启了验证码验证那么必填，否则为空

	public static CPLoginModel convert() {
		return null;
	}
}
