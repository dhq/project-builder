package com.denghq.projectbuilder.component.remote.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import lombok.Data;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
public class AttachmentModel {

    private String attachmentId;
    private String name;
    private String url;
    private String extenson;
    private String sourceId;
    private String fileSize;
    private String creator;
    //@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createdTime;
    private String remark;

    public static ResultInfoModel<List<AttachmentModel>> convert(String json) {
        Type jsonType = new TypeToken<ResultInfoModel<List<AttachmentModel>>>() {
        }.getType();
        GsonBuilder builder = new GsonBuilder();
        builder.setDateFormat("yyyy-MM-dd HH:mm:ss");
        // Register an adapter to manage the date types as long values
	   /* builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
	        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
	            return new Date(json.getAsJsonPrimitive().getAsLong());
	        }
	    });*/

        Gson gson = builder.create();
        ResultInfoModel<List<AttachmentModel>> bean = gson.fromJson(json, jsonType);
        return bean;
    }

    public static ResultInfoModel<Map<String, List<AttachmentModel>>> convertMapList(String json) {
        Type jsonType = new TypeToken<ResultInfoModel<Map<String, List<AttachmentModel>>>>() {
        }.getType();
        GsonBuilder builder = new GsonBuilder();
        builder.setDateFormat("yyyy-MM-dd HH:mm:ss");
        // Register an adapter to manage the date types as long values
	   /* builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
	        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
	            return new Date(json.getAsJsonPrimitive().getAsLong());
	        }
	    });*/

        Gson gson = builder.create();
        ResultInfoModel<Map<String, List<AttachmentModel>>> bean = gson.fromJson(json, jsonType);
        return bean;
    }

    public static ResultInfoModel<Object> convertObj(String json) {
        Type jsonType = new TypeToken<ResultInfoModel<Object>>() {
        }.getType();
        ResultInfoModel<Object> bean = new Gson().fromJson(json, jsonType);
        return bean;
    }
}
