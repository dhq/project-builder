package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenCommonRoadClient;
import com.denghq.projectbuilder.component.remote.model.CommonRoadModel;
import com.denghq.projectbuilder.component.remote.service.CommonRoadService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CommonRoadServiceImpl implements CommonRoadService {

    @Autowired
    private WithTokenCommonRoadClient commonRoadClient;

    @Override
    public List<CommonRoadModel> all() {
        Map<String, Object> params = Maps.newHashMap();
        params.put("isPagination", false);
        return CloudUtils.getResult(commonRoadClient.search(params));
    }
}