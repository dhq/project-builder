package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.CommonSpottingGroupModel;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface CommonSpottingGroupService {
    //查找所有路口组
    List<CommonSpottingGroupModel> all();

    Map<String, Set<String>> spottingWithGroupMap();

}
