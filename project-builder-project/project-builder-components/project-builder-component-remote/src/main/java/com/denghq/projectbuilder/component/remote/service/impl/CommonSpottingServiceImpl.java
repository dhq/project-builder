package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenCommonSpottingClient;
import com.denghq.projectbuilder.component.remote.model.CommonSpottingModel;
import com.denghq.projectbuilder.component.remote.service.CommonSpottingService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class CommonSpottingServiceImpl implements CommonSpottingService {

    @Autowired
    private WithTokenCommonSpottingClient commonSpottingClient;

    @Override
    public List<CommonSpottingModel> all() {
        Map<String, Object> params = Maps.newHashMap();
        params.put("isPagination", false);
        return CloudUtils.getResult(commonSpottingClient.search(params));
    }

    @Override
    public List<CommonSpottingModel> getSpottingByIds(Collection<String> ids) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("spottingidlist", ids);
        List<CommonSpottingModel> spottingModels = CloudUtils.getResult(commonSpottingClient.search(params));
        log.debug("加载{}条路口记录", spottingModels.size());
        return spottingModels;
    }
}