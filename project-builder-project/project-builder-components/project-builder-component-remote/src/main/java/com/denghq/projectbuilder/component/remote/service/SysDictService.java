package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.DictModel;

import java.util.List;
import java.util.Map;

/**
 * 字典
 * 
 * @author denghq
 * 
 * @date 2018/10/29
 */
public interface SysDictService{

	@Deprecated
	List<DictModel> getDictByKind(String kind);
	
	//批量查询字典
	@Deprecated
	Map<String,List<DictModel>> getSomeDicts(List<String> kinds);

	//批量查询字典,不使用缓存
	Map<String,List<DictModel>> getSomeDictsWithOutCache(List<String> kinds);

	/**
	 * 获取公司类型
	 * @return
	 */
	@Deprecated
	List<DictModel> getCompanyType();

	/**
	 * 获取路口组类型
	 * @return
	 */
	@Deprecated
	List<DictModel> getSpottingGroupType();

	/**
	 * 获取设备组分类
	 * @return
	 */
	@Deprecated
	List<DictModel> getDeviceGroupType();

	/**
	 * 获取道路分类
	 * @return
	 */
	@Deprecated
	List<DictModel> getRoadType();
}
