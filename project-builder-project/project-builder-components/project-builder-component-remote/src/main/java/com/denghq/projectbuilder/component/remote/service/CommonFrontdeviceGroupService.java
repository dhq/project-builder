package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.CommonFrontdeviceGroupModel;

import java.util.List;

public interface CommonFrontdeviceGroupService {

    //查找所有设备组
    List<CommonFrontdeviceGroupModel> all();
}
