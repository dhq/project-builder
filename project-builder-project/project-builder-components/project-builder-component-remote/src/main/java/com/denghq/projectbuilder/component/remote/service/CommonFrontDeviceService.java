package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.CommonFrontdeviceModel;

import java.util.List;
import java.util.Map;

public interface CommonFrontDeviceService {

    //查询所有设备
    List<CommonFrontdeviceModel> simpleAll();
    //条件查询设备
    List<CommonFrontdeviceModel> searchSimple(Map<String, Object> params);
}