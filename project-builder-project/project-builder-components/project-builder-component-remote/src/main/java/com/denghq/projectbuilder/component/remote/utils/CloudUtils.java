package com.denghq.projectbuilder.component.remote.utils;

import com.denghq.projectbuilder.common.exception.CloudException;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CloudUtils {

    public static <T> T getResult(ResultInfoModel<T> infoModel){
        if(infoModel != null && infoModel.getCode() == 0){
            return infoModel.getResult();
        } else{
            log.warn("获取远程数据出错，接口返回：{}",infoModel);
            throw new CloudException(BeanTool.objectToJsonStr(infoModel));
        }
    }
}
