package com.denghq.projectbuilder.component.remote.client.withtoken;

import com.denghq.projectbuilder.component.remote.common.Constant;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.denghq.projectbuilder.component.remote.model.SysLogModel;
import com.specialconfig.FeignTokenSupportConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@FeignClient(name = Constant.SERVICE_SMW_CODE, configuration = FeignTokenSupportConfig.class)
public interface WithTokenOprLogClient {

    @PostMapping("/smw/OprLog/BatchSave")
    ResultInfoModel batchSave(List<SysLogModel> sysLogEntityList);

}
