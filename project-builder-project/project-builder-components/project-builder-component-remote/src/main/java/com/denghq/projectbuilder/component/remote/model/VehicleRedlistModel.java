package com.denghq.projectbuilder.component.remote.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 红名单
 * 
 * @author denghq
 * @email 
 * @date 2018/10/29
 */
@ApiModel("红名单")
@Data
public class VehicleRedlistModel implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 红名单主键
	 */
	@ApiModelProperty("红名单主键")
	private String vehicleid;
	/**
	 * 车牌号码
	 */
	@ApiModelProperty("车牌号码")
	private String plateno;
	/**
	 * 车牌颜色
	 */
	@ApiModelProperty("车牌颜色")
	private String platecolor;
	/**
	 * 车主
	 */
	@ApiModelProperty("车主")
	private String vehicleowner;
	/**
	 * 是否禁用
	 */
	@ApiModelProperty("是否禁用")
	private BigDecimal disabled;
	/**
	 * 有效期开始日期
	 */
	@ApiModelProperty("有效期开始日期")
	private Date effectstarttime;
	/**
	 * 有效期结束日期
	 */
	@ApiModelProperty("有效期结束日期")
	private Date effectendtime;
	/**
	 * 是否记录通行(0表示不入库,1表示入库)
	 */
	@ApiModelProperty("是否记录通行(0表示不入库,1表示入库)")
	private BigDecimal isrecord;
	/**
	 * 创建者(用户代码)
	 */
	@ApiModelProperty("创建者(用户代码)")
	private String creator;
	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	private Date createdtime;
	/**
	 * 修改时间
	 */
	@ApiModelProperty("修改时间")
	private Date modifiedtime;
	/**
	 * 备注
	 */
	@ApiModelProperty("备注")
	private String remark;

}
