package com.denghq.projectbuilder.component.remote.client;

import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.denghq.projectbuilder.component.remote.model.UserRightViewModel;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 调用用户管理服务
 */
//FeignClearTokenSupportConfig 不起作用，原因跟同name的FeignClient冲突。单独写了个配置类解决这个问题（WithOutGlobalTokenFeginClientConfiguration）。
//@FeignClient(name = Constant.SERVICE_SMW_CODE, configuration = FeignClearTokenSupportConfig.class)
public interface UserClient {

    @GetMapping("/smw/UAC/GetUserInfo")
    ResultInfoModel<UserRightViewModel> getUserInfo();

}
