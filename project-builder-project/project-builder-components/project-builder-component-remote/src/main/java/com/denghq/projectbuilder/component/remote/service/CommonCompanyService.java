package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.CommonCompanyModel;

import java.util.List;

public interface CommonCompanyService {

    //查询所有单位
    List<CommonCompanyModel> all();

}