package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.VehicleRedlistModel;

import java.util.List;

public interface VehicleRedlistService {

    //查找所有红名单
    List<VehicleRedlistModel> all();
}
