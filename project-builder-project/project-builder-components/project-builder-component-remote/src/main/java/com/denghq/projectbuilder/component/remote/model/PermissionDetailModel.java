package com.denghq.projectbuilder.component.remote.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

//权限明细
@Data
public class PermissionDetailModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2326258730752100708L;
	
	private String functionCode; //功能Code
	private List<String> actionCodeList; //功能点下的动作点List
}
