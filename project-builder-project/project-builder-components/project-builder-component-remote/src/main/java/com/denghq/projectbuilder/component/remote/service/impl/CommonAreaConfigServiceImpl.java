package com.denghq.projectbuilder.component.remote.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenCommonAreaConfigClient;
import com.denghq.projectbuilder.component.remote.model.CommonAreaConfigModel;
import com.denghq.projectbuilder.component.remote.pojo.MapInfo;
import com.denghq.projectbuilder.component.remote.service.CommonAreaConfigService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import com.google.common.collect.Maps;

@Service
public class CommonAreaConfigServiceImpl implements CommonAreaConfigService {

    @Autowired
    private WithTokenCommonAreaConfigClient commonAreaConfigClient;

    @Override
    public List<CommonAreaConfigModel> all() {
        Map<String, Object> params = Maps.newHashMap();
        params.put("isPagination", false);
        return CloudUtils.getResult(commonAreaConfigClient.search(params));
    }

	@Override
	public ConcurrentHashMap<String, List<BigDecimal[]>> allRange() {
		
		ConcurrentHashMap<String, List<BigDecimal[]>> map = new ConcurrentHashMap<String, List<BigDecimal[]>>();
		
		List<CommonAreaConfigModel> areaList = all();
		
		areaList.parallelStream().forEach(area -> {
			
			List<BigDecimal[]> poly = new ArrayList<BigDecimal[]>();
			
			if (StringUtils.isNotBlank(area.getMapInfo())) {
				
				MapInfo mapInfo = JSONObject.parseObject(area.getMapInfo(), MapInfo.class);
				mapInfo.getGeometry().getCoordinates().forEach(d -> {
					poly.addAll(d);
				});
			}
			
			map.put(area.getAreaId(), poly);
		});
		
		return map;
	}
}