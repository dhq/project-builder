package com.denghq.projectbuilder.component.remote.client.withtoken;

import java.util.List;
import java.util.Map;

import com.denghq.projectbuilder.component.remote.common.Constant;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.denghq.projectbuilder.component.remote.model.CommonAreaConfigModel;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.specialconfig.FeignTokenSupportConfig;

@FeignClient(name = Constant.SERVICE_FMW_CODE, configuration = FeignTokenSupportConfig.class)
public interface WithTokenCommonAreaConfigClient {

    @PostMapping("/fmw/commonAreaConfig/search")
    ResultInfoModel<List<CommonAreaConfigModel>> search(@RequestBody Map<String, Object> params);

}