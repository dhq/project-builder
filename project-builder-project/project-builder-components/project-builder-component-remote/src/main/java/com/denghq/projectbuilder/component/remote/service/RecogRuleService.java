package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.RecogRuleModel;

import java.util.List;

public interface RecogRuleService {

    //查询所有识别规则
    List<RecogRuleModel> all(String ruleType);

}