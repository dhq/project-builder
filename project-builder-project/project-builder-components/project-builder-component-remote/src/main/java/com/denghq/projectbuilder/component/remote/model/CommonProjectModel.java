package com.denghq.projectbuilder.component.remote.model;

import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("集成项目信息查询结果")
@Data
public class CommonProjectModel{
	
	
	/**
	 * 项目编号
	 */
	@ApiModelProperty("项目编号")
	private String projectid;
	
	/**
	 * 项目编号
	 */
	@ApiModelProperty("项目编号")
	private String projectno;
	
	/**
	 * 项目名称
	 */
	@ApiModelProperty("项目名称")
	private String projectname;
	
	/**
	 * 承建单位ID（乙方）
	 */
	@ApiModelProperty("承建单位ID（乙方）")
	private String companyid;
	
	/**
	 * 承建单位ID（乙方）
	 */
	@ApiModelProperty("承建单位名称")
	private String companyName;
	
	/**
	 * 项目经理
	 */
	@ApiModelProperty("项目经理")
	private String projectmanager;
	
	/**
	 * 合同金额
	 */
	@ApiModelProperty("合同金额")
	private BigDecimal contractamount;
	
	/**
	 * 免费质保期（单位 年）
	 */
	@ApiModelProperty("免费质保期（单位 年）")
	private BigDecimal freewarranty;
	
	/**
	 * 备注
	 */
	@ApiModelProperty("备注")
	private String remark;
	
	/**
	 * 开工时间
	 */
	@ApiModelProperty("开工时间")
	private Date startdate;
	
	/**
	 * 工期(单位 月)
	 */
	@ApiModelProperty("工期(单位 月)")
	private BigDecimal projectschedule;
	
	/**
	 * 竣工验收时间
	 */
	@ApiModelProperty("竣工验收时间")
	private Date checktime;
	
	/**
	 * 建设单位主要联系人
	 */
	@ApiModelProperty("建设单位主要联系人")
	private String owner;
	
	/**
	 * 监理单位ID
	 */
	@ApiModelProperty("监理单位ID")
	private String supervisioncompanyid;
	
	/**
	 * 监理单位名称
	 */
	@ApiModelProperty("监理单位名称")
	private String supervisioncompanyName;
	
	/**
	 * 过保日期
	 */
	@ApiModelProperty("过保日期")
	private Date afterdate;
	
	/**
	 * 创建者
	 */
	@ApiModelProperty("创建者")
	private String creator;
	
	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	private Date createdtime;
	
	/**
	 * 修改者
	 */
	@ApiModelProperty("修改者")
	private String modifier;
	
	/**
	 * 修改时间
	 */
	@ApiModelProperty("修改时间")
	private Date modifiedtime;
	
	/**
	 * 建设单位ID（甲方）
	 */
	@ApiModelProperty("建设单位ID（甲方）")
	private String departmentid;

	@ApiModelProperty("建设单位名称")
	public String departmentName;

	/**
	 * 管理单位Id
	 */
	@ApiModelProperty("管理单位Id")
	private String managecompanyid;
	
	/**
	 * 运维公司ID
	 */
	@ApiModelProperty("运维公司ID")
	private String maintenancecompanyid;
	
	/**
	 * 运维公司
	 */
	@ApiModelProperty("运维公司名称")
	private String maintenancecompanyName;
	
	/**
	 * 运维开始日期
	 */
	@ApiModelProperty("运维开始日期")
	private Date maintenancestartdate;
	
	/**
	 * 运维结束日期
	 */
	@ApiModelProperty("运维结束日期")
	private Date maintenanceenddate;
	
	/**
	 * 项目简称
	 */
	@ApiModelProperty("项目简称")
	private String abbreviation;
	
	/**
	 * 建设期数（一期、二期、三期）
	 */
	@ApiModelProperty("建设期数（一期、二期、三期）")
	private String period;
	
	/**
	 * 所在标段（一包、二包、三包、四包）
	 */
	@ApiModelProperty("所在标段（一包、二包、三包、四包）")
	private String section;
	
	/**
	 * 招标采购编号
	 */
	@ApiModelProperty("招标采购编号")
	private String procurementno;
	
	/**
	 * 初次维保结束日期
	 */
	@ApiModelProperty("初次维保结束日期")
	private Date firstmaintenanceend;
	
	/**
	 * 主要产品供应商ID列信息（引用公司信息主键, 多个用逗号隔开）
	 */
	@ApiModelProperty("主要产品供应商ID列信息（引用公司信息主键, 多个用逗号隔开）")
	private String mainsupplierlist;
	
	/**
	 * 归属大项目编号 字典项
	 */
	@ApiModelProperty("归属大项目编号 字典项")
	private String underproject;
	
	/**
	 * 项目类型 字典项
	 */
	@ApiModelProperty("项目类型 字典项")
	private String projecttype;
	
	/**
	 * 父项目ID
	 */
	@ApiModelProperty("父项目ID")
	private String parentprojectid;
	
	/**
	 * 施工单位ID列信息（引用公司信息主键, 多个用逗号隔开）
	 */
	@ApiModelProperty("施工单位ID列信息（引用公司信息主键, 多个用逗号隔开）")
	private String constructionunitlist;
	
	/**
	 * 拼音
	 */
	@ApiModelProperty("拼音")
	private String bopomofo;

	/**
	 * 所在标段名称
	 */
	@ApiModelProperty("所在标段名称")
	private String sectionname;

	/**
	 * 建设期数名称
	 */
	@ApiModelProperty("建设期数名称")
	private String periodname;
}
