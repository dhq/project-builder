package com.denghq.projectbuilder.component.remote.client.withtoken;

import com.denghq.projectbuilder.component.remote.common.Constant;
import com.denghq.projectbuilder.component.remote.model.DepartmentModel;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.specialconfig.FeignTokenSupportConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = Constant.SERVICE_SMW_CODE, configuration = FeignTokenSupportConfig.class)
public interface WithTokenDepartmentClient {

    @GetMapping("/smw/Department/FindAll")
    ResultInfoModel<List<DepartmentModel>> findAll();
}
