package com.denghq.projectbuilder.component.remote.client.withtoken;

import com.denghq.projectbuilder.component.remote.common.Constant;
import com.denghq.projectbuilder.component.remote.model.DictModel;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.specialconfig.FeignTokenSupportConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * 调用字典管理服务
 */
@FeignClient(name = Constant.SERVICE_SMW_CODE, configuration = FeignTokenSupportConfig.class)
public interface WithTokenDictClient {

    @GetMapping("/smw/Dict/GetDictByKind")
    ResultInfoModel<List<DictModel>> getDictByKind(@RequestParam("kind") String kind);

    @GetMapping("/smw/Dict/GetSomeDicts")
    ResultInfoModel<Map<String, List<DictModel>>> getSomeDicts(@RequestBody List<String> kinds);

}
