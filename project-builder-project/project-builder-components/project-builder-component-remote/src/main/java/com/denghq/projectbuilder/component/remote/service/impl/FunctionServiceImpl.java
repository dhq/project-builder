package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenFunctionClient;
import com.denghq.projectbuilder.component.remote.model.FunctionModel;
import com.denghq.projectbuilder.component.remote.service.FunctionService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FunctionServiceImpl implements FunctionService {

    @Autowired
    private WithTokenFunctionClient functionClient;

    @Override
    public List<FunctionModel> all() {
        return CloudUtils.getResult(functionClient.findAll());
    }
}
