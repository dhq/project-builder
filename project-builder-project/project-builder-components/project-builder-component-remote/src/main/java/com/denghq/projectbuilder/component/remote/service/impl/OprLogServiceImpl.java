package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenOprLogClient;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.denghq.projectbuilder.component.remote.model.SysLogModel;
import com.denghq.projectbuilder.component.remote.service.OprLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OprLogServiceImpl implements OprLogService {

    @Autowired
    private WithTokenOprLogClient oprLogClient;

    @Override
    public boolean batchSave(List<SysLogModel> sysLogEntityList) {
        final ResultInfoModel resultInfoModel = oprLogClient.batchSave(sysLogEntityList);
        return resultInfoModel.getCode() != null && resultInfoModel.getCode() == 0;
    }
}
