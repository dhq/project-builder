package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenCommonStaffClient;
import com.denghq.projectbuilder.component.remote.model.CommonStaffModel;
import com.denghq.projectbuilder.component.remote.service.CommonStaffService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CommonStaffServiceImpl implements CommonStaffService {

    @Autowired
    private WithTokenCommonStaffClient commonStaffClient;

    @Override
    public List<CommonStaffModel> all() {
        Map<String, Object> params = Maps.newHashMap();
        params.put("isPagination", false);
        return CloudUtils.getResult(commonStaffClient.search(params));
    }
}