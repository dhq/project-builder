package com.denghq.projectbuilder.component.remote.client;

import java.util.Map;

import com.denghq.projectbuilder.component.remote.common.Constant;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;

/**
 * 调用配置管理服务
 */
@FeignClient(Constant.SERVICE_COMMON_CODE)
public interface ConfigClient{

    @GetMapping("/common/ConfigOptions/GetConfigByCategoryNo")
    ResultInfoModel<Map<String,Object>> getConfigByCategoryNo(@RequestParam("categoryNo") String categoryNo);

}
