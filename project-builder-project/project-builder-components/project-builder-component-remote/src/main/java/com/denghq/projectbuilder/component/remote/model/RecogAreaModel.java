package com.denghq.projectbuilder.component.remote.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:识别区域实体类
 * @ClassName: RecognitionArea
 * @author: denghq
 * @date: 2019-8-16 下午5:25:36
 */
@Data
public class RecogAreaModel implements Serializable {

    private static final long serialVersionUID = -2524343117858485854L;

    private String assetsNo;
    private String laneNo;
    private Float pointY;
    private Float pointX;
    private Float width;
    private Float height;
}
