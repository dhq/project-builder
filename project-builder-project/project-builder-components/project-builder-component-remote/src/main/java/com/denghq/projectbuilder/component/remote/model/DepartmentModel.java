package com.denghq.projectbuilder.component.remote.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.Data;

import java.lang.reflect.Type;
import java.util.List;

@Data
public class DepartmentModel {
	private String parentBuName;

    private String areaName;

    private String departmentId;

    private String buName;

    private String buFullName;

    private String buCode;

    private String parentId;

    private boolean isEnd;

    private int buLevel;

    private String areaCode;

    private String remark;

    private String hierarchyCode;

    private String orderCode;

    private String orderHierarchyCode;

    private String creator;

    private String createdTime;

    private String modifier;

    private String modifiedTime;
    
    private List<UserModel> userList;

	public static ResultInfoModel<List<DepartmentModel>> convert(String json) {
		Type jsonType = new TypeToken<ResultInfoModel<List<DepartmentModel>>>() {}.getType();
		ResultInfoModel<List<DepartmentModel>> bean = new Gson().fromJson(json, jsonType);
		return bean;
	}

    public static ResultInfoModel<List<String>> convertLis(String json) {
        Type jsonType = new TypeToken<ResultInfoModel<List<String>>>() {}.getType();
        ResultInfoModel<List<String>> bean = new Gson().fromJson(json, jsonType);
        return bean;
    }
}
