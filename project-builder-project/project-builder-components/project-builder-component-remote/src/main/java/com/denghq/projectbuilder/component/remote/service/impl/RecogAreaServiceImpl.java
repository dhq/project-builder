package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenRecogAreaClient;
import com.denghq.projectbuilder.component.remote.model.RecogAreaModel;
import com.denghq.projectbuilder.component.remote.service.RecogAreaService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecogAreaServiceImpl implements RecogAreaService {

    @Autowired
    private WithTokenRecogAreaClient recogAreaClient;

    @Override
    public List<RecogAreaModel> all() {
        return CloudUtils.getResult(recogAreaClient.getArea());
    }
}