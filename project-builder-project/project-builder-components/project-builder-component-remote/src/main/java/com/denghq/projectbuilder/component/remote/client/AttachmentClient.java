package com.denghq.projectbuilder.component.remote.client;

import java.util.List;
import java.util.Map;

import com.denghq.projectbuilder.component.remote.common.Constant;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.denghq.projectbuilder.component.remote.model.AttachmentModel;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;

/**
 * 调用附件管理服务
 */
@FeignClient(Constant.SERVICE_SMW_CODE)
public interface AttachmentClient {

    @PostMapping("/common/Attachment/UpdateSourceId")
    ResultInfoModel<Integer> updateSourceId(@RequestParam("sourceId") String sourceId,@RequestBody List<String> attachmentIds);

    @PostMapping("/common/Attachment/UpdateOrRemove")
    ResultInfoModel<Integer> updateOrRemove(@RequestParam("sourceId") String sourceId,@RequestBody List<String> attachmentIds);

    @PostMapping("/common/Attachment/BatchUpdateOrRemove")
    ResultInfoModel<Integer> batchUpdateOrRemove(@RequestBody Map<String, Object> sourceAttachmentIds);

    @GetMapping("/common/Attachment/FindBySourceId")
    ResultInfoModel<List<AttachmentModel>> findBySourceId(@RequestParam("sourceId") String sourceId);

    @PostMapping("/common/Attachment/FindBySourceId")
    ResultInfoModel<Map<String,List<AttachmentModel>>> findBySourceId(@RequestBody List<String> sourceIds);

    @PostMapping("/common/Attachment/DeleteBySourceIds")
    ResultInfoModel<Integer> deleteBySourceIds(@RequestBody List<String> sourceIds);
}
