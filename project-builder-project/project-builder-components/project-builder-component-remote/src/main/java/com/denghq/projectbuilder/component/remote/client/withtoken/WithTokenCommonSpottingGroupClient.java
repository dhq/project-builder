package com.denghq.projectbuilder.component.remote.client.withtoken;

import com.denghq.projectbuilder.component.remote.common.Constant;
import com.denghq.projectbuilder.component.remote.model.CommonSpottingGroupModel;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.specialconfig.FeignTokenSupportConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Map;
import java.util.Set;

@FeignClient(name = Constant.SERVICE_FMW_CODE, configuration = FeignTokenSupportConfig.class)
public interface WithTokenCommonSpottingGroupClient {

    @PostMapping("/fmw/commonSpottingGroup/search")
    ResultInfoModel<List<CommonSpottingGroupModel>> search(Map<String, Object> params);

    @GetMapping("/fmw/commonSpottingGroup/allWithSpottingGroupMap")
    ResultInfoModel<Map<String, Set<String>>> allWithSpottingGroupMap();
}
