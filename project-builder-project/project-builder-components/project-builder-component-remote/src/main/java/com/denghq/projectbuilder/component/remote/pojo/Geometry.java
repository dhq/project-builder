package com.denghq.projectbuilder.component.remote.pojo;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;

@Data
public class Geometry {

	private List<List<BigDecimal[]>> coordinates;
	
	private String type;
	
}
