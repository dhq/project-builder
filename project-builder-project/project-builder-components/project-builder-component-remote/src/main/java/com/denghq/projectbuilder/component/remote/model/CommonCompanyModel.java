package com.denghq.projectbuilder.component.remote.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

//@ApiModel("运维公司基本信息信息查询结果")
@Data
public class CommonCompanyModel {


    /**
     * 公司ID
     */
    @ApiModelProperty("公司ID")
    private String companyid;

    /**
     * 公司全称
     */
    @ApiModelProperty("公司全称")
    private String name;

    /**
     * 公司地址
     */
    @ApiModelProperty("公司地址")
    private String address;

    /**
     * 公司电话
     */
    @ApiModelProperty("公司电话")
    private String phone;

    /**
     * 简况
     */
    @ApiModelProperty("简况")
    private String profile;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;

    /**
     * 拼音
     */
    @ApiModelProperty("拼音")
    private String bopomofo;

    /**
     * 公司类型 字典项，多个逗号分隔
     */
    @ApiModelProperty("公司类型 字典项，多个逗号分隔")
    private String companytype;

    /**
     * 公司简称
     */
    @ApiModelProperty("公司简称")
    private String shortname;

    /**
     * 创建者
     */
    @ApiModelProperty("创建者")
    private String creator;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private Date createdtime;

    /**
     * 修改者
     */
    @ApiModelProperty("修改者")
    private String modifier;

    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    private Date modifiedtime;

    /**
     * 关联系统用户code，多个逗号分隔
     */
    @ApiModelProperty("关联系统用户code，多个逗号分隔")
    private String userlist;

    /**
     * 关联人员编号，多个逗号分隔
     */
    @ApiModelProperty("关联人员编号，多个逗号分隔")
    private String personlist;

    /**
     * 公司类型名称
     */
    @ApiModelProperty("公司类型名称")
    private String companytypename;
}
