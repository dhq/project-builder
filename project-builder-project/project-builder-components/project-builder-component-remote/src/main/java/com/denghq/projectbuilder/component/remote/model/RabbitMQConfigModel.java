package com.denghq.projectbuilder.component.remote.model;

import lombok.Data;

@Data
public class RabbitMQConfigModel {
    private Integer port;
    private String host;
    private String userName;
    private String password;
    private String exchangeName;
    private Boolean durable;
}
