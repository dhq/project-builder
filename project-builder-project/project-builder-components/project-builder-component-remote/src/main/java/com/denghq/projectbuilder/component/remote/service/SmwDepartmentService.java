package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.DepartmentModel;

import java.util.List;

public interface SmwDepartmentService {

    /**
     * 查询所有部门
     * @return
     */
    List<DepartmentModel> all();
}
