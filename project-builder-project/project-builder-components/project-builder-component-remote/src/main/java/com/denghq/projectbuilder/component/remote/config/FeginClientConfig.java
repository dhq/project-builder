package com.denghq.projectbuilder.component.remote.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.denghq.projectbuilder.common.Constant;
import com.denghq.projectbuilder.component.remote.utils.UserContextHolder;

import feign.Logger;
import feign.RequestInterceptor;

@Configuration
public class FeginClientConfig {
    @Bean
    public RequestInterceptor headerInterceptor() {
        return requestTemplate -> {

            String authorization = UserContextHolder.getAuthorization();
            if(StringUtils.isNotBlank(authorization)){
                requestTemplate.header(Constant.SERVICE_TOKEN_NAME, authorization);
            }

        };
    }
 
    //@Bean
    public Logger.Level level() {
        return Logger.Level.FULL;
    }
 
}