package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.CommonSpottingModel;

import java.util.Collection;
import java.util.List;

public interface CommonSpottingService {

    //查询所有路口
    List<CommonSpottingModel> all();

    //通过路口id查询所有路口
    List<CommonSpottingModel> getSpottingByIds(Collection<String> ids);
}