package com.denghq.projectbuilder.component.remote.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.denghq.projectbuilder.component.remote.model.CommonAreaConfigModel;

public interface CommonAreaConfigService {

    //查询所有区域配置
    List<CommonAreaConfigModel> all();

    //查询所有区域配置的范围<区域id,所有点>
    ConcurrentHashMap<String,List<BigDecimal[]>> allRange();
}