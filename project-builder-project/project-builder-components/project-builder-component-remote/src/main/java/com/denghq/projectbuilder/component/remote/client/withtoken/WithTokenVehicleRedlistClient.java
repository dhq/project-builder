package com.denghq.projectbuilder.component.remote.client.withtoken;

import com.denghq.projectbuilder.component.remote.common.Constant;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.denghq.projectbuilder.component.remote.model.VehicleRedlistModel;
import com.specialconfig.FeignTokenSupportConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;


@FeignClient(name= Constant.SERVICE_TMW_CODE,configuration = FeignTokenSupportConfig.class)
public interface WithTokenVehicleRedlistClient {

    @PostMapping("/tmw/vehicle/vehicleRedlist/search")
    ResultInfoModel<List<VehicleRedlistModel>> search(@RequestBody Map<String, Object> params);

}
