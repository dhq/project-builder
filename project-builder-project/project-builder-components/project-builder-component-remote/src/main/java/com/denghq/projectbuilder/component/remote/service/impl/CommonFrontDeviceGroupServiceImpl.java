package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenCommonFrontDeviceGroupClient;
import com.denghq.projectbuilder.component.remote.model.CommonFrontdeviceGroupModel;
import com.denghq.projectbuilder.component.remote.service.CommonFrontdeviceGroupService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CommonFrontDeviceGroupServiceImpl implements CommonFrontdeviceGroupService {

    @Autowired
    private WithTokenCommonFrontDeviceGroupClient commonFrontDeviceGroupClient;

    @Override
    public List<CommonFrontdeviceGroupModel> all() {
        Map<String, Object> params = Maps.newHashMap();
        params.put("isPagination",false);
        return CloudUtils.getResult(commonFrontDeviceGroupClient.search(params));
    }
}
