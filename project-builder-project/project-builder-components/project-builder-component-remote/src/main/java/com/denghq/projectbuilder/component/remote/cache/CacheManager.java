package com.denghq.projectbuilder.component.remote.cache;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.denghq.projectbuilder.component.remote.model.DictModel;
import com.denghq.projectbuilder.component.remote.model.UserRightViewModel;
import org.springframework.util.CollectionUtils;

import com.denghq.projectbuilder.common.cache.LocalCache;
import com.google.common.collect.Maps;

public class CacheManager {

    private static String USER_INFO_CACHE_KEY = "Authorization";

    private static int USER_INFO_EXPIRE_TIME = 5;// 用户信息缓存时间，单位：秒

    private static String DICT_DATA_CACHE_KEY = "DICT_DATA";

    private static int DICT_DATA_EXPIRE_TIME = 60 * 60;// 字典信息缓存时间，单位：秒

    private static LocalCache cache =  LocalCache.getInstance();

    /**
     * 通过 Authorization 获取用户信息
     * @param authorization
     * @return
     */
    public static UserRightViewModel getUserInfo(String authorization){

        return  (UserRightViewModel) cache.getValue(USER_INFO_CACHE_KEY + authorization);
    }

    /**
     * 通过 Authorization 缓存用户信息
     * @param authorization
     * @return
     */
    public static void saveUserInfo(String authorization,UserRightViewModel userInfo) {

        cache.putValue(USER_INFO_CACHE_KEY + authorization, userInfo, USER_INFO_EXPIRE_TIME);
    }


    public static List<DictModel> getDictByKind(String kind) {
        Map<String, List<DictModel>> someDicts = getSomeDicts(Arrays.asList(kind));
        if(someDicts!=null){
            return someDicts.get(kind);
        }
        return null;
    }

    public static void saveDictByKind(String kind, List<DictModel> dictData) {
        Map<String,List<DictModel>> dictDataMap = Maps.newHashMap();
        dictDataMap.put(kind,dictData);
        saveSomeDicts(dictDataMap);
    }

    @SuppressWarnings("unchecked")
	public static Map<String, List<DictModel>> getSomeDicts(List<String> kinds) {

        Map<String, List<DictModel>> listMap = (Map<String, List<DictModel>>) cache.getValue(DICT_DATA_CACHE_KEY);

        if(CollectionUtils.isEmpty(listMap)||CollectionUtils.isEmpty(kinds)){
            return null;
        }

        Map<String,List<DictModel>> res = new HashMap<>();

        for(String kind:kinds){
            List<DictModel> dictModels = listMap.get(kind);
            if(CollectionUtils.isEmpty(dictModels)){
                return null;
            }else {
                res.put(kind,dictModels);
            }
        }

        return res;
    }
    
    @SuppressWarnings("unchecked")
    public static void saveSomeDicts(Map<String, List<DictModel>> dictData) {

        if(!CollectionUtils.isEmpty(dictData)){
            
			Map<String, List<DictModel>> listMap = (Map<String, List<DictModel>>) cache.getValue(DICT_DATA_CACHE_KEY);
            if(listMap == null){
                listMap = new ConcurrentHashMap<>();
                cache.putValue(DICT_DATA_CACHE_KEY,listMap,DICT_DATA_EXPIRE_TIME);
            }
            for(String kind:dictData.keySet()){
                listMap.put(kind,dictData.get(kind));
            }
        }
    }
}
