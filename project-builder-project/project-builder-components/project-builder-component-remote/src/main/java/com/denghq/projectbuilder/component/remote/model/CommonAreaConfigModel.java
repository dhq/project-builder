package com.denghq.projectbuilder.component.remote.model;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 自定义区域信息
 * 
 * @author denghq
 * @email 
 * @date 2018/10/29
 */
@ApiModel("自定义区域信息")
@Data
public class CommonAreaConfigModel implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
	@ApiModelProperty("主键ID")
	private String areaId;
	
	/**
	 * 区域名称
	 */
	@ApiModelProperty("区域名称")
	private String areaName;
	
	/**
	 * 地图的点信息
	 */
	@ApiModelProperty("地图的点信息")
	private String mapInfo;
	
	/**
	 * 区域类型 区域类型在字典表中
	 */
	@ApiModelProperty("区域类型 区域类型在字典表中")
	private String areaType;
	
	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	private Date crEatETime;
	
	/**
	 * 创建人
	 */
	@ApiModelProperty("创建人")
	private String creator;
	
	/**
	 * 修改人
	 */
	@ApiModelProperty("修改人")
	private String modifier;
	
	/**
	 * 修改时间
	 */
	@ApiModelProperty("修改时间")
	private Date modifiedTime;
}