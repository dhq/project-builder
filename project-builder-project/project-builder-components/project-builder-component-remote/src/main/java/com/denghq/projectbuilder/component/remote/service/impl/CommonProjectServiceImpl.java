package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenCommonProjectClient;
import com.denghq.projectbuilder.component.remote.model.CommonProjectModel;
import com.denghq.projectbuilder.component.remote.service.CommonProjectService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CommonProjectServiceImpl implements CommonProjectService {

    @Autowired
    private WithTokenCommonProjectClient commonProjectClient;

    @Override
    public List<CommonProjectModel> all() {
        Map<String, Object> params = Maps.newHashMap();
        params.put("isPagination", false);
        return CloudUtils.getResult(commonProjectClient.search(params));
    }
}