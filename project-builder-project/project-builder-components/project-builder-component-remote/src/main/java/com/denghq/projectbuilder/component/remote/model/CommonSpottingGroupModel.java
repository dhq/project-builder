package com.denghq.projectbuilder.component.remote.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 路口分组信息
 * 
 * @author denghq
 * @email 
 * @date 2018/10/29
 */
@ApiModel("路口分组信息")
@Data
public class CommonSpottingGroupModel implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 路口分组ID-主键
	 */
	@ApiModelProperty("路口分组ID-主键")
	private String spottinggroupid;
	/**
	 * 路口组名称
	 */
	@ApiModelProperty("路口组名称")
	private String name;
	/**
	 * 路口组类型
	 */
	@ApiModelProperty("路口组类型")
	private String grouptype;
	/**
	 * 描述
	 */
	@ApiModelProperty("描述")
	private String remark;
	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	private Date createdtime;
	/**
	 * 创建人
	 */
	@ApiModelProperty("创建人")
	private String creator;
	/**
	 * 修改时间
	 */
	@ApiModelProperty("修改时间")
	private Date modifiedtime;
	/**
	 * 修改人
	 */
	@ApiModelProperty("修改人")
	private String modifier;


}
