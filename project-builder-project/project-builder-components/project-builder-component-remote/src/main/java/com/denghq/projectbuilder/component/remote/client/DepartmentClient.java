package com.denghq.projectbuilder.component.remote.client;

import java.util.List;

import com.denghq.projectbuilder.component.remote.common.Constant;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.denghq.projectbuilder.component.remote.model.DepartmentModel;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;

/**
 * 调用部门管理服务
 */
@FeignClient(Constant.SERVICE_SMW_CODE)
public interface DepartmentClient {

    @GetMapping("/smw/Department/FindAll")
    ResultInfoModel<List<DepartmentModel>> findAll();

    @PostMapping("/smw/Department/GetSomeDepartment")
    ResultInfoModel<List<DepartmentModel>> getSomeDepartment(@RequestBody List<String> departmentIds);

    @GetMapping("/smw/Department/GetPermissonDepartmentByFACode")
    ResultInfoModel<List<DepartmentModel>> getPermissonDepartmentByFACode(@RequestParam("functionCode") String functionCode,@RequestParam("actionCode") String actionCode);

    @GetMapping("/smw/Department/QueryChildDepartmentId")
    ResultInfoModel<List<String>> queryChildDepartmentId(@RequestParam("departmentId") String departmentId);
}
