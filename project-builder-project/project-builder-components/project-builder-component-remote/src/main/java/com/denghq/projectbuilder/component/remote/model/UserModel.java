package com.denghq.projectbuilder.component.remote.model;

import lombok.Data;

@Data
public class UserModel {

	private String id;

    private String userGUID;

    private String userCode;

    private String userName;

    private String buName;

    private String departmentId;

    private boolean isDisabeld;

}
