package com.denghq.projectbuilder.component.remote.client;


import java.util.List;

import com.denghq.projectbuilder.component.remote.common.Constant;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.denghq.projectbuilder.component.remote.model.UserModel;

/**
 * 调用账号管理服务
 */
@FeignClient(Constant.SERVICE_SMW_CODE)
public interface AccountClient {

    @PostMapping("/smw/Account/GetSomeAccountByCode")
    ResultInfoModel<List<UserModel>> getSomeAccountByCode(@RequestBody List<String> userCodeList);
}
