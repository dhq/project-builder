package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.client.ConfigClient;
import com.denghq.projectbuilder.component.remote.config.properties.SysConfProp;
import com.denghq.projectbuilder.component.remote.model.RabbitMQConfigModel;
import com.denghq.projectbuilder.component.remote.model.ResultInfoModel;
import com.denghq.projectbuilder.component.remote.service.ConfigService;
import com.denghq.projectbuilder.component.remote.utils.BeanTool;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ConfigServiceImpl implements ConfigService {

    @Autowired
    private SysConfProp sysConfProp;

    @Autowired
    private ConfigClient configClient;

    @Override
    public Map<String, Object> getConfigByCategoryNo(String categoryNo) {
        ResultInfoModel<Map<String, Object>> infoModel = configClient.getConfigByCategoryNo(categoryNo);
        return CloudUtils.getResult(infoModel);
    }

    @Override
    public RabbitMQConfigModel getRabbitMQConfig() {
        Map<String, Object> config = getConfigByCategoryNo(sysConfProp.getRabbitMQConfigcategoryNo());
        return BeanTool.mapToObject(config,RabbitMQConfigModel.class);
    }
}
