package com.denghq.projectbuilder.component.remote.model;

import lombok.Data;

@Data
public class AppModel {
    private String appId;
    private String appNo;
    private String appName;
    private String appEnName;
    private String category;
    private String funcGroup;
    private String isDisabled;
    private String sort;
    private String remark;
    private String logoUrl;
    private String indexUrl;
    private String authMode;
    private String authExt;
}
