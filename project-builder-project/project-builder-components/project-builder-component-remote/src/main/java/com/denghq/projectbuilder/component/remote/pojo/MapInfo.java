package com.denghq.projectbuilder.component.remote.pojo;

import java.util.Map;

import lombok.Data;

@Data
public class MapInfo {

	private String id;
	
	private String type;
	
	private Map properties;
	
	private Geometry geometry;
	
}
