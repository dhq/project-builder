package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.SysLogModel;

import java.util.List;

public interface OprLogService {

    boolean batchSave(List<SysLogModel> sysLogEntityList);
}
