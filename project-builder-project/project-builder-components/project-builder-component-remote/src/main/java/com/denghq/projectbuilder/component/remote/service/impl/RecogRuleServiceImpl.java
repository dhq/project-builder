package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenRecogRuleClient;
import com.denghq.projectbuilder.component.remote.model.RecogRuleModel;
import com.denghq.projectbuilder.component.remote.service.RecogRuleService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecogRuleServiceImpl implements RecogRuleService {

    @Autowired
    private WithTokenRecogRuleClient recogRuleClient;

    @Override
    public List<RecogRuleModel> all(String ruleType) {
        return CloudUtils.getResult(recogRuleClient.getRule(ruleType)).get(ruleType);
    }
}