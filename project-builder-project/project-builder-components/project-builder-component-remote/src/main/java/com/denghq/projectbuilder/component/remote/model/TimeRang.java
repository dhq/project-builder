package com.denghq.projectbuilder.component.remote.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 时间范围实体类
 * @ClassName: TimeRang
 * @author: denghq
 * @date: 2019-8-16 下午5:21:36
 */
@Data
public class TimeRang implements Serializable {
    private static final long serialVersionUID = 2308819588440891589L;

    private String startTime;

    private String endTime;

}
