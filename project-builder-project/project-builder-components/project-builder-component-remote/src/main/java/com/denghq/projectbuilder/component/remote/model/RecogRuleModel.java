package com.denghq.projectbuilder.component.remote.model;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 识别范围的实体类
 * @ClassName:  RecogRuleModel
 * @author:  denghq
 * @date:   2019-8-16 下午 5:29:57
 */
@Data
public class RecogRuleModel implements Serializable{

	private static final long serialVersionUID = -5128346565662227119L;
	
	private String assetsId;
	
	private String assetsNo;
	
	private String assetsName;
	
	private List<TimeRang> timeRangs;

}
