package com.denghq.projectbuilder.component.remote.model;

import lombok.Data;

@Data
public class ResultInfoModel<T> {
	
	//api 状态码
    private Integer code;
    
    //状态码说明
    private String codeRemark;
    
    //业务信息
    private String message;
    
    //数据信息
    private T result;
    
    //总记录数
    private Integer totalCount;
    
    //总页数
    private Integer totalPage;
    
}
