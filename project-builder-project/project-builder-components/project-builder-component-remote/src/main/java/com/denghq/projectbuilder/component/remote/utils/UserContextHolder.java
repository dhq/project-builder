package com.denghq.projectbuilder.component.remote.utils;

import com.denghq.projectbuilder.common.Constant;
import com.denghq.projectbuilder.common.util.SpringContextUtils;
import com.denghq.projectbuilder.component.remote.model.UserRightViewModel;
import com.denghq.projectbuilder.component.remote.service.SysUserService;

import javax.servlet.http.HttpServletRequest;

public class UserContextHolder {
	
	public static String getUserId() {
		
		UserRightViewModel userInfo = getUserInfo();
		if(userInfo != null){
			return userInfo.getUserCode();
		}else{
			return null;
		}
	}
	
	
	public static UserRightViewModel getUserInfo(){

		SysUserService sysUserService = SpringContextUtils.getBean(SysUserService.class);
		return sysUserService.getUserInfo();
		
	}
	
	public static String getAuthorization(){
		HttpServletRequest req = HttpContextUtils.getHttpServletRequest();
		if(req!=null){
			return req.getHeader(Constant.SERVICE_TOKEN_NAME);
		}else {
			return null;
		}

	}
	
}
