package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenCommonFrontDeviceClient;
import com.denghq.projectbuilder.component.remote.model.CommonFrontdeviceModel;
import com.denghq.projectbuilder.component.remote.service.CommonFrontDeviceService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CommonFrontDeviceServiceImpl implements CommonFrontDeviceService {

    @Autowired
    private WithTokenCommonFrontDeviceClient commonFrontDeviceClient;

    @Override
    public List<CommonFrontdeviceModel> simpleAll() {
        Map<String, Object> params = Maps.newHashMap();
        params.put("isPagination",false);
        return CloudUtils.getResult(commonFrontDeviceClient.searchSimple(params));
    }

    @Override
    public List<CommonFrontdeviceModel> searchSimple(Map<String, Object> params) {
        return CloudUtils.getResult(commonFrontDeviceClient.searchSimple(params));
    }
}