package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.CommonStaffModel;

import java.util.List;

public interface CommonStaffService {

    //查询所有人员
    List<CommonStaffModel> all();

}