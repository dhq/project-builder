package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenVehicleRedlistClient;
import com.denghq.projectbuilder.component.remote.model.VehicleRedlistModel;
import com.denghq.projectbuilder.component.remote.service.VehicleRedlistService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class VehicleRedlistServiceImpl implements VehicleRedlistService {

    @Autowired
    private WithTokenVehicleRedlistClient withTokenVehicleRedlistClient;

    @Override
    public List<VehicleRedlistModel> all() {
        Map<String, Object> params = Maps.newHashMap();
        params.put("isPagination", false);
        return CloudUtils.getResult(withTokenVehicleRedlistClient.search(params));
    }
}
