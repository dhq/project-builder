package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.CommonRoadModel;

import java.util.List;

public interface CommonRoadService {

    //查询所有道路
    List<CommonRoadModel> all();

}