package com.denghq.projectbuilder.component.remote.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.Data;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

///登录成功结果
@Data
public class UserRightViewModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6473983616746276537L;
	
	private String userGUID; //当前登录用户GUID
	private String userCode; //当前登录用户Code
	private String userName	;//当前登录用户名称
	private Boolean	isAdmin; //是否超级管理员
	private String departmentId ;//当前登录用户部门GUID
	private String departmentName ;//当前登录用户部门名称
	private List<PermissionDetailModel> permissionList; //权限明细
	
	public static ResultInfoModel<UserRightViewModel> convert(String json){
		Type jsonType = new TypeToken<ResultInfoModel<UserRightViewModel>>() {}.getType();
		ResultInfoModel<UserRightViewModel> bean = new Gson().fromJson(json, jsonType);
		return bean;
	} 
	

}
