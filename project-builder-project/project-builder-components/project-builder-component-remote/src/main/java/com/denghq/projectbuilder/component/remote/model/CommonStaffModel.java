package com.denghq.projectbuilder.component.remote.model;

import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("人员信息查询结果")
@Data
public class CommonStaffModel{
	
	
	/**
	 * 人员ID,主键
	 */
	@ApiModelProperty("人员ID,主键")
	private String staffid;
	
	/**
	 * 人员姓名
	 */
	@ApiModelProperty("人员姓名")
	private String staffname;
	
	/**
	 * 人员编号
	 */
	@ApiModelProperty("人员编号")
	private String staffno;
	
	/**
	 * 人员类型 字典项
	 */
	@ApiModelProperty("人员类型 字典项")
	private String stafftype;
	
	/**
	 * 性别 字典项
	 */
	@ApiModelProperty("性别 字典项")
	private String gender;
	
	/**
	 * 出生日期
	 */
	@ApiModelProperty("出生日期")
	private Date birthday;
	
	/**
	 * 身份证号码
	 */
	@ApiModelProperty("身份证号码")
	private String sid;
	
	/**
	 * 联系方式 多个逗号分隔
	 */
	@ApiModelProperty("联系方式 多个逗号分隔")
	private String phone;
	
	/**
	 * 所属单位
	 */
	@ApiModelProperty("所属单位")
	private String departmentid;
	
	/**
	 * 工作年限
	 */
	@ApiModelProperty("工作年限")
	private BigDecimal workyear;
	
	/**
	 * 驾驶技能 字典项
	 */
	@ApiModelProperty("驾驶技能 字典项")
	private String drivingskill;
	
	/**
	 * 家庭住址
	 */
	@ApiModelProperty("家庭住址")
	private String homeaddress;
	
	/**
	 * 微信号
	 */
	@ApiModelProperty("微信号")
	private String weixinno;
	
	/**
	 * 学历 字典项
	 */
	@ApiModelProperty("学历 字典项")
	private String education;
	
	/**
	 * 关联系统账户CODE，多个逗号分隔
	 */
	@ApiModelProperty("关联系统账户CODE，多个逗号分隔")
	private String sysusercode;
	
	/**
	 * 职务
	 */
	@ApiModelProperty("职务")
	private String duty;
	
	/**
	 * 人员级别 字典项
	 */
	@ApiModelProperty("人员级别 字典项")
	private String staffrank;
	
	/**
	 * 创建者
	 */
	@ApiModelProperty("创建者")
	private String creator;
	
	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	private Date createdtime;
	
	/**
	 * 修改者
	 */
	@ApiModelProperty("修改者")
	private String modifier;
	
	/**
	 * 修改时间
	 */
	@ApiModelProperty("修改时间")
	private Date modifiedtime;

	/**
	 * 驾驶技能名称
	 */
	@ApiModelProperty("驾驶技能名称")
	private String drivingskillname;

	/**
	 * 学历名称
	 */
	@ApiModelProperty("学历名称")
	private String educationname;

	/**
	 * 性别名称
	 */
	@ApiModelProperty("性别名称")
	private String gendername;

	/**
	 * 人员级别 名称
	 */
	@ApiModelProperty("人员级别 名称")
	private String staffrankname;

	/**
	 * 人员类型名称
	 */
	@ApiModelProperty("人员类型名称")
	private String stafftypename;
	
	/**
	 * 所属单位
	 */
	@ApiModelProperty("所属单位名称")
	private String departmentname;
}
