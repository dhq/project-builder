package com.denghq.projectbuilder.component.remote.service.impl;

import com.denghq.projectbuilder.component.remote.client.withtoken.WithTokenCommonSpottingGroupClient;
import com.denghq.projectbuilder.component.remote.model.CommonSpottingGroupModel;
import com.denghq.projectbuilder.component.remote.service.CommonSpottingGroupService;
import com.denghq.projectbuilder.component.remote.utils.CloudUtils;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class CommonSpottingGroupServiceImpl implements CommonSpottingGroupService {

    @Autowired
    private WithTokenCommonSpottingGroupClient commonSpottingGroupClient;

    @Override
    public List<CommonSpottingGroupModel> all() {
        Map<String, Object> params = Maps.newHashMap();
        params.put("isPagination", false);
        return CloudUtils.getResult(commonSpottingGroupClient.search(params));
    }

    @Override
    public Map<String, Set<String>> spottingWithGroupMap() {
        return CloudUtils.getResult(commonSpottingGroupClient.allWithSpottingGroupMap());
    }


}
