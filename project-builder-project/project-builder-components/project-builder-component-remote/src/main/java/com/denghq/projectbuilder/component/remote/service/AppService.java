package com.denghq.projectbuilder.component.remote.service;

import com.denghq.projectbuilder.component.remote.model.AppModel;

import java.util.List;

public interface AppService {

	//查询所有应用
	List<AppModel> all();
}
