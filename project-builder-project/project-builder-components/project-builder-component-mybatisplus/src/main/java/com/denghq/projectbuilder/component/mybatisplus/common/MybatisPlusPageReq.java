package com.denghq.projectbuilder.component.mybatisplus.common;

import com.baomidou.mybatisplus.plugins.Page;
import com.denghq.projectbuilder.common.PageReq;
import com.denghq.projectbuilder.component.mybatisplus.xss.SQLFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import springfox.documentation.annotations.ApiIgnore;

@Data
public class MybatisPlusPageReq extends PageReq {

    @ApiIgnore
    @JsonIgnore
    public <T> Page<T> getPage() {

        if (this.getPageNo() == null || this.getPageNo() < 1) {
            this.setPageNo(1);
        }
        if (this.getPageSize() == null) {
            this.setPageSize(10);
        }

        //防止SQL注入（因为sidx、order是通过拼接SQL实现排序的，会有SQL注入风险）

        String sField = SQLFilter.sqlInject(this.getSortField());
        String sOrder = SQLFilter.sqlInject(this.getSortOrder());

        Page<T> page = new Page<>(this.getPageNo(), this.getPageSize());
        if (this.getSearchCount() != null && !this.getSearchCount()) {
            page.setSearchCount(false);
        }
        //排序
        if (StringUtils.isNotBlank(sField) && StringUtils.isNotBlank(sOrder)) {
            page.setOrderByField(sField);
            page.setAsc("ASC".equalsIgnoreCase(sOrder));
        }
        return page;
    }
}
