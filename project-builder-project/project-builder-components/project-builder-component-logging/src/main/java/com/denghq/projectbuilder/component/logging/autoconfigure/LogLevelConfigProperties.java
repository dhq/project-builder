package com.denghq.projectbuilder.component.logging.autoconfigure;

import com.denghq.projectbuilder.component.config.metadata.annotation.Category;
import com.denghq.projectbuilder.component.config.metadata.annotation.ConfigAttribute;
import com.denghq.projectbuilder.component.logging.bean.LogLevelConfigDetail;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix = "com.denghq.loglevelconfig")
@Category(projectName = "日志配置", categoryNo = "smw/LogLevelConfig", categoryName = "微服务日志配置", appNo = "smw")
@Data
public class LogLevelConfigProperties {

    @ConfigAttribute(name = "日志级别配置", remark = "配置所用应用日志级别")
    private List<LogLevelConfigDetail> logLevelDetails ;

}
