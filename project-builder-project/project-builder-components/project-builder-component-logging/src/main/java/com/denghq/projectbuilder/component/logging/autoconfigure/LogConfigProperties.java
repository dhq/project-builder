package com.denghq.projectbuilder.component.logging.autoconfigure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Arrays;
import java.util.List;

@ConfigurationProperties(prefix = "com.denghq.component.logging")
@Data
public class LogConfigProperties {

    private List<String> sqlPackages = Arrays.asList("com.denghq.dao", "org.springframework.jdbc.core.JdbcTemplate");

    private String rootLogName = "com.denghq";

}
