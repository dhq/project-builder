package com.denghq.projectbuilder.component.logging.bean;

import com.denghq.projectbuilder.component.config.metadata.annotation.ConfigAttribute;
import com.denghq.projectbuilder.component.config.metadata.enums.ConfigValueTypeEnum;
import lombok.Data;

@Data
public class LogLevelConfigDetail {

    @ConfigAttribute(name = "应用编号", primaryKey = true, dataSourceApi = "/smw/Server/GetAllServer", dataSourceText = "value,text", valueType = ConfigValueTypeEnum.STRING, remark = "应用唯一编号")
    private String appNo;

    // 是否打印SQL语句
    @ConfigAttribute(name = "是否打印SQL语句", primaryKey = true, valueType = ConfigValueTypeEnum.BOOLEAN, remark = "开启后将记录当前应用编号所有服务SQL")
    public Boolean enableSqlLog = false;


    // 日志级别
    @ConfigAttribute(name = "日志级别", valueType = ConfigValueTypeEnum.STRING, remark = "用于修改当前应用日志级别")
    public String rootLogLevel = "INFO";


}
