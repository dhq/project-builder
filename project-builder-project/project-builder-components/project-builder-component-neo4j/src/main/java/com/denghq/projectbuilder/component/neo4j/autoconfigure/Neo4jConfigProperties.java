package com.denghq.projectbuilder.component.neo4j.autoconfigure;

import com.denghq.projectbuilder.component.config.metadata.annotation.Category;
import com.denghq.projectbuilder.component.config.metadata.annotation.ConfigAttribute;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "spring.data.neo4j")
@Category(projectName = "基础配置", appNo = "common", categoryNo = "common/Neo4jConfigProperties", categoryName = "Neo4j配置")
@Data
public class Neo4jConfigProperties {

    @ConfigAttribute(name = "连接地址", remark = "填写示例：bolt://192.168.0.229:7687")
    private String url;

    @ConfigAttribute(name = "用户名", remark = "填写示例：admin")
    private String username;

    @ConfigAttribute(name = "密码", remark = "填写示例：admin")
    private String password;

}
