/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.denghq.projectbuilder.component.neo4j.autoconfigure;

/*import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.data.neo4j.Neo4jDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.neo4j.Neo4jProperties;
import org.springframework.boot.autoconfigure.domain.EntityScanPackages;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.boot.autoconfigure.transaction.TransactionManagerCustomizers;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import org.springframework.context.annotation.Configuration;
import org.neo4j.ogm.session.SessionFactory;
import org.neo4j.ogm.session.event.EventListener;
import org.springframework.data.neo4j.transaction.Neo4jTransactionManager;
import org.springframework.data.neo4j.web.support.OpenSessionInViewInterceptor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.util.StringUtils;*/

import java.util.List;


/**
 * @author Gary Russell
 * @author Stephane Nicoll
 * @author Eddú Meléndez
 * @author Nakul Mishra
 * @since 1.5.0
 */
/*@Configuration
@ConditionalOnClass({ SessionFactory.class, Neo4jTransactionManager.class,
        PlatformTransactionManager.class })
@ConditionalOnMissingBean(SessionFactory.class)
@EnableConfigurationProperties({Neo4jProperties.class,Neo4jConfigProperties.class})
@AutoConfigureBefore(org.springframework.boot.autoconfigure.data.neo4j.Neo4jDataAutoConfiguration.class)*/
public class Neo4jAutoConfigurationOld {

   /* private final Neo4jProperties properties;

    private final Neo4jConfigProperties neo4jConfigProperties;

    public Neo4jAutoConfigurationOld(Neo4jProperties properties, Neo4jConfigProperties neo4jConfigProperties) {
        this.properties = properties;
        this.neo4jConfigProperties = neo4jConfigProperties;
        this.compose(properties, neo4jConfigProperties);

    }

    @Bean
    public org.neo4j.ogm.config.Configuration configuration(Neo4jProperties properties, Neo4jConfigProperties neo4jConfigProperties) {
        this.compose(properties, neo4jConfigProperties);
        return properties.createConfiguration();
    }

    private void compose(Neo4jProperties properties, Neo4jConfigProperties neo4jConfigProperties) {
        properties.setUri(neo4jConfigProperties.getUrl());
        properties.setUsername(neo4jConfigProperties.getUsername());
        properties.setPassword(neo4jConfigProperties.getPassword());
    }


    @Bean
    public SessionFactory sessionFactory(org.neo4j.ogm.config.Configuration configuration,
                                         ApplicationContext applicationContext,
                                         ObjectProvider<List<EventListener>> eventListeners) {
        SessionFactory sessionFactory = new SessionFactory(configuration,
                getPackagesToScan(applicationContext));
        List<EventListener> providedEventListeners = eventListeners.getIfAvailable();
        if (providedEventListeners != null) {
            for (EventListener eventListener : providedEventListeners) {
                sessionFactory.register(eventListener);
            }
        }
        return sessionFactory;
    }

    @Bean
    @ConditionalOnMissingBean(PlatformTransactionManager.class)
    public Neo4jTransactionManager transactionManager(SessionFactory sessionFactory,
                                                      Neo4jProperties properties,
                                                      ObjectProvider<TransactionManagerCustomizers> transactionManagerCustomizers) {
        return customize(new Neo4jTransactionManager(sessionFactory),
                transactionManagerCustomizers.getIfAvailable());
    }

    private Neo4jTransactionManager customize(Neo4jTransactionManager transactionManager,
                                              TransactionManagerCustomizers customizers) {
        if (customizers != null) {
            customizers.customize(transactionManager);
        }
        return transactionManager;
    }

    private String[] getPackagesToScan(ApplicationContext applicationContext) {
        List<String> packages = EntityScanPackages.get(applicationContext)
                .getPackageNames();
        if (packages.isEmpty() && AutoConfigurationPackages.has(applicationContext)) {
            packages = AutoConfigurationPackages.get(applicationContext);
        }
        return StringUtils.toStringArray(packages);
    }
*/
  /*  @Configuration
    @ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
    @ConditionalOnClass({ WebMvcConfigurer.class, OpenSessionInViewInterceptor.class })
    @ConditionalOnMissingBean(OpenSessionInViewInterceptor.class)
    @ConditionalOnProperty(prefix = "spring.data.neo4j", name = "open-in-view", havingValue = "true", matchIfMissing = true)
    protected static class Neo4jWebConfiguration {

        @Configuration
        protected static class Neo4jWebMvcConfiguration implements WebMvcConfigurer {

            private static final Log logger = LogFactory
                    .getLog(Neo4jDataAutoConfiguration.Neo4jWebConfiguration.Neo4jWebMvcConfiguration.class);

            private final Neo4jProperties neo4jProperties;

            protected Neo4jWebMvcConfiguration(Neo4jProperties neo4jProperties) {
                this.neo4jProperties = neo4jProperties;
            }

            @Bean
            public OpenSessionInViewInterceptor neo4jOpenSessionInViewInterceptor() {
                if (this.neo4jProperties.getOpenInView() == null) {
                    logger.warn("spring.data.neo4j.open-in-view is enabled by default."
                            + "Therefore, database queries may be performed during view "
                            + "rendering. Explicitly configure "
                            + "spring.data.neo4j.open-in-view to disable this warning");
                }
                return new OpenSessionInViewInterceptor();
            }

            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addWebRequestInterceptor(neo4jOpenSessionInViewInterceptor());
            }

        }

    }*/

}
