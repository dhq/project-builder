package com.denghq.projectbuilder.component.neo4j.autoconfigure;

import lombok.extern.slf4j.Slf4j;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author denghq
 */
@Slf4j
@Configuration
@ConditionalOnClass({Driver.class})
@ConditionalOnMissingBean(Driver.class)
@EnableConfigurationProperties({Neo4jConfigProperties.class})
public class Neo4jAutoConfiguration {

    private final Neo4jConfigProperties neo4jConfigProperties;

    public Neo4jAutoConfiguration(Neo4jConfigProperties neo4jConfigProperties) {
        this.neo4jConfigProperties = neo4jConfigProperties;
    }


    @Bean
    public Driver neo4jDriver() {
        try {
            return GraphDatabase.driver(neo4jConfigProperties.getUrl(), AuthTokens.basic(neo4jConfigProperties.getUsername(), neo4jConfigProperties.getPassword()));
        } catch (Exception e) {
            log.warn("连接neo4j数据库出错，请检查neo4j配置。。。。msg:{}", e.getMessage());
            return null;
        }
    }
}
