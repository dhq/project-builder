package com.denghq.projectbuilder.component.zuul.config;

import com.denghq.projectbuilder.component.zuul.CustomerDiscoveryClientRouteLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.cloud.netflix.zuul.ZuulServerAutoConfiguration;
import org.springframework.cloud.netflix.zuul.filters.discovery.DiscoveryClientRouteLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ZuulConfig extends ZuulServerAutoConfiguration {
	
	@Autowired(required = false)
	private Registration registration;

	@Autowired
	private DiscoveryClient discovery;
	
	@Bean
	public DiscoveryClientRouteLocator discoveryRouteLocator() {
		return new CustomerDiscoveryClientRouteLocator(this.server.getServlet().getServletPrefix(), this.discovery, this.zuulProperties,
				this.registration);
	}
}
