package com.denghq.projectbuilder.common.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
@NoArgsConstructor
public class CloudException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8691030481915658240L;
	
	private String res;

}
