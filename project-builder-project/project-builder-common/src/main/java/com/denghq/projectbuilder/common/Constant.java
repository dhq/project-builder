package com.denghq.projectbuilder.common;

/**
 * 常量
 * 
 * @author denghq
 * 
 * @date 2018/10/29
 */
public class Constant {
	/** 超级管理员ID */
	public static final int SUPER_ADMIN = 1;

    public static final String SERVICE_TOKEN_NAME = "Authorization";

    /**
	 * 菜单类型
	 * 
	 * @author denghq
	 * 
	 * @date 2018/10/29
	 */
    public enum MenuType {
        /**
         * 目录
         */
    	CATALOG(0),
        /**
         * 菜单
         */
        MENU(1),
        /**
         * 按钮
         */
        BUTTON(2);

        private int value;

        MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    
    /**
     * 定时任务状态
     * 
     * @author denghq
     * 
     * @date 2018/10/29
     */
    public enum ScheduleStatus {
        /**
         * 正常
         */
    	NORMAL(0),
        /**
         * 暂停
         */
    	PAUSE(1);

        private int value;

        ScheduleStatus(int value) {
            this.value = value;
        }
        
        public int getValue() {
            return value;
        }
    }
}
