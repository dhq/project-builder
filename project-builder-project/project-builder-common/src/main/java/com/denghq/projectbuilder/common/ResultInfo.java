package com.denghq.projectbuilder.common;

import java.io.Serializable;
import java.util.List;

import com.denghq.projectbuilder.common.exception.BussinessException;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.NoArgsConstructor;

/*
 *
  *  json 格式返回结果封装类
  * 
  * @author denghq
  * @date 2018/10/29
 */
@ApiModel(value="返回结果")
@NoArgsConstructor
public class ResultInfo<T> implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -2784240448628076090L;
	
    //api 状态码
	@ApiModelProperty(value = "状态码[ 0, 5000, 10001, 10002, 10003, 10008, 10012, 10021, 10026, 10027, 10028, 10029, 10030, 10031, 10032, 10033, 11000, 12000 ]")
    private Integer code;
    
    //状态码说明
	@ApiModelProperty(value = "状态码说明")
    private String codeRemark;
    
  //业务信息
    @ApiModelProperty(value = "业务信息")
    private String message;
    
    //数据信息
    @ApiModelProperty(value = "数据信息")
    private T result;
    
    //总记录数
    @ApiModelProperty(value = "总记录数")
    private Integer totalCount;
    
    //总页数
    @ApiModelProperty(value = "总页数")
    private Integer totalPage;
    
    
    public ResultInfo(Integer code ,String codeRemark,String message,T result ,Integer totalCount,Integer totalPage) {
        
    	this.code = code;
    	this.codeRemark = codeRemark;
    	this.message = message;
    	this.result = result;
    	this.totalCount = totalCount;
    	this.totalPage = totalPage;
    }

    private ResultInfo(CodeMsg codeMsg)  {
        if(codeMsg != null) {
            this.code = codeMsg.getCode();
            this.codeRemark = codeMsg.getCodeRemark();
        }
    }

    private ResultInfo(CodeMsg codeMsg,String message,T result,Integer totalCount,Integer totalPage){
    	if(codeMsg != null){
		    this.code = codeMsg.getCode();
	        this.codeRemark = codeMsg.getCodeRemark();
    	}
    	this.message = message;
        this.result = result;
        this.totalCount = totalCount;
        this.totalPage = totalPage;
    }
 

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> ResultInfo<T> success(String message,T result,Integer totalCount,Integer totalPage) {
        return new ResultInfo(CodeMsg.SUCCESS,message,result,totalCount,totalPage);
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static <T> ResultInfo<T> success(T result,Integer totalCount,Integer totalPage) {
        return new ResultInfo(CodeMsg.SUCCESS,null,result,totalCount,totalPage);
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static <T> ResultInfo<List<T>> successForPage(List<T> list,Long totalCount,Long totalPage) {
    	return new ResultInfo(CodeMsg.SUCCESS,null,list,new Integer(totalCount==null?"0":totalCount.toString()),new Integer(totalPage==null?"0":totalPage.toString()));
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static <T> ResultInfo<T> success(T result,String message) {
        return new ResultInfo(CodeMsg.SUCCESS,message,result,null,null);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static <T> ResultInfo<T> success(T result) {
        return new ResultInfo(CodeMsg.SUCCESS,null,result,null,null);
    }
    
    @SuppressWarnings({ "rawtypes" })
    public static ResultInfo success() {
        return new ResultInfo(CodeMsg.SUCCESS);
    }
    
   	@SuppressWarnings("rawtypes")
	public static ResultInfo error(BussinessException e) {
         return error(e.getCodeMsg(),e.getMessage());
    }
    
    @SuppressWarnings({ "rawtypes" })
    public static ResultInfo error(CodeMsg codeMsg) {
        return error(codeMsg,codeMsg.getCodeRemark());
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static ResultInfo error(CodeMsg codeMsg,String message) {
        return new ResultInfo(codeMsg,message,null,null,null);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static <T> ResultInfo<T> error(CodeMsg codeMsg,T result) {
        return new ResultInfo(codeMsg,null,result,null,null);
    }

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getCodeRemark() {
		return codeRemark;
	}

	public void setCodeRemark(String codeRemark) {
		this.codeRemark = codeRemark;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}
    
}
