package com.denghq.projectbuilder.common.validator;

import com.denghq.projectbuilder.common.CodeMsg;
import com.denghq.projectbuilder.common.exception.BussinessException;
import org.apache.commons.lang3.StringUtils;

/**
 * 数据校验
 * @author denghq
 * 
 * @date 2018/10/29
 */
public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new BussinessException(CodeMsg.PARAMETER_ERROR,message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
        	throw new BussinessException(CodeMsg.PARAMETER_ERROR,message);
        }
    }
}
