package com.denghq.projectbuilder.common.cache;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * 简易本地缓存的实现类
 */
@Slf4j
public class LocalCache {

    // 默认的缓存容量
    private static int DEFAULT_CAPACITY = 512;

    // 最大容量
    private static int MAX_CAPACITY = 100000;

    // 刷新缓存的频率，单位：秒
    private static int MONITOR_DURATION = 2;

    private LocalCache() {

    }

    /**
     * 类级的内部类，也就是静态的成员式内部类，该内部类的实例与外部类的实例
     * 没有绑定关系，而且只有被调用到才会装载，从而实现了延迟加载
     */
    private static class SingletonHolder {
        /**
         * 静态初始化器，由JVM来保证线程安全
         */
        private static LocalCache instance = new LocalCache();
    }

    public static LocalCache getInstance() {
        return SingletonHolder.instance;
    }

    // 启动监控线程
    static {
        Thread thread = new Thread(new TimeoutTimerThread());
        thread.setDaemon(true);
        thread.start();
    }

    // 使用默认容量创建一个Map
    private static ConcurrentHashMap<String, CacheEntity> cache = new ConcurrentHashMap<String, CacheEntity>(
            DEFAULT_CAPACITY);

    /**
     * 将key-value 保存到本地缓存并制定该缓存的过期时间
     *
     * @param key
     * @param value
     * @param expireTime 过期时间，如果是-1 则表示永不过期
     * @return
     */
    public boolean putValue(String key, Object value, int expireTime) {
        return putCloneValue(key, value, expireTime);
    }

    /**
     * 将值通过序列化clone 处理后保存到缓存中，可以解决值引用的问题
     *
     * @param key
     * @param value
     * @param expireTime
     * @return
     */
    private boolean putCloneValue(String key, Object value, int expireTime) {
        try {
            if (cache.size() >= MAX_CAPACITY) {
                cache.clear();
                //return false;
            }
            CacheEntity cacheEntity = new CacheEntity(value, System.nanoTime(), expireTime);
            // 序列化赋值
            try {
                CacheEntity cloneEntity = clone(cacheEntity);
                if (cloneEntity != null) {
                    cacheEntity = cloneEntity;
                }
            } catch (Exception e) {
                log.info("序列化：{}失败，直接缓存对象引用", value.getClass().getName());
            }
            cache.put(key, cacheEntity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 序列化 克隆处理
     *
     * @param object
     * @return
     */
    @SuppressWarnings("unchecked")
    private <T extends Serializable> T clone(T object) {
        T cloneObject = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            oos.close();
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            cloneObject = (T) ois.readObject();
            ois.close();
        } catch (Exception e) {
            log.debug(e.getMessage());
            //e.printStackTrace();
        }
        return cloneObject;
    }

    /**
     * 从本地缓存中获取key对应的值，如果该值不存则则返回null
     *
     * @param key
     * @return
     */
    public Object getValue(String key) {
        CacheEntity cacheEntity = cache.get(key);
        if (cacheEntity != null) {
            Boolean clearOverdueCache = clearOverdueCache(key, cacheEntity);
            if (clearOverdueCache) {//如果过期被清理掉了
                return null;
            } else {
                cacheEntity.setGmtModify(System.nanoTime());//更新过期的时间
                return cacheEntity.getValue();
            }
        } else {
            return null;
        }

    }

    /**
     * 清空所有
     */
    public void clear() {
        cache.clear();
    }

    /**
     * 过期处理线程
     */
    static class TimeoutTimerThread implements Runnable {
        public void run() {
            while (true) {
                try {
                    log.debug("Cache monitor");
                    //System.out.println("Cache monitor");
                    TimeUnit.SECONDS.sleep(MONITOR_DURATION);
                    checkTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * 过期缓存的具体处理方法
         *
         * @throws Exception
         */
        private void checkTime() throws Exception {
            // "开始处理过期 ";

            for (String key : cache.keySet()) {
                CacheEntity tce = cache.get(key);
                clearOverdueCache(key, tce);
            }
        }


    }

    private static Boolean clearOverdueCache(String key, CacheEntity tce) {
        long timoutTime = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - tce.getGmtModify());
        // " 过期时间 : "+timoutTime);
        if (tce.getExpire() > timoutTime) {
            return false;
        }
        log.debug(" 清除过期缓存 ： " + key);
        //System.out.println(" 清除过期缓存 ： " + key);
        // 清除过期缓存和删除对应的缓存队列
        cache.remove(key);
        return true;
    }

}