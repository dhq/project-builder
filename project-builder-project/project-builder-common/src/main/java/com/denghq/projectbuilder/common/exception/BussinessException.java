package com.denghq.projectbuilder.common.exception;

import com.denghq.projectbuilder.common.CodeMsg;

import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 *
  * 业务异常类
  * 
  * @author denghq
  * @date 2018/10/29
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class BussinessException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private CodeMsg codeMsg;
    
    private String msg;

    public BussinessException() {

    }
    public BussinessException(CodeMsg codeMsg,String msg) {
        super(codeMsg.toString());
        this.codeMsg = codeMsg;
        this.msg = msg;
    }

    public CodeMsg getCodeMsg() {
        return codeMsg;
    }


}
