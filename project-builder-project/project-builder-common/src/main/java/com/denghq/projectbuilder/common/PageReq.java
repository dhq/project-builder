package com.denghq.projectbuilder.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PageReq {

    @ApiModelProperty(value = "是否分页，默认为否", position = 0, example = "false")
    private Boolean isPagination = false;

    @ApiModelProperty(value = "是否返回总条数，默认为是", position = 0, example = "true")
    private Boolean searchCount = true;

    @ApiModelProperty(value = "第几页，isPagination为true时有效", position = 0, example = "1")
    private Integer pageNo;
    @ApiModelProperty(value = "每页大小，isPagination为true时有效", position = 0, example = "10")
    private Integer pageSize;

    @ApiModelProperty(value = "排序字段", position = 0, example = "createTime")
    private String sortField;

    @ApiModelProperty(value = "排序方向", position = 0, example = "desc")
    private String sortOrder;

}
